package com.leon.lfilepickerlibrary.adapter;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leon.lfilepickerlibrary.R;
import com.leon.lfilepickerlibrary.utils.Constant;
import com.leon.lfilepickerlibrary.utils.FileUtils;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.leon.lfilepickerlibrary.utils.FileUtils.getPlayTime;

/**
 * 作者：Leon
 * 时间：2017/3/15 15:47
 */
public class PathAdapter extends RecyclerView.Adapter<PathAdapter.PathViewHolder> {
    public interface OnItemClickListener {
        void click(int position);
    }

    public interface OnCancelChoosedListener {
        void cancelChoosed(CheckBox checkBox);
    }

    private final String TAG = "FilePickerLeon";
    private List<File> mListData;
    private Context mContext;
    public OnItemClickListener onItemClickListener;
    private FileFilter mFileFilter;
    private boolean[] mCheckedFlags;
    private boolean mMutilyMode;
    private int mIconStyle;
    private boolean mIsGreater;
    private long mFileSize;

    public PathAdapter(List<File> mListData, Context mContext, FileFilter mFileFilter, boolean mMutilyMode, boolean mIsGreater, long mFileSize) {
        this.mListData = mListData;
        this.mContext = mContext;
        this.mFileFilter = mFileFilter;
        this.mMutilyMode = mMutilyMode;
        this.mIsGreater = mIsGreater;
        this.mFileSize = mFileSize;
        mCheckedFlags = new boolean[mListData.size()];

        String size = String.valueOf(mListData.size());
        SharedPreferencesUtil.putString(mContext, SharedPreferencesUtil.FILECOUNT, size);

    }

    @Override
    public PathViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = View.inflate(mContext, R.layout.item_video_list, null);
//        PathViewHolder pathViewHolder = new PathViewHolder(view);
//        return pathViewHolder;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_list,
                parent, false);//解决宽度不能铺满
        PathViewHolder pathViewHolder = new PathViewHolder(view);
        return pathViewHolder;
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    @Override
    public void onBindViewHolder(final PathViewHolder holder, final int position) {
        final File file = mListData.get(position);
        if (file.isFile()) {
            Glide.with( mContext )
                    .load( Uri.fromFile( new File( file.getPath() ) ) )
                    .into( holder.item_video_imgv );

            long playTime = Long.parseLong(FileUtils.getPlayTime(file.getPath()));
            String duration = FileUtils.timeParse(playTime);
            holder.item_duration_tv.setText(duration);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String recordtime=sdf.format(file.lastModified());
            holder.item_recordtime_tv.setText(recordtime);
            holder.btn_status.setText("上传");

        }
    }


    /**
     * 设置监听器
     *
     * @param onItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * 设置数据源
     *
     * @param mListData
     */
    public void setmListData(List<File> mListData) {
        this.mListData = mListData;
        mCheckedFlags = new boolean[mListData.size()];
    }

    public void setmIconStyle(int mIconStyle) {
        this.mIconStyle = mIconStyle;
    }

    /**
     * 设置是否全选
     *
     * @param isAllSelected
     */
    public void updateAllSelelcted(boolean isAllSelected) {

        for (int i = 0; i < mCheckedFlags.length; i++) {
            mCheckedFlags[i] = isAllSelected;
        }
        notifyDataSetChanged();
    }

    class PathViewHolder extends RecyclerView.ViewHolder {

        private ImageView item_video_imgv; //缩略图
        private TextView item_duration_tv; //时长
        private TextView item_policyname_tv; //投保人姓名
        private TextView item_insuretype_tv; //保险种类
        private TextView item_recordtime_tv; //录制的开始时间
        private TextView btn_status; //按钮

        public PathViewHolder(View itemView) {
            super(itemView);
            item_video_imgv = (ImageView) itemView.findViewById(R.id.item_video_imgv);
            item_duration_tv = (TextView) itemView.findViewById(R.id.item_duration_tv);
            item_policyname_tv = (TextView) itemView.findViewById(R.id.item_policyname_tv);
            item_insuretype_tv = (TextView) itemView.findViewById(R.id.item_insuretype_tv);
            item_recordtime_tv = (TextView) itemView.findViewById(R.id.item_recordtime_tv);
            btn_status = (TextView) itemView.findViewById(R.id.btn_status);
        }
    }
}


