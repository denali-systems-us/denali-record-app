/*
 *     (C) Copyright 2019, ForgetSky.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.leon.lfilepickerlibrary.adapter;

import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.leon.lfilepickerlibrary.R;
import com.leon.lfilepickerlibrary.utils.FileUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;


public class FileAdapter extends BaseQuickAdapter<File, BaseViewHolder> {





    public FileAdapter(int layoutResId, @Nullable List<File> list) {
        super(layoutResId,list);

    }


    @Override
    protected void convert(BaseViewHolder helper, File file) {

        if (file.isFile()) {

            ImageView imgv =(ImageView) helper.getView(R.id.item_video_imgv);

            Glide.with( getContext() )
                    .load( Uri.fromFile( new File( file.getPath() ) ) )
                    .into( imgv );

            long playTime = Long.parseLong(FileUtils.getPlayTime(file.getPath()));
            String duration = FileUtils.timeParse(playTime);
            helper.setText(R.id.item_duration_tv, duration);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String recordtime=sdf.format(file.lastModified());
            helper.setText(R.id.item_recordtime_tv,recordtime);
            helper.setText(R.id.btn_status, "上传");

        }

    }

}
