package com.leon.lfilepickerlibrary.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 */
public class SharedPreferencesUtil {

    private static final String spFileName = "Live";

    public static final String SERVICEIP = "serviceIp";

    public static final String SOCKETIP = "socketIp";
    public static final String SOCKETPORT = "socketPort";

    public static final String LOGINTOKEN = "logintoken";
    public static final String REFRESHTOKEN = "refreshtoken";
    public static final String LOGINUSERNAME = "loginusername";

    public static final String FILECOUNT = "filecount";


    public static String getString(Context context, String strKey,
                                   String strDefault) {//strDefault	boolean: Value to return if this preference does not exist.
        SharedPreferences setPreferences = context.getSharedPreferences(
                spFileName, Context.MODE_PRIVATE);
        String result = setPreferences.getString(strKey, strDefault);
        return result;
    }

    public static void putString(Context context, String strKey,
                                 String strData) {
        SharedPreferences activityPreferences = context.getSharedPreferences(
                spFileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = activityPreferences.edit();
        editor.putString(strKey, strData);
        editor.apply();
    }
}
