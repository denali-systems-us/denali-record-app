package com.leon.lfilepickerlibrary.ui;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.leon.lfilepickerlibrary.R;
import com.leon.lfilepickerlibrary.adapter.FileAdapter;
import com.leon.lfilepickerlibrary.adapter.PathAdapter;
import com.leon.lfilepickerlibrary.filter.LFileFilter;
import com.leon.lfilepickerlibrary.model.ParamEntity;
import com.leon.lfilepickerlibrary.utils.CommonUtils;
import com.leon.lfilepickerlibrary.utils.FileUtils;
import com.leon.lfilepickerlibrary.utils.StringUtils;
import com.leon.lfilepickerlibrary.widget.EmptyRecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.yongxing.lib.StatusBarBlackOnWhiteUtil;
import me.jessyan.autosize.internal.CustomAdapt;

public class LFilePickerActivity extends AppCompatActivity implements CustomAdapt {

    private final String TAG = "FilePickerLeon";
//    private EmptyRecyclerView mRecylerView;
    private RecyclerView file_recycle;
    private View mEmptyView;
    private TextView mTvPath;
    private TextView mTvBack;
    private Button mBtnAddBook;
    private String mPath;
    private List<File> mListFiles;
    private ArrayList<String> mListNumbers = new ArrayList<String>();//存放选中条目的数据地址
//    private PathAdapter mPathAdapter;
    private FileAdapter mAdapter;
    private ParamEntity mParamEntity;
    private LFileFilter mFilter;

    Toolbar mToolbar;
    TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mParamEntity = (ParamEntity) getIntent().getExtras().getSerializable("param");
//        setTheme(mParamEntity.getTheme());
        // 系统 6.0 以上 状态栏白底黑字的实现方法
        StatusBarBlackOnWhiteUtil.setStatusBarColorAndFontColor(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lfile_picker);
        initView();
        setToolbarTitle(R.string.list_title);
        initToolbar();

        if (!checkSDState()) {
            Toast.makeText(this, R.string.lfile_NotFoundPath, Toast.LENGTH_SHORT).show();
            return;
        }
        mPath = mParamEntity.getPath();
        if (StringUtils.isEmpty(mPath)) {
            //如果没有指定路径，则使用默认路径
            mPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        mFilter = new LFileFilter(mParamEntity.getFileTypes());
        mListFiles = FileUtils.getFileList(mPath, mFilter, mParamEntity.isGreater(), mParamEntity.getFileSize());


        mAdapter = new FileAdapter(R.layout.item_video_list, mListFiles);
        mAdapter.setOnItemClickListener((adapter, view, position) -> startActivity(position));
        // 一行代码搞定（默认为渐显效果）
//        mAdapter.openLoadAnimation();
        mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.SlideInLeft);

        file_recycle.setLayoutManager(new LinearLayoutManager(LFilePickerActivity.this));
        file_recycle.setHasFixedSize(true);
        //添加分割线
//        file_recycle.addItemDecoration( new SimpleDividerDecoration(LFilePickerActivity.this));
        file_recycle.setAdapter(mAdapter);


    }

    private void startActivity(int position) {

    }

    public void setToolbarTitle(int resId) {
        mTitle.setText(resId);
    }

    /**
     * 更新Toolbar展示
     */
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mToolbar.setNavigationOnClickListener(v -> onBackPressedSupport());
    }

    private void onBackPressedSupport() {
        CommonUtils.hideKeyBoard(this, this.getWindow().getDecorView().getRootView());
    }


    /**
     * 初始化控件
     */
    private void initView() {
        file_recycle = (RecyclerView) findViewById(R.id.file_recycle);

        mEmptyView = findViewById(R.id.empty_view);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mToolbar=findViewById(R.id.toolbar);
        mTitle=findViewById(R.id.toolbar_title);
    }

    /**
     * 检测SD卡是否可用
     */
    private boolean checkSDState() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 显示顶部地址
     *
     * @param path
     */
    private void setShowPath(String path) {
        mTvPath.setText(path);
    }


    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}
