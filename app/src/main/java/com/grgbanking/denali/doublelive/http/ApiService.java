package com.grgbanking.denali.doublelive.http;

import com.grgbanking.denali.doublelive.bean.AllListBean;
import com.grgbanking.denali.doublelive.bean.MainPageSendData;
import com.grgbanking.denali.doublelive.login.LoginData;
import com.grgbanking.denali.doublelive.login.LoginSendData;
import com.grgbanking.denali.doublelive.login.RegisterData;
import com.grgbanking.denali.doublelive.socketUtil.BaseApplication;

import io.reactivex.Flowable;
import me.lake.librestreaming.utils.ConstantUtil;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public  interface  ApiService {

//    String BASE_IP="10.252.206.11";
//    String BASE_IP="221.5.109.20";
//    String BASE_IP="10.252.15.9";
//    String BASE_IP="10.252.51.11";
//    String BASE_IP="122.9.53.187";

    String BASE_PORT="31200";

    /**
     * other 公有云
     */
    String BASE_URL_cloud = "http://"+BaseApplication.getInstance().getBASE_IP()+":31000/";

    /**
     * 登录
     * http://10.252.51.11:30055/denali-authentication-server/oauth/app/login
     *
     * @return 登录数据
     */

    @POST("oauth/app/login")
    Flowable<LoginData> login(@Body LoginSendData loginSendData);

    /**
     * 注册
     * http://10.252.51.11:30055/denali-authentication-server/oauth/clients?accessTokenValidity=10000
     * http://10.252.51.11:30781/api/v1/clients?accessTokenValidity=36000&clientName=live&description=live
     *
     */

    @FormUrlEncoded
    @POST("api/v1/clients")
    Flowable<RegisterData> register(@Field("accessTokenValidity") int accessTokenValidity,
                                    @Field("clientName") String clientName,
                                    @Field("description") String description);

    /**
     * 获取视频列表
     * http://10.252.15.20:31000/api/v1/video/getVideoForCheck
     *
     */

    @POST("api/v1/video/getVideoForCheck")
    Flowable<AllListBean> getVideoForCheck(@Body MainPageSendData vedioCheckQueryDto, @Header("Authorization") String token);

    @Streaming
    @GET("api/v1/file/play")
    Flowable<String> getPlayer(@Query("fileId") String fileId ,@Header("Authorization") String token);
}
