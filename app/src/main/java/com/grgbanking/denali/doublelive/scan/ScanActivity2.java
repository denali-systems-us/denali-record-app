package com.grgbanking.denali.doublelive.scan;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.github.ybq.android.spinkit.style.Wave;
import com.google.gson.Gson;
import com.grgbanking.denali.doublelive.LiveMainActivity;
import com.grgbanking.denali.doublelive.LiveMainActivity0721;
import com.grgbanking.denali.doublelive.LiveMainActivity0819;
import com.grgbanking.denali.doublelive.RecordService;
import com.grgbanking.denali.doublelive.base.activity.AbstractSimpleActivity;
import com.grgbanking.denali.doublelive.bean.ScanBean;
import com.grgbanking.denali.doublelive.bean.VoiceConfigBean;
import com.grgbanking.denali.doublelive.login.LoginActivity;
import com.grgbanking.denali.doublelive.utils.AlertAnimationLoading;
import com.grgbanking.denali.doublelive.utils.JsonUtils;
import com.grgbanking.denali.doublelive.utils.SimpleDividerDecoration;
import com.grgbanking.denali.doublelive.R;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.utils.ClickUtil;
import me.lake.librestreaming.utils.ConstantUtil;

public class ScanActivity2 extends AbstractSimpleActivity implements CustomAdapt {

    /**
     * ----------------标题栏------------------
     **/
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mTitle;

    /**
     * ----------------scan_main_info------------------
     **/
    @BindView(R.id.scan_info_num)
    TextView scan_info_num;
    @BindView(R.id.scan_info_type)
    TextView scan_info_type;
    @BindView(R.id.scan_info_name)
    TextView scan_info_name;
    @BindView(R.id.scan_info_id_num)
    TextView scan_info_id_num;
    @BindView(R.id.scan_info_org_code)
    TextView scan_info_org_code;
    @BindView(R.id.scan_info_sales_name)
    TextView scan_info_sales_name;
    @BindView(R.id.scan_info_sales_num)
    TextView scan_info_sales_num;
    @BindView(R.id.scan_info_sales_id_num)
    TextView scan_info_sales_id_num;

    /**
     * ----------------scan_main_warn------------------
     **/
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private ScanAdapter mAdapter;
    /**
     * ----------------scan_main_warn------------------
     **/

    @BindView(R.id.scan_main_info_layout)
    View scan_main_info_layout;
    @BindView(R.id.scan_main_warn_layout)
    View scan_main_warn_layout;
    @BindView(R.id.btn_info_next)
    TextView btn_info_next;
    @BindView(R.id.btn_warn_next)
    TextView btn_warn_next;

    private String scanValue;
    private AlertAnimationLoading alertLoading1;

    private static final int RECORD_REQUEST_CODE = 101;
    private static final int STORAGE_REQUEST_CODE = 102;
    private static final int AUDIO_REQUEST_CODE = 103;

    private MediaProjectionManager projectionManager;
    private MediaProjection mediaProjection;
    private RecordService recordService;

    public static ScanActivity2 scanActivity2;
    private VoiceConfigBean voiceConfigBean;

    /**
     * -----------------------handle 更新UI-------------------------
     **/
    private CountDownTimer timerFace;
    public final int SEND_DIALOG = 1;
    public Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case SEND_DIALOG:
                    initTimer();
                    break;
                default:
                    break;

            }
            return false;
        }
    });

    private void initTimer() {
        timerFace = new CountDownTimer(2 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时执行的方法
                alertLoading1 = new AlertAnimationLoading(ScanActivity2.this,
                        R.layout.base_loading_dialog);
                alertLoading1.setMessage("正在启动....");
                alertLoading1.setCanceledOnTouchOutside(false);
                alertLoading1.showDialog();
            }

            @Override
            public void onFinish() {
                //倒计时结束后的方法
                Intent intentLogin = new Intent(ScanActivity2.this,
                        LiveMainActivity.class);
//                Intent intentLogin = new Intent(ScanActivity2.this,
//                        LiveMainActivity0819.class);
                intentLogin.putExtra("scanValue", scanValue);
                intentLogin.putExtra("voiceConfigBean", voiceConfigBean);
                startActivity(intentLogin);
                finish();
            }
        };
        timerFace.start();//开始倒计时
    }

    @Override
    protected void initView() {
        scanActivity2 = this;

        setToolbarTitle(R.string.scan_info_title);
        scanValue = getIntent().getStringExtra("scanValue");

//        projectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        if (ContextCompat.checkSelfPermission(ScanActivity2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
        }

//        if (ContextCompat.checkSelfPermission(ScanActivity2.this, Manifest.permission.RECORD_AUDIO)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[] {Manifest.permission.RECORD_AUDIO}, AUDIO_REQUEST_CODE);
//        }
        //话术配置 现在是仿照HTTP
        String voiceData = JsonUtils.getJsonData("voiceabout.json", ScanActivity2.this);
        Gson gson = new Gson();
        voiceConfigBean = gson.fromJson(voiceData, VoiceConfigBean.class);
    }

    private void initRecyclerView() {
        String data = JsonUtils.getJsonData("scan.json", ScanActivity2.this);
        Gson gson = new Gson();
        Model model = gson.fromJson(data, Model.class);

       /* List<Model> list=new ArrayList<>();
        list.add(model);*/
        //关键要传值是对应得列表
        List<Model.DenaliBean> list = model.getDenali();

        mAdapter = new ScanAdapter(R.layout.item_scan_warn_list, list);
        mAdapter.setOnItemClickListener((adapter, view, position) -> startActivity(position));
        // 一行代码搞定（默认为渐显效果）
//        mAdapter.openLoadAnimation();
        mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.AlphaIn);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(ScanActivity2.this));
        mRecyclerView.setHasFixedSize(true);
        //添加分割线
        mRecyclerView.addItemDecoration(new SimpleDividerDecoration(ScanActivity2.this));
        mRecyclerView.setAdapter(mAdapter);

    }

    private void startActivity(int position) {

        switch (mAdapter.getData().get(position).getId()) {
            case "01":

                break;

            default:
                break;
        }
    }


    public void setToolbarTitle(int resId) {
        mTitle.setText(resId);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scan_info;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mToolbar.setNavigationOnClickListener(v -> onBackPressedSupport());
    }

    @Override
    protected void initEventAndData() {
        //扫码成功返回更新布局到保单信息
        scan_main_info_layout.setVisibility(View.VISIBLE);
        btn_info_next.setVisibility(View.VISIBLE);
        if (!scanValue.contains("{")) {
            scan_info_num.setText("123456789");
            scan_info_type.setText("人身保险");
            scan_info_name.setText("张晓红");
            scan_info_id_num.setText("445464564656842185");
            scan_info_org_code.setText("597456");
            scan_info_sales_name.setText("社会人");
            scan_info_sales_num.setText("123456");
            scan_info_sales_id_num.setText("440221100106173519");
        } else {
            Gson gson = new Gson();
            ScanBean scanBean = gson.fromJson(scanValue, ScanBean.class);

            scan_info_num.setText(scanBean.getPolicyNum());
            scan_info_type.setText(scanBean.getInsureTypeDesc());
            scan_info_name.setText(scanBean.getPolicyName());
            scan_info_id_num.setText(scanBean.getPolicyCard());
            scan_info_org_code.setText(scanBean.getOrganizationCode());
            scan_info_sales_name.setText(scanBean.getStaffName());
            scan_info_sales_num.setText(scanBean.getStaffNum());
            scan_info_sales_id_num.setText(scanBean.getStaffCard());
        }
    }

    @OnClick({R.id.btn_info_next, R.id.btn_warn_next})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_info_next:
                if (ClickUtil.isNotFastClick()) {
                    //更改标题
                    setToolbarTitle(R.string.scan_warn_title);
                    scan_main_info_layout.setVisibility(View.GONE);
                    btn_info_next.setVisibility(View.GONE);
                    scan_main_warn_layout.setVisibility(View.VISIBLE);
                    btn_warn_next.setVisibility(View.VISIBLE);
                    btn_warn_next.setClickable(true);
                    btn_warn_next.setEnabled(true);
                    //加载内容
                    initRecyclerView();
                }
                break;

            case R.id.btn_warn_next:

                //就是为了添加加载框，让启动摄像头不那么突兀
                Message msg = new Message();
                msg.what = SEND_DIALOG;
                mHandler.sendMessage(msg);


                //处理点击事件

//                Intent intentLogin = new Intent(ScanActivity2.this,
//                        LiveMainActivity0721.class);
//                intentLogin.putExtra("scanValue", scanValue);
//                startActivity(intentLogin);
//                finish();

                break;

            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unbindService(connection);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RECORD_REQUEST_CODE && resultCode == RESULT_OK) {
            mediaProjection = projectionManager.getMediaProjection(resultCode, data);
            recordService.setMediaProject(mediaProjection);
            recordService.startRecord();

            Intent intentLogin = new Intent(ScanActivity2.this,
                    LoginActivity.class);
            startActivity(intentLogin);
//            Intent intentLogin = new Intent(ScanActivity2.this,
//                    LiveMainActivity.class);
//            intentLogin.putExtra("scanValue", scanValue);
//            startActivity(intentLogin);
//            setResult(RESULT_OK, intentLogin);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_REQUEST_CODE /*|| requestCode == AUDIO_REQUEST_CODE*/) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            RecordService.RecordBinder binder = (RecordService.RecordBinder) service;
            recordService = binder.getRecordService();
            recordService.setConfig(metrics.widthPixels, metrics.heightPixels, metrics.densityDpi);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}
