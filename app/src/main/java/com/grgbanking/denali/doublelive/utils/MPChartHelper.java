package com.grgbanking.denali.doublelive.utils;

import android.graphics.Color;


import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.grgbanking.denali.doublelive.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JKWANG-PC on 2016/10/20.
 */

public class MPChartHelper {

    public static final int[] PIE_COLORS = {
            Color.rgb(241, 67, 66), Color.rgb(255, 152, 0),
            Color.rgb(255,216,93),Color.rgb(51,118,234),
            Color.rgb(55,157,229), Color.rgb(19,181,170)
    };

    public static final int[] LINE_COLORS = {
            Color.rgb(140, 210, 118), Color.rgb(159, 143, 186), Color.rgb(233, 197, 23)
    };//绿色，紫色，黄色

    public static final int[] LINE_FILL_COLORS = {
            Color.rgb(222, 239, 228), Color.rgb(246, 234, 208), Color.rgb(235, 228, 248)
    };


    public static final int[] LINE_CURVE_COLORS = {
            Color.rgb(241, 67, 66), Color.rgb(250, 185, 82),
            Color.rgb(0, 129, 185), Color.rgb(19, 181, 170)
    };//绿色，紫色，黄色

    public static final int[] LINE_CURVE_FILL_COLORS = {
            Color.rgb(245, 107, 118), Color.rgb(250, 193, 83),
            Color.rgb(135, 206, 250), Color.rgb(19, 230, 230)
    };


    /**
     * 测试堆叠柱状图
     * @param barChart
     * @param xAxisValue
     * @param yAxisValue
     * @param title
     * @param xAxisTextSize
     * @param barColor
     */
    public static void setStackChart(BarChart barChart, List<String> xAxisValue, List<BarEntry> yAxisValue, String title, float xAxisTextSize, Integer barColor) {
        barChart.getDescription().setEnabled(false);//设置描述
        barChart.setPinchZoom(false);//设置按比例放缩柱状图
        barChart.setVisibleXRangeMaximum(12);
        barChart.setDragEnabled(true);    // 可拖动,默认true
        barChart.setScaleXEnabled(false);  // X轴上的缩放,默认true
        barChart.setScaleYEnabled(false);

        //设置自定义的markerView
//        MPChartMarkerView markerView = new MPChartMarkerView(barChart.getContext(), R.layout.custom_marker_view);
//        barChart.setMarker(markerView);

        //x坐标轴设置
        IAxisValueFormatter xAxisFormatter = new StringAxisValueFormatter(xAxisValue);//设置自定义的x轴值格式化器
        XAxis xAxis = barChart.getXAxis();//获取x轴
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//设置X轴标签显示位置
        xAxis.setDrawGridLines(false);//不绘制格网线
        xAxis.setDrawLabels(true);
        xAxis.setDrawAxisLine(false);  //设置false不绘制x轴的线
        xAxis.setGranularity(1f);//设置最小间隔，防止当放大时，出现重复标签。
        xAxis.setValueFormatter(xAxisFormatter);
        xAxis.setTextSize(xAxisTextSize);//设置标签字体大小
        xAxis.setLabelCount(xAxisValue.size());//设置标签显示的个数

        //y轴设置
        YAxis leftAxis = barChart.getAxisLeft();//获取左侧y轴
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);//设置y轴标签显示在外侧
        leftAxis.setAxisMinimum(0f);//设置Y轴最小值
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawLabels(true);//禁止绘制y轴标签
        leftAxis.setDrawAxisLine(true);//禁止绘制y轴
        //图表显示的是哪种类型的数据，该轴最小值总是0
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(false);//不绘制格网线
        leftAxis.setGridColor(Color.parseColor("#F2F2F2"));    // 网格线颜色，默认GRAY
        leftAxis.setGridLineWidth(1);    // 网格线宽度，dp，默认1dp
        //设置Y轴只绘制轴的标签，不绘制轴线
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawLabels(false);

        barChart.getAxisRight().setEnabled(false);//禁用右侧y轴

        //图例设置
        Legend legend = barChart.getLegend();
        legend.setEnabled(false);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);//图例水平居中
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);//图例在图表上方
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);//图例的方向为水平
        legend.setDrawInside(false);//绘制在chart的外侧
        legend.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);//图例中的文字方向

        legend.setForm(Legend.LegendForm.SQUARE); //图例文字前面的形状
        legend.setTextSize(12f);
        legend.setXEntrySpace(20);  //图例每一项之间的margin
        legend.setYOffset(15);  //图例与底部的margin

        //设置柱状图数据
        setStackChartData(barChart, yAxisValue, title, barColor);

        barChart.setExtraBottomOffset(10);//距视图窗口底部的偏移，类似与paddingbottom
        barChart.setExtraTopOffset(30);//距视图窗口顶部的偏移，类似与paddingtop
        barChart.setFitBars(true);//使两侧的柱图完全显示
        barChart.animateX(1500);//数据显示动画，从左往右依次显示

//        barChart.setExtraOffsets(10, 30, 20, 10);
    }

    private static void setStackChartData(BarChart barChart, List<BarEntry> yAxisValue, String title, Integer barColor) {

        BarDataSet set1;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(yAxisValue);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yAxisValue, title );
            set1.setStackLabels(new String[]{">360天", "180~360天", "90~180天","<90天"}); //图例的值
            //是否显示每个段的值,显示柱状图上方的值
            set1.setDrawValues(true);
            set1.setValueTextColor(Color.parseColor("#333333"));
            if (barColor == null) {
//                set1.setColor(ContextCompat.getColor(barChart.getContext(), R.color.bar));//设置set1的柱的颜色
                set1.setColors(Color.parseColor("#C5D5EB"));//set会循环这里的颜色进行添加

//                set1.setColors(new int[] { Color.parseColor("#C5D5EB"), Color.parseColor("#0E6BF2") });
            } else {
                set1.setColor(barColor);
            }

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.3f);
            data.setValueFormatter(new MyValueFormatter());

            barChart.setData(data);
        }
    }


}