package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;

public class SocketBean implements Serializable {

    /**
     * rtmpUrl : rtmp://10.252.37.102/live/abc
     * msgId : 102
     */

    private String rtmpUrl;
    private int msgId;
    private int code;
    private String msg;

    //添加 发送checkType为0时中止算法检测
    private int checkType;
    private String uuid;


    //后续添加
//    private int checkType;
//    private String clientId;
//    private int costTime;
//    private DataBean data;
//    private long timestamp;
//    private String uuid;


    public String getRtmpUrl() {
        return rtmpUrl;
    }

    public void setRtmpUrl(String rtmpUrl) {
        this.rtmpUrl = rtmpUrl;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public int getCheckType() {
        return checkType;
    }

    public void setCheckType(int checkType) {
        this.checkType = checkType;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
