package com.grgbanking.denali.doublelive.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class ImageUtils {

    //编码base64图片
    public static String encodeImageToString(Bitmap bitmap) {
        //读取图片到ByteArrayOutputStream
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //参数如果为100那么就不压缩
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, outputStream);
        byte[] bytes = outputStream.toByteArray();
        String strImg = Base64.encodeToString(bytes, Base64.DEFAULT);
        return strImg;
    }

    //解码
    public static Bitmap sendImage(String imgAddress) {
        byte[] input = Base64.decode(imgAddress, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(input, 0, input.length);
        return bitmap;
    }


    public static Bitmap getVideoThumbnail(String filePath) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(filePath);
            bitmap = retriever.getFrameAtTime(100); //100为截取0.01秒时的图
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }


    public  static Bitmap getVideoThumb(String path) {

        MediaMetadataRetriever media = new MediaMetadataRetriever();

        media.setDataSource(path);

        return  media.getFrameAtTime();

    }
}
