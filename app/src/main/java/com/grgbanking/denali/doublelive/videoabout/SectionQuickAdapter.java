package com.grgbanking.denali.doublelive.videoabout;


import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.grgbanking.denali.doublelive.utils.GlideImageLoader;
import com.grgbanking.denali.doublelive.R;
import com.grgbanking.denali.doublelive.bean.MySection;
import com.grgbanking.denali.doublelive.bean.VideoItem2;
import com.grgbanking.denali.doublelive.utils.ImageUtils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class SectionQuickAdapter extends BaseSectionQuickAdapter<MySection, BaseViewHolder> {

   private Context context;
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param sectionHeadResId The section head layout id for each item
     * @param layoutResId      The layout resource id of each item.
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public SectionQuickAdapter(int layoutResId, int sectionHeadResId, List<MySection> data, Context context) {
        super(sectionHeadResId, data);
        this.context=context;
        setNormalLayout(layoutResId);

        addChildClickViewIds(R.id.more);
    }

    @Override
    protected void convertHeader(@NotNull BaseViewHolder helper, @NotNull MySection item) {
        if (item.getObject() instanceof String) {
            helper.setText(R.id.header, (String) item.getObject());
        }
    }


    @Override
    protected void convert(@NotNull BaseViewHolder helper, @NotNull MySection item) {
        VideoItem2 videoItem = (VideoItem2) item.getObject();

        helper.setText(R.id.item_policyname_tv,videoItem.getPolicyName());
        helper.setText(R.id.item_insuretype_tv,videoItem.getInsureTypeDesc());
        helper.setText(R.id.item_recordtime_tv,videoItem.getRecordTime());
        helper.setText(R.id.item_duration_tv,videoItem.getDuration());

        if(videoItem.getAuditStatus()==0) {
            helper.setText(R.id.btn_status,"待审核");
            helper.setBackgroundResource(R.id.btn_status,R.drawable.btn_status_review_bg);
            helper.setTextColor(R.id.btn_status, Color.parseColor("#999999"));
        }else if(videoItem.getAuditStatus()==1){
            helper.setText(R.id.btn_status, "已通过");
            helper.setBackgroundResource(R.id.btn_status,R.drawable.btn_status_pass_bg);
            helper.setTextColor(R.id.btn_status, Color.parseColor("#FFFFFF"));
        }else if(videoItem.getAuditStatus()==2){
            helper.setText(R.id.btn_status,"未通过");
            helper.setBackgroundResource(R.id.btn_status,R.drawable.btn_status_nopass_bg);
            helper.setTextColor(R.id.btn_status, Color.parseColor("#FFFFFF"));
        }


        ImageView imgv =(ImageView) helper.getView(R.id.item_video_imgv);
        if(videoItem.getImgUrl()!=null) {
            imgv.setImageBitmap(ImageUtils.sendImage(videoItem.getImgUrl()));
        }else {
            GlideImageLoader.load(getContext(),  videoItem.getVideoUrl(),
                    imgv );
        }

    }
}
