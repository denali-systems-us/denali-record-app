/*
 *     (C) Copyright 2019, ForgetSky.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.grgbanking.denali.doublelive.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


import com.grgbanking.denali.doublelive.base.activity.AbstractSimpleActivity;
import com.grgbanking.denali.doublelive.socketUtil.BaseApplication;
import com.grgbanking.denali.doublelive.utils.EditTextUtils;
import com.grgbanking.denali.doublelive.videoabout.MainPagerActivity;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;
import com.grgbanking.denali.doublelive.R;
import com.grgbanking.denali.doublelive.http.ApiService;
import com.simple.spiderman.SpiderMan;
import com.yanzhenjie.permission.AndPermission;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.ResourceSubscriber;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.utils.ConstantUtil;
import me.lake.librestreaming.utils.ToastUtils;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @date: 2019/3/4
 */
public class LoginActivity extends AbstractSimpleActivity implements CustomAdapt {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mTitle;

    @BindView(R.id.et_username)
    EditText mUsernameEdit;
    @BindView(R.id.et_password)
    EditText mPasswordEdit;

    @BindView(R.id.usename_imgv)
    ImageView usenameImgv;
    @BindView(R.id.usename_tv)
    TextView usenameTv;
    @BindView(R.id.psw_imgv)
    ImageView pswImgv;
    @BindView(R.id.psw_tv)
    TextView pswTv;
    @BindView(R.id.usename_line)
    View usenameLine;
    @BindView(R.id.psw_line)
    View pswLine;
    @BindView(R.id.username_lLayout)
    LinearLayout username_lLayout;
    @BindView(R.id.psw_lLayout)
    LinearLayout psw_lLayout;
    @BindView(R.id.btn_login)
    TextView btn_login;
    @BindView(R.id.setting_imgv)
    ImageView setting_imgv;
    @BindView(R.id.login_activity_layout)
    View parent;
    @BindView(R.id.ip_tv)
    TextView ip_tv;

    private EditText ip;
    private boolean isdialogshow;
    private BaseApplication application;

    private PopupWindow popupWindow;

    private static final int STORAGE_REQUEST_CODE = 1025;
//    private int REQUEST_CODE_SCAN = 1111;

    private ApiService mApiService;

    @Override
    protected void initView() {

        //home键重新启动
        if (!this.isTaskRoot()) {

            Intent mainIntent = getIntent();
            String action = mainIntent.getAction();
            if(mainIntent.hasCategory(Intent.CATEGORY_LAUNCHER) &&
                    action.equals(Intent.ACTION_MAIN)){
                finish();
                return;
            }

        }

        mUsernameEdit.setSelection(mUsernameEdit.getText().length());
        mPasswordEdit.setSelection(mPasswordEdit.getText().length());

        //启动页给IP设置一个默认值
        application= (BaseApplication) getApplication();
        application.setBASE_IP("221.5.109.20");
//        SharedPreferencesUtil.putString(LoginActivity.this, SharedPreferencesUtil.SERVICEIP, "221.5.109.20");
        ip_tv.setText("当前IP: "+application.getBASE_IP()+" 可手动设置");
//        setToolbarTitle(R.string.login);

        initPermission();
    }

    private void initPermission() {
        if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
        }
        AndPermission.with(this)
                .permission(
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                // 准备方法，和 okhttp 的拦截器一样，在请求权限之前先运行改方法，已经拥有权限不会触发该方法
                .rationale((context, permissions, executor) -> {
                    // 此处可以选择显示提示弹窗
                    executor.execute();
                })
                // 用户给权限了
                .onGranted(permissions -> {

                })
                // 用户拒绝权限，包括不再显示权限弹窗也在此列
                .onDenied(permissions -> {
                    // 判断用户是不是不再显示权限弹窗了，若不再显示的话进入权限设置页
                    if (AndPermission.hasAlwaysDeniedPermission(LoginActivity.this, permissions)) {
                        // 打开权限设置页
                        AndPermission.permissionSetting(LoginActivity.this).execute();
                        return;
                    }
                })
                .start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    private void initRetrofit(String ipaddress) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://"+ipaddress+":30055/denali-authentication-server/")//请求url
                //增加转换器，这一步能直接Json字符串转换为实体对象
                .addConverterFactory(GsonConverterFactory.create())
                //加入 RxJava转换器
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        mApiService = retrofit.create(ApiService.class);
    }

    public void setToolbarTitle(int resId) {
        mTitle.setText(resId);
    }

    @Override
    protected void onViewCreated() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

//        mToolbar.setNavigationOnClickListener(v -> onBackPressedSupport());
        mToolbar.setNavigationIcon(null);
    }

    @Override
    protected void initEventAndData() {
        mUsernameEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {


                if(usenameImgv!=null &&usenameTv!=null && usenameLine!=null&& hasFocus){ //获取焦点

                    usenameImgv.setImageResource(R.mipmap.login_user_c);
                    usenameTv.setTextColor(Color.parseColor("#5A5C5C"));
                    usenameLine.setBackgroundColor(Color.parseColor("#5A5C5C"));
                }else if(usenameImgv!=null &&usenameTv!=null && usenameLine!=null){    //失去焦点
                    usenameImgv.setImageResource(R.mipmap.login_user);
                    usenameTv.setTextColor(Color.parseColor("#7E7E7E"));
                    usenameLine.setBackgroundColor(Color.parseColor("#E0E0E0"));
                }else { //添加这个判断主要为了防止回退界面的时候控件为空
                    finish();
                }
            }
        });

        mPasswordEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(pswImgv!=null &&pswTv!=null && pswLine!=null&&hasFocus){ //获取焦点

                    pswImgv.setImageResource(R.mipmap.login_password_c);
                    pswTv.setTextColor(Color.parseColor("#5A5C5C"));
                    pswLine.setBackgroundColor(Color.parseColor("#5A5C5C"));
                }else if(pswImgv!=null &&pswTv!=null && pswLine!=null){    //失去焦点

                    pswImgv.setImageResource(R.mipmap.login_password);
                    pswTv.setTextColor(Color.parseColor("#7E7E7E"));
                    pswLine.setBackgroundColor(Color.parseColor("#E0E0E0"));
                }else {
                    finish();
                }
            }
        });
    }

    @OnClick({R.id.btn_login, R.id.username_lLayout,R.id.psw_lLayout ,R.id.setting_imgv})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
//                initRetrofit( SharedPreferencesUtil.getString(LoginActivity.this,
//                        SharedPreferencesUtil.SERVICEIP, "") );
                initRetrofit( application.getBASE_IP());
                login();
                //隐藏软键盘
                InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.showSoftInput(btn_login,InputMethodManager.SHOW_FORCED);
                    imm.hideSoftInputFromWindow(btn_login.getWindowToken(), 0);
                }
                break;

            case R.id.username_lLayout:
                EditTextUtils.getEdittextFoucs(this,mUsernameEdit);
                break;
            case R.id.psw_lLayout:
                EditTextUtils.getEdittextFoucs(this,mPasswordEdit);
                break;
            case R.id.setting_imgv:
                showSocketDialog();
                break;
            default:
                break;
        }
    }

    private void showSocketDialog() {
        View view = initPopu(R.layout.socket_dialog);
        ip = (EditText) view
                .findViewById(R.id.modify_password_old);

        //弹窗就获取启动页设置的IP默认值
//        ip.setText(SharedPreferencesUtil.getString(LoginActivity.this,
//                SharedPreferencesUtil.SERVICEIP, ""));
        ip.setText(application.getBASE_IP());
        ip_tv.setText("当前IP: "+application.getBASE_IP()+" 可手动设置");

        Button okButton = (Button) view.findViewById(R.id.modify_password_ok);

        try{
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    isdialogshow=true;
                    String ipAddress = ip.getText().toString();
                    //判断输入的是数字还是字母

                    if (!"".equals(ipAddress) ) {
                        //手动改变获取设置的IP值
//                        SharedPreferencesUtil.putString(LoginActivity.this, SharedPreferencesUtil.SERVICEIP, ipAddress);
                        application.setBASE_IP(ipAddress);
                        ip_tv.setText("当前IP: "+application.getBASE_IP()+" 可手动设置");
                        if (popupWindow != null && popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    } else {
                        ToastUtils.showToast(LoginActivity.this, "IP地址不能为空");
                    }
                }
            });

        }catch (Exception e){

            SpiderMan.show(e);
        }

    }

    private View initPopu(int layoutId) {
        View view = View.inflate(this, layoutId, null);
        if (popupWindow == null || !popupWindow.isShowing()) {
            popupWindow = new PopupWindow(view,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            popupWindow.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.transparent));
            popupWindow.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(false);
            popupWindow.update();
        }
        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
        view.findViewById(R.id.popup_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        return view;
    }

    private void login() {

        String username = mUsernameEdit.getText().toString();
        String password = mPasswordEdit.getText().toString();
        LoginSendData loginSendData=new LoginSendData();
        loginSendData.setUsername(username);
        loginSendData.setPassword(password);
        loginSendData.setGrant_type("password");
        loginSendData.setClient_id("3ee23197af02482b998a9a420ce0efb4");
        loginSendData.setClient_secret("44fa0fa289034d558a6d9adab6d0aabf");
        //登录

        mApiService.login(loginSendData)
                .subscribeOn(Schedulers.io())//运行在io线程
                .observeOn(AndroidSchedulers.mainThread())//回调在主线程
                .subscribeWith(new ResourceSubscriber<LoginData>() {


                    @Override
                    public void onNext(LoginData loginData) {
                        int code= loginData.getCode();
                        if (code==0){
                            ToastUtils.showToast(LoginActivity.this,getString(R.string.login_success));

                            String accessToken= loginData.getData().getAccess_token();
                            String refreshToken= loginData.getData().getRefresh_token();
                            SharedPreferencesUtil.putString(LoginActivity.this, SharedPreferencesUtil.LOGINUSERNAME, username);
                            SharedPreferencesUtil.putString(LoginActivity.this, SharedPreferencesUtil.LOGINTOKEN, accessToken);
                            SharedPreferencesUtil.putString(LoginActivity.this, SharedPreferencesUtil.REFRESHTOKEN, refreshToken);

                            //处理点击事件
                            Intent intentMain=new Intent(LoginActivity.this,
                                    MainPagerActivity.class);
                            startActivity(intentMain);

                        }else if(code==-6){
                            ToastUtils.showToast(LoginActivity.this, loginData.getMsg());
                        }
                        else if(code==-7){
                            ToastUtils.showToast(LoginActivity.this, loginData.getMsg());
                        }
                        //结果回调
                        Log.e("login", "onNext: " + loginData);
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        ToastUtils.showToast(LoginActivity.this,getString(R.string.login_fail));
                    }

                    @Override
                    public void onComplete() {
                        Log.e("login", "onComplete: ");
                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isdialogshow) {
            String ipAddress = ip.getText().toString();
            //判断输入的是数字还是字母

            if (!"".equals(ipAddress)) {
                //手动改变获取设置的IP值
                application.setBASE_IP(ipAddress);
                ip_tv.setText("当前IP: " + application.getBASE_IP() + " 可手动设置");
            }
        }
    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}
