package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;

public class ScanBean implements Serializable {


    /**
     * policyNum : 123456789
     * insureType : 1
     * insureTypeDesc : 保险类型1
     * policyName : 张晓红
     * policyCard : 445464564656842185
     * organizationCode : 597456
     * staffNum : 123456
     * staffName : 黄社会
     * staffCard : 4455465612553156656
     */

    private String policyNum;
    private int insureType;
    private String insureTypeDesc;
    private String policyName;
    private String policyCard;
    private String organizationCode;
    private String staffNum;
    private String staffName;
    private String staffCard;

    public String getPolicyNum() {
        return policyNum;
    }

    public void setPolicyNum(String policyNum) {
        this.policyNum = policyNum;
    }

    public int getInsureType() {
        return insureType;
    }

    public void setInsureType(int insureType) {
        this.insureType = insureType;
    }

    public String getInsureTypeDesc() {
        return insureTypeDesc;
    }

    public void setInsureTypeDesc(String insureTypeDesc) {
        this.insureTypeDesc = insureTypeDesc;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getPolicyCard() {
        return policyCard;
    }

    public void setPolicyCard(String policyCard) {
        this.policyCard = policyCard;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getStaffNum() {
        return staffNum;
    }

    public void setStaffNum(String staffNum) {
        this.staffNum = staffNum;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffCard() {
        return staffCard;
    }

    public void setStaffCard(String staffCard) {
        this.staffCard = staffCard;
    }
}
