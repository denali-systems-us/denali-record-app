package com.grgbanking.denali.doublelive;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import me.lake.librestreaming.ws.StreamLiveCameraView;

/**
 * Created by WangShuo on 2018/2/26.
 */

public class LiveUIIDCard implements View.OnClickListener {

    private LiveMainActivity activity;
    private StreamLiveCameraView liveCameraView;
    private String rtmpUrl = "";
    boolean isFilter = false;
    boolean isMirror = false;

    //top_camera_layout
    private TextView result_title;

    private TextView warn_tv;//提示框
    //bottom_camera_layout
    private TextView voice_tv;
    //right_camera_layout
    private LinearLayout check_result_face;
    private LinearLayout check_result_face2face;
    private LinearLayout check_result_reply;
    private ImageView check_face_imgv;
    private ImageView check_face2face_imgv;
    private ImageView check_reply_imgv;
    private TextView check_face_tv;
    private TextView check_face2face_tv;
    private TextView check_reply_tv;

    public LiveUIIDCard(LiveMainActivity liveActivity , StreamLiveCameraView liveCameraView , String rtmpUrl) {
        this.activity = liveActivity;
        this.liveCameraView = liveCameraView;
        this.rtmpUrl = rtmpUrl;

        init();
    }

    private void init() {
        result_title= activity.findViewById(R.id.result_title);
        result_title.setText(R.string.check_idcard_title);

        warn_tv= activity.findViewById(R.id.warn_tv);
        warn_tv.setText(R.string.check_idcard_tip);

        voice_tv= activity.findViewById(R.id.voice_tv);
        voice_tv.setText(R.string.check_idcard);

        check_result_face=(LinearLayout) activity.findViewById(R.id.check_result_face);
        check_result_face2face=(LinearLayout)activity.findViewById(R.id.check_result_face2face);
        check_result_reply=(LinearLayout)activity.findViewById(R.id.check_result_reply);
        check_face_imgv=activity.findViewById(R.id.check_face_imgv);
        check_face2face_imgv=activity.findViewById(R.id.check_face2face_imgv);
        check_reply_imgv=activity.findViewById(R.id.check_reply_imgv);
        check_face_tv= activity.findViewById(R.id.check_face_tv);
        check_face2face_tv= activity.findViewById(R.id.check_face2face_tv);
        check_reply_tv= activity.findViewById(R.id.check_reply_tv);

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.VISIBLE);
        check_result_reply.setVisibility(View.GONE);

        check_face_tv.setText(R.string.check_show_idcard);
        check_face2face_tv.setText(R.string.check_result_idcard);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            default:
                break;
        }
    }
}
