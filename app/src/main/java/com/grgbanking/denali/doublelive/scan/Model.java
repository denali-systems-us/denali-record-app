package com.grgbanking.denali.doublelive.scan;


import java.io.Serializable;
import java.util.List;

public class Model implements Serializable {


	private List<DenaliBean> denali;

	public List<DenaliBean> getDenali() {
		return denali;
	}

	public void setDenali(List<DenaliBean> denali) {
		this.denali = denali;
	}

	public static class DenaliBean {
		public DenaliBean(String name, String id, String menu) {
			this.name = name;
			this.id = id;
			this.menu = menu;
		}

		/**
		 * name : 征求录像意见
		 * id : 01
		 * menu : 代理人人脸验证、投保人明确答复
		 */

		private String name;
		private String id;
		private String menu;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getMenu() {
			return menu;
		}

		public void setMenu(String menu) {
			this.menu = menu;
		}
	}
}
