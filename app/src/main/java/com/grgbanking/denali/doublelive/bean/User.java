package com.grgbanking.denali.doublelive.bean;

public class User {

    private static User mUser;

    private String token;

    public static User get() {
        if (mUser == null) {
            synchronized (User.class) {
                if (mUser == null)
                    mUser = new User();
            }
        }
        return mUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
