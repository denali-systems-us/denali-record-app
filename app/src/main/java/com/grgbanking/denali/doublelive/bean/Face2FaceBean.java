package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;
import java.util.List;

public class Face2FaceBean implements Serializable {
    /**
     * checkType : 3
     * clientId : 0242acfffe110014-00000001-00007938-7b251848d5d4e42e-145c9d8d
     * code : 1
     * costTime : 341
     * data : {"boxes":[[724.5817884206772,189.9209387898445,868.3585328161716,362.9463322907686,0.981044352054596],[532.6879130452871,584.7226200252771,581.725753724575,645.1901800855994,0.8998130559921265]],"num":2,"status":0}
     * msg : 同框成功
     * msgId : 102
     * timestamp : 1587709454465
     * uuid : 6a31900287284119a9f15ecf65a3d171
     */

    private int checkType;
    private String clientId;
    private int code;
    private int costTime;
    private DataBean data;
    private String msg;
    private int msgId;
    private long timestamp;
    private String uuid;

    public int getCheckType() {
        return checkType;
    }

    public void setCheckType(int checkType) {
        this.checkType = checkType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCostTime() {
        return costTime;
    }

    public void setCostTime(int costTime) {
        this.costTime = costTime;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public static class DataBean {
        /**
         * boxes : [[724.5817884206772,189.9209387898445,868.3585328161716,362.9463322907686,0.981044352054596],[532.6879130452871,584.7226200252771,581.725753724575,645.1901800855994,0.8998130559921265]]
         * num : 2
         * status : 0
         */

        private int num;
        private int status;
        private List<List<Float>> boxes;

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public List<List<Float>> getBoxes() {
            return boxes;
        }

        public void setBoxes(List<List<Float>> boxes) {
            this.boxes = boxes;
        }
    }


    /*
    *//**
     * checkType : 1
     * code : 1
     * data : {"boxes":[[346.4214754104614,171.07952499389648,402.552396774292,239.25860595703125,91.91313982009888]],"gesturetype":"","status":0}
     * msgId : 103
     *//*

    private int checkType;
    private int code;
    private DataBean data;
    private int msgId;

    public int getCheckType() {
        return checkType;
    }

    public void setCheckType(int checkType) {
        this.checkType = checkType;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public static class DataBean {
        *//**
         * boxes : [[346.4214754104614,171.07952499389648,402.552396774292,239.25860595703125,91.91313982009888]]
         * gesturetype :
         * status : 0
         *//*

        private String gesturetype;
        private int status;
        private List<List<Float>> boxes;

        public String getGesturetype() {
            return gesturetype;
        }

        public void setGesturetype(String gesturetype) {
            this.gesturetype = gesturetype;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public List<List<Float>> getBoxes() {
            return boxes;
        }

        public void setBoxes(List<List<Float>> boxes) {
            this.boxes = boxes;
        }
    }*/
}
