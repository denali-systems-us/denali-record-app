package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;

public class Face2FaceBeanOld implements Serializable {


    /**
     * checkType : 3
     * code : -1
     * data : {"num":"1","status":"1"}
     * msgId : 103
     */

    private int checkType;
    private int code;
    private DataBean data;
    private int msgId;

    public int getCheckType() {
        return checkType;
    }

    public void setCheckType(int checkType) {
        this.checkType = checkType;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public static class DataBean {
        /**
         * num : 1
         * status : 1
         */

        private String num;
        private String status;

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
