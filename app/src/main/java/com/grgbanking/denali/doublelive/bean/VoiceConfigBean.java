package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;
import java.util.List;

public class VoiceConfigBean implements Serializable {

    /**
     * code : 0
     * msg : 成功
     * data : {"voiceList":[{"name":"1/4 征求录像意见","id":"01","voiceDesc":"尊敬的投保人，您好！为了维护您的权益，根据银保监会的要求，我们将以录音录像方式对我的销售过程关键环节予以记录。请问您是否同意？"},{"name":"2/4 代理人员出示证件","id":"02","voiceDesc":"请代理人向您出示证件，并在镜头前展示三秒。"},{"name":"3/4 责任确认","id":"03","voiceDesc":"本次投保的产品条款已向你介绍完毕,请问你是否已清楚产品的保险责任和免除责任?"},{"name":"4/4 免责条款签字","id":"04","voiceDesc":"请你仔细阅读《产品条款责任免除内容知情同意书》，并签名确认，完成后由代理人在镜头前展示三秒。操作完成后，此次录音录像到此结束，感谢您的配合。"}],"total":4}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * voiceList : [{"name":"1/4 征求录像意见","id":"01","voiceDesc":"尊敬的投保人，您好！为了维护您的权益，根据银保监会的要求，我们将以录音录像方式对我的销售过程关键环节予以记录。请问您是否同意？"},{"name":"2/4 代理人员出示证件","id":"02","voiceDesc":"请代理人向您出示证件，并在镜头前展示三秒。"},{"name":"3/4 责任确认","id":"03","voiceDesc":"本次投保的产品条款已向你介绍完毕,请问你是否已清楚产品的保险责任和免除责任?"},{"name":"4/4 免责条款签字","id":"04","voiceDesc":"请你仔细阅读《产品条款责任免除内容知情同意书》，并签名确认，完成后由代理人在镜头前展示三秒。操作完成后，此次录音录像到此结束，感谢您的配合。"}]
         * total : 4
         */

        private int total;
        private List<VoiceListBean> voiceList;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<VoiceListBean> getVoiceList() {
            return voiceList;
        }

        public void setVoiceList(List<VoiceListBean> voiceList) {
            this.voiceList = voiceList;
        }

        public static class VoiceListBean implements Serializable{
            /**
             * name : 1/4 征求录像意见
             * id : 01
             * voiceDesc : 尊敬的投保人，您好！为了维护您的权益，根据银保监会的要求，我们将以录音录像方式对我的销售过程关键环节予以记录。请问您是否同意？
             */

            private String name;
            private String id;
            private String voiceDesc;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getVoiceDesc() {
                return voiceDesc;
            }

            public void setVoiceDesc(String voiceDesc) {
                this.voiceDesc = voiceDesc;
            }
        }
    }
}
