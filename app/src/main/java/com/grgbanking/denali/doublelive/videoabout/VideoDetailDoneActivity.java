package com.grgbanking.denali.doublelive.videoabout;

import android.Manifest;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;
import com.grgbanking.denali.doublelive.bean.FailListBean;
import com.grgbanking.denali.doublelive.socketUtil.BaseApplication;
import com.grgbanking.denali.doublelive.utils.DataFormatUtil;
import com.grgbanking.denali.doublelive.utils.GlideImageLoader;
import com.grgbanking.denali.doublelive.utils.HomeKeyWatcher;
import com.grgbanking.denali.doublelive.utils.SimpleDividerDecoration;
import com.king.zxing.CaptureActivity;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;
import com.mylhyl.circledialog.CircleDialog;
import com.grgbanking.denali.doublelive.R;
import com.grgbanking.denali.doublelive.base.activity.AbstractSimpleActivity;
import com.grgbanking.denali.doublelive.bean.ScanBean;
import com.grgbanking.denali.doublelive.http.ApiService;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.utils.FileUtils;
import com.grgbanking.denali.doublelive.scan.ScanActivity2;
import com.grgbanking.denali.doublelive.utils.ImageUtils;
import com.xiao.nicevideoplayer.NiceVideoPlayer;
import com.xiao.nicevideoplayer.NiceVideoPlayerManager;
import com.xiao.nicevideoplayer.TxVideoPlayerController;
import com.yanzhenjie.permission.AndPermission;
//import com.yzq.zxinglibrary.android.CaptureActivity;
//import com.yzq.zxinglibrary.bean.ZxingConfig;
//import com.yzq.zxinglibrary.common.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.utils.ClickUtil;
import me.lake.librestreaming.utils.ConstantUtil;
import me.lake.librestreaming.utils.SharedPreferencesMediaUtil;
import me.lake.librestreaming.utils.ToastUtils;
import rxhttp.wrapper.param.RxHttp;

/**
 *
 * 录制完成详情页
 */
public class VideoDetailDoneActivity extends AbstractSimpleActivity implements CustomAdapt {

    /**----------------标题栏------------------**/
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mTitle;
    @BindView(R.id.record_oncemore_tv)
    TextView record_oncemore_tv;

    /**----------------main_info------------------**/
    @BindView(R.id.duration_tv)
    TextView duration_tv;
    @BindView(R.id.fail_msg_tv)
    TextView fail_msg_tv;

    @BindView(R.id.scan_info_num)
    TextView scan_info_num;
    @BindView(R.id.scan_info_type)
    TextView scan_info_type;
    @BindView(R.id.video_status)
    TextView video_status;

    @BindView(R.id.scan_info_name)
    TextView scan_info_name;
    @BindView(R.id.scan_info_id_num)
    TextView scan_info_id_num;
    @BindView(R.id.record_time_tv)
    TextView record_time_tv;
    @BindView(R.id.upload_time_tv)
    TextView upload_time_tv;
    @BindView(R.id.check_time_tv)
    TextView check_time_tv;
    @BindView(R.id.btn_uploadlater)
    TextView btn_uploadlater;
    @BindView(R.id.btn_uploadnow)
    TextView btn_uploadnow;

    @BindView(R.id.recorddone_recycle)
    RecyclerView mRecyclerView;

    @BindView(R.id.fail_node_lLayout)
    LinearLayout fail_node_lLayout;
    @BindView(R.id.fail_lLayout)
    LinearLayout fail_lLayout;
    @BindView(R.id.status_lLayout)
    LinearLayout status_lLayout;
    @BindView(R.id.check_time_lLayout)
    LinearLayout check_time_lLayout;

//    private ScanAdapter mAdapter;
    private FailListdapter mAdapter;

    @BindView(R.id.nice_video_player)
    NiceVideoPlayer mNiceVideoPlayer;

    private boolean pressedHome;
    private HomeKeyWatcher mHomeKeyWatcher;

    private String loginToken;
    private String loginName;
    private String videoUrl;
    private String imgUrl;
    private String duration;
    private String videoName;

    private String scanValue;
    private String result;
    private String recordTime;
    private ScanBean scanBean;
    private List<FailListBean> failList=new ArrayList<>();
    private CircleDialog.Builder builder;
    private CircleDialog.Builder builderToHome;
    Timer timer;
    int currentProgress;
    private int REQUEST_CODE_SCAN = 1115;

    // 数据库变量
    private UploadSQLiteOpenHelper helper;
    private SQLiteDatabase db;

    private boolean isContinuousScan;
    private int REQUEST_CODE_SCAN2 = 1123;

    @Override
    protected void initView() {
        setToolbarTitle(R.string.video_detail_title);

        scanValue = getIntent().getStringExtra("scanValue");
        result = getIntent().getStringExtra("result");
        recordTime = getIntent().getStringExtra("recordTime");
        failList =  (List<FailListBean>) getIntent().getSerializableExtra("failList");

        loginToken = SharedPreferencesUtil.getString(VideoDetailDoneActivity.this, SharedPreferencesUtil.LOGINTOKEN, "");
        loginName = SharedPreferencesUtil.getString(VideoDetailDoneActivity.this, SharedPreferencesUtil.LOGINUSERNAME, "");
        videoUrl = SharedPreferencesMediaUtil.getString(VideoDetailDoneActivity.this, SharedPreferencesMediaUtil.RECORDVIDEOPATH, "");
        videoName = SharedPreferencesMediaUtil.getString(VideoDetailDoneActivity.this, SharedPreferencesMediaUtil.RECORDVIDEONAME, "");

        fail_lLayout.setVisibility(View.GONE);
        status_lLayout.setVisibility(View.GONE);
        check_time_lLayout.setVisibility(View.GONE);

        Gson gson = new Gson();
        scanBean = gson.fromJson(scanValue, ScanBean.class);
        scan_info_num.setText(scanBean.getPolicyNum());
        scan_info_type.setText(scanBean.getInsureTypeDesc());
        scan_info_name.setText(scanBean.getPolicyName());
        scan_info_id_num.setText(scanBean.getPolicyCard());
        record_time_tv.setText(recordTime);
        upload_time_tv.setText(DataFormatUtil.getDateStrIncludeTimeZone());

        // 2. 实例化数据库SQLiteOpenHelper子类对象
//        helper = new UploadSQLiteOpenHelper(VideoDetailDoneActivity.this);
        helper = new UploadSQLiteOpenHelper(VideoDetailDoneActivity.this,
                "upload.db", null, 1);

        //加载内容
        if (failList!=null && failList.size()!=0){
            fail_node_lLayout.setVisibility(View.VISIBLE);
            initRecyclerView();
        }else {
            fail_node_lLayout.setVisibility(View.GONE);
        }
        //初始化视频播放器
        initVideoPlayer();

    }

    private void initVideoPlayer() {
        mHomeKeyWatcher = new HomeKeyWatcher(this);
        mHomeKeyWatcher.setOnHomePressedListener(new HomeKeyWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                pressedHome = true;
            }
        });
        pressedHome = false;
        mHomeKeyWatcher.startWatch();

        mNiceVideoPlayer.setPlayerType(NiceVideoPlayer.TYPE_IJK); // IjkPlayer or MediaPlayer
//        String videoUrl = "http://tanzi27niu.cdsb.mobi/wps/wp-content/uploads/2017/05/2017-05-17_17-33-30.mp4";
        mNiceVideoPlayer.setUp(videoUrl, null);
        TxVideoPlayerController controller = new TxVideoPlayerController(this);
        controller.setTitle("");
//        controller.setLenght(8000);
        if(FileUtils.getPlayTime(videoUrl)!=null ){
            long playTime = Long.parseLong(FileUtils.getPlayTime(videoUrl));
            duration = FileUtils.timeParse(playTime);
            controller.setLenght(playTime);

            imgUrl= ImageUtils.encodeImageToString(ImageUtils.getVideoThumb(videoUrl));

        }else {
            controller.setLenght(0);
        }

        GlideImageLoader.load(this, videoUrl,
                controller.imageView() );
        mNiceVideoPlayer.setController(controller);
    }

    private void initRecyclerView() {
        mAdapter = new FailListdapter(R.layout.item_videodetail_done_list, failList);
        // 一行代码搞定（默认为渐显效果）
        mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.AlphaIn);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(VideoDetailDoneActivity.this));
        mRecyclerView.setHasFixedSize(true);
        //添加分割线
        mRecyclerView.addItemDecoration( new SimpleDividerDecoration(VideoDetailDoneActivity.this));
        mRecyclerView.setAdapter(mAdapter);

    }

    public void setToolbarTitle(int resId) {
        mTitle.setText(resId);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_detail_recorddone;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    @Override
    protected void initEventAndData() {

    }

    @OnClick({R.id.btn_uploadlater,R.id.btn_uploadnow,R.id.record_oncemore_tv})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_uploadlater: //以后上传
                if (ClickUtil.isNotFastClick()) {
                    /**
                     * 设置一个状态值用来区分以后上传，现在上传
                     * 以后上传：
                     */
                    ConstantUtil.getInstance().setUploadToMain(false);

                    ConstantUtil.getInstance().setNeedFilter(false);
                    //以后上传的话需要把录制的节点信息存储到数据库
                    insertData();

                    //以后上传,跳转到主页
                    Intent intentMain=new Intent(VideoDetailDoneActivity.this,
                            MainPagerActivity.class);
                    startActivity(intentMain);
                    finish();
                }
                break;

            case R.id.record_oncemore_tv: //重录
                //删除录制的视频，跳转到扫码界面
                if (ClickUtil.isNotFastClick()) {
                    ConstantUtil.getInstance().setUploadToMain(false);

                    SpinKitView spinKitView=findViewById(R.id.spin_kit);
                    spinKitView.setVisibility(View.VISIBLE);
                    //以后上传,跳转到主页
                    File file=new File(videoUrl);
                    if(file.exists()){
                        file.delete();
                    }
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                    }
                    Intent intentMain=new Intent(VideoDetailDoneActivity.this,
                            MainPagerActivity.class);
                    startActivity(intentMain);
                    finish();
                    //跳转到扫码界面会有问题，finish()，比如点击扫码的关闭按钮,已经删除的文件又可以上传
//                    initScan();

                }
                break;
            case R.id.btn_uploadnow: //现在上传
                if (ClickUtil.isNotFastClick()) {
                    ConstantUtil.getInstance().setUploadToMain(false);

                    uploadAndProgress();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 关注4
     * 插入数据到数据库
     */
    private void insertData() {
        //values里的值不要存在空格，不然插入的数据前面会有空格
        db = helper.getWritableDatabase();
        db.execSQL("insert into uploads(videoName,scanValue,result,recordTime,videoUrl,loginName) " +
                "values('"+videoName+"','"+scanValue+"','"+result+"','"+recordTime+"','"+videoUrl+"','"+loginName+"')");
        db.close();
    }

//    File file = new File("/storage/emulated/0/视频.mp4");

    //上传文件，带进度
    public void uploadAndProgress() {

        try {

//            Gson gson = new Gson();
//            ScanBean scanBean = gson.fromJson(scanValue, ScanBean.class);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("policyNum", scanBean.getPolicyNum());
            jsonObject.put("insureType", scanBean.getInsureType());
            jsonObject.put("insureTypeDesc", scanBean.getInsureTypeDesc());
            jsonObject.put("policyName", scanBean.getPolicyName());
            jsonObject.put("policyCard", scanBean.getPolicyCard() );
            jsonObject.put("organizationCode", scanBean.getOrganizationCode() );
            jsonObject.put("staffNum", scanBean.getStaffNum());
            jsonObject.put("staffName", scanBean.getStaffName());
            jsonObject.put("staffCard", scanBean.getStaffCard());
            jsonObject.put("recordTime", recordTime);
            jsonObject.put("duration", duration);
            jsonObject.put("videoUrl", videoUrl);
            jsonObject.put("imgUrl", imgUrl); //base64的图片;
            jsonObject.put("result", result );

//            RxHttp.postForm(ApiService.BASE_URL_cloud +"api/v1/video/upLoadVideoForCheck")
            RxHttp.postForm(BaseApplication.getInstance().getBASE_URL_cloud() +"api/v1/video/upLoadVideoForCheck")
                    .add("data", jsonObject.toString())//添加参数，非必须
                    .addHeader("Authorization", "Bearer " + loginToken)
                    .addHeader("Content-Type", "multipart/form-data")
                    .addFile("file", videoUrl)
                    .upload(progress -> {
                        //上传进度回调,0-100，仅在进度有更新时才会回调
                        currentProgress = progress.getProgress(); //当前进度 0-100
                        long currentSize = progress.getCurrentSize(); //当前已上传的字节大小
                        long totalSize = progress.getTotalSize();     //要上传的总字节大小
                    }, AndroidSchedulers.mainThread()) //指定回调(进度/成功/失败)线程,不指定,默认在请求所在线程回调
                    .asString()
//                    .asParser(new ResponseParser<Response<UploadBean>>())
                    .doOnSubscribe(disposable -> {
                        //请求开始

                        final int max = 100;

                        timer = new Timer();
                        builder = new CircleDialog.Builder();
                        builder.setCancelable(false).setCanceledOnTouchOutside(false)
                                //.setTypeface(typeface)
                                .configDialog(params -> params.backgroundColor = Color.WHITE)
                                .setTitle("上传")
                                .setProgressText("正在上传")
//                                .setProgress(max, currentProgress)
//                        .setProgressText("已经下载%s了")
//                        .setProgressDrawable(R.drawable.bg_progress_h)
//                                .setNegative("取消", v -> timer.cancel())
                                .show(getSupportFragmentManager());
                        TimerTask timerTask = new TimerTask() {

                            @Override
                            public void run() {
                                //有一种断网重连情况，
                                if (currentProgress >= max) {
                                    VideoDetailDoneActivity.this.runOnUiThread(() -> {
                                        builder.setProgressText("上传视频完成").refresh();
                                        timer.cancel();
                                    });
                                } else {
                                    builder.setProgress(max, currentProgress).refresh();
                                }
                            }
                        };
                        timer.schedule(timerTask, 0, 500);

                    })
                    .doFinally(() -> {
                        //请求结束，不管成功/失败都会回调
                    })
//                    .to(RxLife.to(this))               //加入感知生命周期的观察者
                    .subscribe(s -> {
                        //上传成功
                        builder.dismiss();
                        /**
                         * 设置一个状态值用来区分以后上传，现在上传
                         * 上传成功设置的状态值
                         */
                        ConstantUtil.getInstance().setNeedFilter(true);

                        builderToHome = new CircleDialog.Builder();
                        builderToHome.setCancelable(false).setCanceledOnTouchOutside(false)
                                //.setTypeface(typeface)
                                .configDialog(params -> params.backgroundColor = Color.WHITE)
                                .setTitle("上传已完成，将跳转到首页")
                                .setNegative("确定", v -> startToMainPagerActivity())
                                .show(getSupportFragmentManager());


                    }, throwable -> {
                        //上传失败
                        builder.dismiss();
                        ToastUtils.showToast(VideoDetailDoneActivity.this, "上传失败");
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void startToMainPagerActivity() {

        //立即上传不需要保存数据到数据库
//        insertData();

        Intent intentMain=new Intent(VideoDetailDoneActivity.this,
                MainPagerActivity.class);
        startActivity(intentMain);
        finish();
    }

    @Override
    protected void onRestart() {
        mHomeKeyWatcher.startWatch();
        pressedHome = false;
        super.onRestart();
        NiceVideoPlayerManager.instance().resumeNiceVideoPlayer();
    }

    @Override
    protected void onStop() {
        // 在OnStop中是release还是suspend播放器，需要看是不是因为按了Home键
        if (pressedHome) {
            NiceVideoPlayerManager.instance().suspendNiceVideoPlayer();
        } else {
            NiceVideoPlayerManager.instance().releaseNiceVideoPlayer();
        }
        super.onStop();
        mHomeKeyWatcher.stopWatch();
    }

    @Override
    public void onBackPressedSupport() {
        if (NiceVideoPlayerManager.instance().onBackPressd()) return;
        super.onBackPressedSupport();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // 扫描二维码/条码回传
//        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
//            if (data != null) {
//
//                String content = data.getStringExtra(Constant.CODED_CONTENT);
//                //扫码成功返回更新布局到保单信息
//                Intent intentScan = new Intent(VideoDetailDoneActivity.this,
//                        ScanActivity2.class);
//                intentScan.putExtra("scanValue", content);
//                startActivity(intentScan);
//                setResult(RESULT_OK, intentScan);
//
//                finish();
//
//            }
//        }
    }


    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}
