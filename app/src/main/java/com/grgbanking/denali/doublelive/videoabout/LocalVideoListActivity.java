package com.grgbanking.denali.doublelive.videoabout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.grgbanking.denali.doublelive.utils.DataServer;
import com.grgbanking.denali.doublelive.R;
import com.grgbanking.denali.doublelive.base.activity.AbstractSimpleActivity;
import com.grgbanking.denali.doublelive.bean.VideoItem;

import java.util.List;

import butterknife.BindView;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.utils.ToastUtils;


/**
 */
public class LocalVideoListActivity extends AbstractSimpleActivity implements CustomAdapt {

    /**----------------标题栏------------------**/
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mTitle;

    private static final int PAGE_SIZE = 5;

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LoadMoreAdapter    mAdapter;

    private String status;

    private PageInfo pageInfo = new PageInfo();

    class PageInfo {
        int page = 0;

        void nextPage() {
            page++;
        }

        void reset() {
            page = 0;
        }

        boolean isFirstPage() {
            return page == 0;
        }
    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.video_list);
        status = getIntent().getStringExtra("status");

        mRecyclerView = findViewById(R.id.rv_list);
        mSwipeRefreshLayout = findViewById(R.id.swipeLayout);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        initAdapter();
        initRefreshLayout();
        initLoadMore();

        // 进入页面，刷新数据
        mSwipeRefreshLayout.setRefreshing(true);
        refresh();
    }


    private void initAdapter() {
        mAdapter = new LoadMoreAdapter();
        // 一行代码搞定（默认为渐显效果）
        mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.AlphaIn);
        mAdapter.setAnimationEnable(true);
        mRecyclerView.setAdapter(mAdapter);
        //item点击
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                Intent intentDetail=new Intent(LocalVideoListActivity.this,
                        VideoDetailActivity.class);
                intentDetail.putExtra("status", status);
                startActivity(intentDetail);

            }
        });
        //item想里面的子项点击
        mAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
                ToastUtils.showToast(LocalVideoListActivity.this,
                        "子项"+position);
            }
        });
    }

    private void initRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
    }

    /**
     * 初始化加载更多
     */
    private void initLoadMore() {
        mAdapter.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });
        mAdapter.getLoadMoreModule().setAutoLoadMore(true);
        //当自动加载开启，同时数据不满一屏时，是否继续执行自动加载更多(默认为true)
        mAdapter.getLoadMoreModule().setEnableLoadMoreIfNotFullPage(false);
    }

    /**
     * 刷新
     */
    private void refresh() {
        // 这里的作用是防止下拉刷新的时候还可以上拉加载
        mAdapter.getLoadMoreModule().setEnableLoadMore(false);
        // 下拉刷新，需要重置页数
        pageInfo.reset();
        request();
    }

    /**
     * 加载更多
     */
    private void loadMore() {
        request();
    }

    /**
     * 请求数据
     */
    private void request() {
        new Request(pageInfo.page, new RequestCallBack() {
            @Override
            public void success(List<VideoItem> data) {
                mSwipeRefreshLayout.setRefreshing(false);
                mAdapter.getLoadMoreModule().setEnableLoadMore(true);

                if (pageInfo.isFirstPage()) {
                    //如果是加载的第一页数据，用 setData()
                    mAdapter.setList(data);
                } else {
                    //不是第一页，则用add
                    mAdapter.addData(data);
                }

                if (data.size() < PAGE_SIZE) {
                    //如果不够一页,显示没有更多数据布局
                    mAdapter.getLoadMoreModule().loadMoreEnd();
                } else {
                    mAdapter.getLoadMoreModule().loadMoreComplete();
                }

                // page加一
                pageInfo.nextPage();
            }

            @Override
            public void fail(Exception e) {
                mSwipeRefreshLayout.setRefreshing(false);
                mAdapter.getLoadMoreModule().setEnableLoadMore(true);

                mAdapter.getLoadMoreModule().loadMoreFail();
            }
        }).start();
    }


    /**
     * 模拟加载数据的类，不用特别关注
     */
    static class Request extends Thread {
        private int             mPage;
        private RequestCallBack mCallBack;
        private Handler         mHandler;

        private static boolean mFirstPageNoMore;
        private static boolean mFirstError = true;

        public Request(int page, RequestCallBack callBack) {
            mPage = page;
            mCallBack = callBack;
            mHandler = new Handler(Looper.getMainLooper());
        }

        @Override
        public void run() {
            try {
                Thread.sleep(800);
            } catch (InterruptedException e) {
            }

            if (mPage == 2 && mFirstError) {
                mFirstError = false;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mCallBack.fail(new RuntimeException("fail"));
                    }
                });
            }
            else {
                int size = PAGE_SIZE;
                if (mPage == 1) {
                    if (mFirstPageNoMore) {
                        size = 1;
                    }
                    mFirstPageNoMore = !mFirstPageNoMore;
                    if (!mFirstError) {
                        mFirstError = true;
                    }
                } else if (mPage == 4) {
                    size = 1;
                }

                final int dataSize = size;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
//                        mCallBack.success(DataServer.getSampleData(dataSize));
                        mCallBack.success(DataServer.getVideoListData(1));
                    }
                });
            }
        }
    }

    interface RequestCallBack {
        /**
         * 模拟加载成功
         *
         * @param data 数据
         */
        void success(List<VideoItem> data);

        /**
         * 模拟加载失败
         *
         * @param e 错误信息
         */
        void fail(Exception e);
    }

    public void setToolbarTitle(int resId) {
        mTitle.setText(resId);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_load_more_videolist;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mToolbar.setNavigationOnClickListener(v -> onBackPressedSupport());
    }

    @Override
    protected void initEventAndData() {
    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}