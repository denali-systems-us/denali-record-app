/*
 *     (C) Copyright 2019, ForgetSky.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.grgbanking.denali.doublelive.base.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.android.debug.hv.ViewServer;
import com.classic.common.MultipleStatusView;
import com.grgbanking.denali.doublelive.utils.CommonUtils;
import com.gyf.barlibrary.ImmersionBar;
import com.grgbanking.denali.doublelive.BuildConfig;
import com.grgbanking.denali.doublelive.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.yongxing.lib.StatusBarBlackOnWhiteUtil;
import me.yokeyword.fragmentation.SupportActivity;
/**
 * create by li
 */
public abstract class AbstractSimpleActivity extends SupportActivity {

    private Unbinder unBinder;
    private MultipleStatusView mMultipleStatusView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ImmersionBar.with(this)
                .statusBarView(findViewById(R.id.status_bar_view))
                .keyboardEnable(true)
                .init();

        unBinder = ButterKnife.bind(this);
        // 系统 6.0 以上 状态栏白底黑字的实现方法
        StatusBarBlackOnWhiteUtil.setStatusBarColorAndFontColor(this);

        onViewCreated();
        initToolbar();
        initView();
        initEventAndData();
        if (BuildConfig.DEBUG) {
            ViewServer.get(this).addWindow(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ImmersionBar.with(this).destroy(); //必须调用该方法，防止内存泄漏
        if (unBinder != null && unBinder != Unbinder.EMPTY) {
            unBinder.unbind();
            unBinder = null;
        }
        if (BuildConfig.DEBUG) {
            ViewServer.get(this).removeWindow(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) {
            ViewServer.get(this).setFocusedWindow(this);
        }
    }

    protected abstract void initView();

    /**
     * 在initEventAndData()之前执行
     */
    protected abstract void onViewCreated();

    /**
     * 获取当前Activity的UI布局
     *
     * @return 布局id
     */
    protected abstract int getLayoutId();

    /**
     * 初始化ToolBar
     */
    protected abstract void initToolbar();

    /**
     * 初始化数据
     */
    protected abstract void initEventAndData();

    @Override
    public void onBackPressedSupport() {
        CommonUtils.hideKeyBoard(this, this.getWindow().getDecorView().getRootView());
        super.onBackPressedSupport();
    }

}
