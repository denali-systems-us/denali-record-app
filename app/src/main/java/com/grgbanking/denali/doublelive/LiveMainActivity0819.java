package com.grgbanking.denali.doublelive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.aiuisdk.BaseSpeechCallback;
import com.aiuisdk.SpeechManager;
import com.alibaba.fastjson.JSONArray;
import com.baidu.tts.auth.AuthInfo;
import com.baidu.tts.client.SpeechError;
import com.baidu.tts.client.SpeechSynthesizer;
import com.baidu.tts.client.SpeechSynthesizerListener;
import com.baidu.tts.client.TtsMode;
import com.google.gson.Gson;
import com.grgbanking.denali.doublelive.bean.Face2FaceBean;
import com.grgbanking.denali.doublelive.bean.FailListBean;
import com.grgbanking.denali.doublelive.bean.ScanBean;
import com.grgbanking.denali.doublelive.bean.SocketBean;
import com.grgbanking.denali.doublelive.bean.UploadSendBean;
import com.grgbanking.denali.doublelive.bean.VoiceConfigBean;
import com.grgbanking.denali.doublelive.http.ApiService;
import com.grgbanking.denali.doublelive.socketUtil.BaseApplication;
import com.grgbanking.denali.doublelive.socketUtil.RxSocketManager;
import com.grgbanking.denali.doublelive.socketUtil.SocketType;
import com.grgbanking.denali.doublelive.utils.CircleImageView;
import com.grgbanking.denali.doublelive.utils.DataFormatUtil;
import com.grgbanking.denali.doublelive.utils.InputBug;
import com.grgbanking.denali.doublelive.utils.JsonUtils;
import com.grgbanking.denali.doublelive.utils.MyTextView;
import com.grgbanking.denali.doublelive.utils.NetSpeedTimer;
import com.grgbanking.denali.doublelive.videoabout.MainPagerActivity;
import com.grgbanking.denali.doublelive.videoabout.VideoDetailDoneActivity;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;
import com.simple.spiderman.SpiderMan;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.core.listener.RESConnectionListener;
import me.lake.librestreaming.core.listener.RESScreenShotListener;
import me.lake.librestreaming.filter.hardvideofilter.BaseHardVideoFilter;
import me.lake.librestreaming.utils.ClickUtil;
import me.lake.librestreaming.utils.ConstantUtil;
import me.lake.librestreaming.utils.SharedPreferencesMediaUtil;
import me.lake.librestreaming.utils.StringUtil;
import me.lake.librestreaming.utils.ToastUtils;
import me.lake.librestreaming.ws.StreamAVOption;
import me.lake.librestreaming.ws.StreamLiveCameraView;

/**
 * create by li
 * 录制主流程
 */
public class LiveMainActivity0819 extends AppCompatActivity implements View.OnClickListener,
        CustomAdapt {

    //初始化Logger
    protected static Logger logger = Logger.getLogger(LiveMainActivity0819.class);

    private static final String TAG = LiveMainActivity0819.class.getSimpleName();
    private StreamLiveCameraView mLiveCameraView;
    private StreamAVOption streamAVOption;

    private LiveUIIDCard mLiveUIIDCard;
    private LiveUIProduct mLiveUIProduct;

    private boolean isTCP = true;//默认 TCP   UDP
    private boolean isConnected;

    boolean isFilter = false;
    boolean isMirror = false;
    //是否显示获取推流地址的对话框
    boolean issendBtn = false;

    private PopupWindow popupWindow;
    private long exitTime = 0;


    @BindView(R.id.btn_startStreaming)
    Button btnStartStreaming;
    @BindView(R.id.btn_stopStreaming)
    Button btnStopStreaming;
    @BindView(R.id.btn_startRecord)
    Button btnStartRecord;
    @BindView(R.id.btn_stopRecord)
    Button btnStopRecord;
    @BindView(R.id.btn_filter)
    Button btnFliter;
    @BindView(R.id.btn_swapCamera)
    ImageView btnSwapCamera;
    @BindView(R.id.btn_screenshot)
    Button btnScreenshot;
    @BindView(R.id.btn_mirror)
    ImageView btnMirror;
    @BindView(R.id.btn_socket)
    Button btnSocket;
    @BindView(R.id.btn_getUrl)
    Button btngetUrl;

    @BindView(R.id.iv_image)
    ImageView imageView;

    @BindView(R.id.msg)
    TextView msg;

    @BindView(R.id.net)
    TextView net;

    @BindView(R.id.btn_record)
    TextView btnRecord;
    @BindView(R.id.btn_next)
    TextView btnNext;
    @BindView(R.id.btn_success)
    TextView btnSuccess;

    @BindView(R.id.main_live)
    View parent;

    @BindView(R.id.surfaceview_live)
    SurfaceView surfaceview_live;
    //提示用语
    @BindView(R.id.speech_tips)
    TextView speech_tips;

    @BindView(R.id.time_face_tv)
    TextView time_face_tv;
    @BindView(R.id.showMsg_tv)
    TextView showMsg_tv;
    @BindView(R.id.countdown_tv)
    TextView countdown_tv;

    private NetSpeedTimer mNetSpeedTimer;

    //normal
    @BindView(R.id.warn_tv)
    TextView warn_tv;
    @BindView(R.id.main_camera_layout)
    View main_camera_layout;
    @BindView(R.id.right_camera_layout)
    View right_camera_layout;
    @BindView(R.id.bottom_camera_layout)
    View bottom_camera_layout;
    @BindView(R.id.success_camera_layout)
    View success_camera_layout;

    //top_camera_layout
    @BindView(R.id.result_title)
    TextView result_title;
    @BindView(R.id.setting_imgv)
    ImageView setting_imgv;
    @BindView(R.id.close_live_imgv)
    ImageView close_live_imgv;

    //bottom_camera_layout
    @BindView(R.id.voice_tv)
    MyTextView voice_tv;
    //right_camera_layout
    @BindView(R.id.check_result_face)
    LinearLayout check_result_face;
    @BindView(R.id.check_result_face2face)
    LinearLayout check_result_face2face;
    @BindView(R.id.check_result_reply)
    LinearLayout check_result_reply;
    @BindView(R.id.check_face_imgv)
    ImageView check_face_imgv;
    @BindView(R.id.check_face2face_imgv)
    ImageView check_face2face_imgv;
    @BindView(R.id.check_reply_imgv)
    ImageView check_reply_imgv;
    @BindView(R.id.check_face_tv)
    TextView check_face_tv;
    @BindView(R.id.check_face2face_tv)
    TextView check_face2face_tv;
    @BindView(R.id.check_reply_tv)
    TextView check_reply_tv;

    @BindView(R.id.chronometer)
    Chronometer chronometer;

    //单独为身份证添加
    @BindView(R.id.ll_camera_crop_container)
    View mLlCameraCropContainer;
    @BindView(R.id.iv_camera_crop)
    ImageView mIvCameraCrop;
    @BindView(R.id.fl_camera_option)
    FrameLayout mFlCameraOption;
    @BindView(R.id.lLayout_camera_crop)
    LinearLayout lLayout_camera_crop;

    private CountDownTimer timerFace;
    private CountDownTimer timeDown;
    private CountDownTimer voiceasr;

    private String scanValue;
    private String staffName;
    private String staffCard;
    private String organizationCode;

    //保存全局的登录用户名
    private String login_username;
    //登录验证token
    private String loginToken;
    //token过期后刷新Token的值
    private String refreshToken;
    private VoiceConfigBean voiceConfigBean;

    private int nextFlag = 0;//定义是否可以点击下一步按钮的标记变量
    private int voiceNextFlag = 0; //定义是否可以语音控制下一步的标记变量
    private int gestureNextFlag = 0; //定义是否可以手势控制下一步的标记变量

    private SpeechSynthesizer speechSynthesizer;
    private static final String TTS_TEXT_MODEL_FILE = "bd_etts_text.dat";
    private static final String TTS_SPEECH_MODEL_FILE = "bd_etts_speech_female.dat";
    private String mSampleDirPath; //本地路径

    private SurfaceHolder surfaceHolder;
    private CircleImageView circleImageView;

    JSONArray jsonArray = new JSONArray();
    private String recordTime;
    //录制的本地视频路径
    private String videoUrl;
    private int count;

    UploadSendBean uploadSendBean1;
    UploadSendBean uploadSendBean2;
    UploadSendBean uploadSendBean3;
    UploadSendBean uploadSendBean4;
    UploadSendBean uploadSendBean5;
    UploadSendBean uploadSendBean6;
    UploadSendBean uploadSendBean7;
    UploadSendBean uploadSendBean8;
    UploadSendBean uploadSendBean9;
    UploadSendBean uploadSendBean10;
    UploadSendBean uploadSendBean11;
    UploadSendBean uploadSendBean12;
    UploadSendBean uploadSendBean13;
    UploadSendBean uploadSendBean14;
    UploadSendBean uploadSendBean15;
    UploadSendBean uploadSendBean16;
    UploadSendBean uploadSendBean17;
    UploadSendBean uploadSendBean18;

    List<FailListBean> failList=new ArrayList<>();
    /**
     *录屏功能相关
     * @param savedInstanceState
     *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initChatSDK();
        login_username = SharedPreferencesUtil.getString(LiveMainActivity0819.this, SharedPreferencesUtil.LOGINUSERNAME, "");
        loginToken = SharedPreferencesUtil.getString(LiveMainActivity0819.this, SharedPreferencesUtil.LOGINTOKEN, "");
        refreshToken = SharedPreferencesUtil.getString(LiveMainActivity0819.this, SharedPreferencesUtil.REFRESHTOKEN, "");
        voiceConfigBean=(VoiceConfigBean) getIntent().getSerializableExtra("voiceConfigBean");


        // 保持Activity处于唤醒状态
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_live);
        /**
         * 解决 侵入式下  EditText位于屏幕底部时会被软键盘覆盖的问题
         * 在setContentView(R.layout.xxx)之后调用
         */
        InputBug.assistActivity(this);
        ButterKnife.bind(this);

        ConstantUtil.getInstance().setRtmpUrl("rtmp://10.252.37.102/live/abc");
        initLiveConfig(ConstantUtil.getInstance().getRtmpUrl());

        if (!mLiveCameraView.isRecord()) {
            ToastUtils.showToast(LiveMainActivity0819.this, "开始录制视频");
            //动态权限申请
            //处理点击事件
            mLiveCameraView.startRecord(login_username);
        }
        connect(BaseApplication.getInstance().getBASE_IP(), ApiService.BASE_PORT);
//        connect("122.9.53.187", "31200");
        //一开始初始化语音播报最后一步变量为false
        ConstantUtil.getInstance().setEndStep(false);

        initEnv();
        initTTS();

        initUI();

    }

    //初始化科大讯飞语音识别
    private void initChatSDK() {
        SpeechManager.CreateInstance(getApplicationContext());
        SpeechManager.getInstance().setBaseSpeechCallback(speechCallback);
    }

    BaseSpeechCallback speechCallback = new BaseSpeechCallback() {
        @Override
        public void recognizeResult(String text) {
            Log.e("li", "recognizeResult::" + text);
            if ("确认".equals(text) || "同意".equals(text) || "清楚".equals(text) ||
                    "是的".equals(text) || "可以".equals(text) || "知道".equals(text)) {
                ConstantUtil.getInstance().setAgree(true);
            } else {
                ConstantUtil.getInstance().setAgree(false);
            }
            /**
             *
             */
            if (voiceNextFlag == 16 && "完成".equals(text)) {
//                Intent intentDone = new Intent(LiveMainActivity.this,
//                        VideoDetailDoneActivity.class);
//                startActivity(intentDone);
//                finish();
//                System.exit(0);
            }
            /**------           start         -------*/

//            if(! ConstantUtil.getInstance().isVoiceing()) {
            //如果是最后一步语音播报不在进行判断
            if ( ! ConstantUtil.getInstance().isEndStep() ) {
                if (voiceNextFlag == 0 && "下一步".equals(text)) {

                    nextFlag = 1;
                    gestureNextFlag = 1;
                    nextStep1();

                } else if (voiceNextFlag == 1 && "下一步".equals(text)) {

                    nextFlag = 2;
                    gestureNextFlag = 2;
                    nextStep2();

                } else if (voiceNextFlag == 2 && "下一步".equals(text)) { //

                    nextFlag = 3;
                    gestureNextFlag = 3;
                    nextStep4();

                } else if (voiceNextFlag == 3 && "下一步".equals(text)) { //书面文件为准

                    nextFlag = 4;
                    gestureNextFlag = 4;
                    nextStep5();

                } else if (voiceNextFlag == 4 && "下一步".equals(text)) { //保障方案说明

                    nextFlag = 5;
                    gestureNextFlag = 5;
                    nextStep6();

                } else if (voiceNextFlag == 5 && "下一步".equals(text)) { //产品说明书（人身保证新型产品）

                    nextFlag = 6;
                    gestureNextFlag = 6;
                    nextStep7();

                } else if (voiceNextFlag == 6 && "下一步".equals(text)) { //条款介绍

                    nextFlag = 7;
                    gestureNextFlag = 7;
                    nextStep8();

                } else if (voiceNextFlag == 7 && "下一步".equals(text)) { //疾病观察期介绍

                    nextFlag = 8;
                    gestureNextFlag = 8;
                    nextStep9();

                } else if (voiceNextFlag == 8 && "下一步".equals(text)) { //犹豫期

                    nextFlag = 9;
                    gestureNextFlag = 9;
                    nextStep10();

                } else if (voiceNextFlag == 9 && "下一步".equals(text)) { //宽限期（11/20）

                    nextFlag = 10;
                    gestureNextFlag = 10;
                    nextStep11();

                } else if (voiceNextFlag == 10 && "下一步".equals(text)) { //保单利益不确定（人身保证新型产品）（12/20）

                    nextFlag = 11;
                    gestureNextFlag = 11;
                    nextStep12();

                } else if (voiceNextFlag == 11 && "下一步".equals(text)) { //重要提示事项（13/20）

                    nextFlag = 12;
                    gestureNextFlag = 12;
                    nextStep13();

                } else if (voiceNextFlag == 12 && "下一步".equals(text)) { //投保人确认（14/20）

                    nextFlag = 13;
                    gestureNextFlag = 13;
                    nextStep14();

                } else if (voiceNextFlag == 13 && "下一步".equals(text)) { //被保险人证件展示（ 大连 (以死亡为给付条件保险产品)15/20）

                    nextFlag = 14;
                    gestureNextFlag = 14;
                    nextStep15();

                } else if (voiceNextFlag == 14 && "下一步".equals(text)) { //被保险人确认（16/20）

                    nextFlag = 15;
                    gestureNextFlag = 15;
                    nextStep16();

                } else if (voiceNextFlag == 15 && "下一步".equals(text)) { //投保提示书签字 （17/20）

                    nextFlag = 16;
                    gestureNextFlag = 16;
                    nextStep17();

                } else if (voiceNextFlag == 16 && "下一步".equals(text)) { //免责条款签字（18/20）

                    nextFlag = 17;
                    gestureNextFlag = 17;
                    nextStep18();

                    ConstantUtil.getInstance().setEndStep(true);
                }
//                else if (voiceNextFlag == 17 && "下一步".equals(text)) { //风险提示语签字（人身保证新型产品）（19/20）
//
//                    nextFlag = 18;
//                    gestureNextFlag = 18;
//                    nextStep18();
//
//                }

                if (voiceNextFlag < 17 && "下一步".equals(text)) {
                    voiceNextFlag = (voiceNextFlag + 1) % 17;//其余得到循环执行上面18个不同的功能
                }
            }
        }
//        }

    };
    /**
     * -----------------------handle 更新UI-------------------------
     **/
    public final int FACE_5_TTS = 21;
    public final int FACE_3_TTS = 22;
    public final int VOICE_ASR = 23;
    public final int PRODUCT_3_TTS = 24;
    public Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case FACE_5_TTS:
                    initTimer();
                    break;
                case VOICE_ASR:
                    //朗读完之后进行语音回答
                    initVoiceASR();

                    break;
                case PRODUCT_3_TTS:
                    initCountDown();
                    break;
                // end of added
                default:
                    break;

            }
            return false;
        }
    });

    private void initCountDown() {
        timeDown = new CountDownTimer(4 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时执行的方法
                countdown_tv.setVisibility(View.VISIBLE);
                countdown_tv.setText(millisUntilFinished / 1000 + "");
                Log.e("@li@", millisUntilFinished / 1000 + "");
            }

            @Override
            public void onFinish() {
                //倒计时结束后的方法
                countdown_tv.setVisibility(View.GONE);
                timeDown.cancel();

                //倒计时结束后开始视频录制计时
                chronometer.setFormat("%s");
                chronometer.setBase(SystemClock.elapsedRealtime());//计时器清零,必须
                chronometer.start();
                //开始录屏
//                if (recordService.isRunning()) {
//                    recordService.stopRecord();
//                } else {
//                    Intent captureIntent = projectionManager.createScreenCaptureIntent();
//                    startActivityForResult(captureIntent, RECORD_REQUEST_CODE);
//                }

            }
        };
        timeDown.start();//开始倒计时

    }

    private void initVoiceASR() {
        voiceasr = new CountDownTimer(500, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时执行的方法
                Log.e("#li#", millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                //倒计时结束后的方法
                if (ConstantUtil.getInstance().isAgree()) {
                    //回答同意 true
                    check_reply_imgv.setImageResource(R.mipmap.result_yes);
                    check_reply_tv.setTextColor(Color.parseColor("#60C163"));
                    check_reply_tv.setText(R.string.check_resule_reply);
                    //明确回复之后倒计时取消
                    voiceasr.cancel();

                } else {
                    Message msgVoice = new Message();
                    msgVoice.what = VOICE_ASR;
                    mHandler.sendMessage(msgVoice);

                    check_reply_imgv.setImageResource(R.mipmap.result_no);
                    check_reply_tv.setTextColor(Color.parseColor("#E53C3C"));
                    check_reply_tv.setText(R.string.check_resule_reply);
                }

            }
        };
        voiceasr.start();//开始倒计时
    }

    private void initTimer() {
        timerFace = new CountDownTimer(8 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时执行的方法
                Log.e("#li#", millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                //倒计时结束后的方法
                if (ConstantUtil.getInstance().isAgree() && ConstantUtil.getInstance().isFace2Face()) {
                    //回答同意并且同框 true
                    check_reply_imgv.setImageResource(R.mipmap.result_yes);
                    check_reply_tv.setTextColor(Color.parseColor("#28FE37"));

                    //明确回复之后倒计时取消
                    timerFace.cancel();
                    //设置可以下一步
                    voiceNextFlag = 0; //语音

                } else if (ConstantUtil.getInstance().isAgree() && !ConstantUtil.getInstance().isFace2Face()) {
                    //回答同意但不同框

                    speak("请代理人与投保人保持同框，3秒后将再次确认");
                    Message msg = new Message();
                    msg.what = FACE_5_TTS;
                    mHandler.sendMessage(msg);

                } else { //没回答，或不是指定的回答
                    speak("请问您是否同意？");
                    Message msg = new Message();
                    msg.what = FACE_5_TTS;
                    mHandler.sendMessage(msg);
                }
            }
        };
        timerFace.start();//开始倒计时
    }


    private void initUI() {

        videoUrl = SharedPreferencesMediaUtil.getString(LiveMainActivity0819.this, SharedPreferencesMediaUtil.RECORDVIDEOPATH, "");
        surfaceview_live.setZOrderOnTop(true);//处于顶层
        surfaceview_live.getHolder().setFormat(PixelFormat.TRANSPARENT);//设置surface为透明
        surfaceHolder = surfaceview_live.getHolder();

        LinearLayout.LayoutParams containerParams1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams cropParams1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 480);
        mLlCameraCropContainer.setLayoutParams(containerParams1);
        mIvCameraCrop.setLayoutParams(cropParams1);
        mIvCameraCrop.setImageResource(R.mipmap.camera_idcard_background);

        //一开始配置固定的rtmpURL的时候不给推流操作
        btnStartStreaming.setEnabled(false);
        btnStopStreaming.setEnabled(false);

        btngetUrl.setEnabled(false);
        btnNext.setVisibility(View.VISIBLE);
        btnSuccess.setVisibility(View.GONE);

        recordTime= DataFormatUtil.getDateStrIncludeTimeZone();
        //倒计时
        Message msg = new Message();
        msg.what = PRODUCT_3_TTS;
        mHandler.sendMessage(msg);

        //第一步
        jsonArray.clear();
        uploadSendBean1=new UploadSendBean("1",1,"征求录像意见",
                10000, DataFormatUtil.getDateStrIncludeTimeZone() ) ;
        jsonArray.add(uploadSendBean1);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.VISIBLE);
        check_result_reply.setVisibility(View.VISIBLE);
        speech_tips.setText(R.string.speech_tips2);

        speak(getResources().getString(R.string.check_voice));

    }

    /**
     * 设置推流参数
     */
    public void initLiveConfig(String message) {
        mLiveCameraView = (StreamLiveCameraView) findViewById(R.id.stream_previewView);

        //参数配置 start
        streamAVOption = new StreamAVOption();
        streamAVOption.streamUrl = message;
        //参数配置 end

        mLiveCameraView.init(this, streamAVOption);
//        mLiveCameraView.addStreamStateListener(resConnectionListener);
        LinkedList<BaseHardVideoFilter> files = new LinkedList<>();
    }

    RESConnectionListener resConnectionListener = new RESConnectionListener() {
        @Override
        public void onOpenConnectionResult(int result) {
            //result 0成功  1 失败
            Toast.makeText(LiveMainActivity0819.this,"打开推流连接 状态："+result ,Toast.LENGTH_LONG).show();

        }

        @Override
        public void onWriteError(int errno) {
            Toast.makeText(LiveMainActivity0819.this,"推流出错,请尝试重连",Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCloseConnectionResult(int result) {
            //result 0成功  1 失败
            Toast.makeText(LiveMainActivity0819.this,"关闭推流连接 状态："+result,Toast.LENGTH_LONG).show();
        }
    };


    private void connect(String ipAddress, String portAddress) {
        if (isConnected) {
            RxSocketManager.getInstance().close();
            //断开socket连接的同时停止推流

            if (mLiveCameraView.isStreaming()) {
                mLiveCameraView.stopStreaming();
            }
            return;
        }

        String ip = ipAddress;
        int port = Integer.valueOf(portAddress);
        RxSocketManager.getInstance().close();//创建之前 请现在关闭上一次的socket 释放资源
        RxSocketManager.getInstance()
                .setClient(SocketType.TCP, ip, port)
                .setResultCallback(new RxSocketManager.ResultCallback() {
                    @Override
                    public void onSucceed(byte[] data) {

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        Log.e("#count#", "当前时间为："+ sdf.format(new Date()) );
                        count++;
                        Log.e("#count#", "推流次数为："+count );

                        String rec = null;
                        try {
                            rec = new String(data, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
//                        String rec="{\"checkType\":3,\"code\":1,\"data\":{\"boxes\":\"[[360.49905859 148.72525561 517.2071292  345.49290021   0.99936312]\\n [254.98722434  59.73911428 349.79854032 180.87477799   0.99824762]]\",\"num\":2,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":1,\"data\":{\"boxes\":\"[[358.73372208 150.34423414 525.63462262 352.68975373   0.99979287]\\n [240.03068209  63.13652882 334.53340364 183.44977444   0.99355161]]\",\"num\":2,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":1,\"data\":{\"boxes\":\"[[243.94758384  40.05586407 339.75652967 157.07328446   0.99967062]\\n [357.88176507 112.03457971 524.15673418 318.1011143    0.99914634]]\",\"num\":2,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\":\"[]\",\"num\":0,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\":\"[]\",\"num\":0,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\":\"[]\",\"num\":0,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\"";
                        int strCount = StringUtil.getKeyCount(rec, ConstantUtil.socketStr);

                        String subString = null;
                        if (strCount == 1) {
                            subString = rec.substring(0, rec.length() - 2);
                        } else if (strCount > 1) {
                            subString = rec.substring(0, rec.indexOf(ConstantUtil.socketStr));
                            Log.e("#li#", "----subString" + subString);
                        }

                        if (subString != null) {

                            Log.e("#li#", "----subString截取后" + subString);
                            logger.error("----subString截取后" + subString);

                            //输出日志
//                            showMsg_tv.setVisibility(View.VISIBLE);
//                            showMsg_tv.setText(subString);

                            Gson gson1 = new Gson();
                            SocketBean socketBean = gson1.fromJson(subString, SocketBean.class);
                            int code1 = socketBean.getCode();
                            String loginMsg = socketBean.getMsg();
                            int messageId1 = socketBean.getMsgId();
                            String message1 = socketBean.getRtmpUrl();
                            String uuid = socketBean.getUuid();
                            int checkType=socketBean.getCheckType();

                            //传递登录token后的返回值解析
                            if (code1 == 0 && messageId1 != 101 && ! subString.contains("uuid")) {

                                //初始化ASR
                                initChatSDK();

                                Log.e("li", loginMsg);
                                logger.error(loginMsg);
                                /**----------------第一步：获取URL推流-------------------**/
                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("msgId", 101);
                                    jsonObject.put("msg", "");
                                    jsonObject.put("scanValue", scanValue);

                                    sendData(jsonObject.toString() + ConstantUtil.socketStr);
                                    issendBtn = false;
                                    ConstantUtil.getInstance().setTest(1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else if (code1 == -3 && messageId1 != 101) {
                                Log.e("li", loginMsg);
                                logger.error(loginMsg);
                            }
//                            else if (code1 == -1 && messageId1 != 101) {
//                                ToastUtils.showToast(LiveMainActivity.this, loginMsg);
//                            }

                            if (messageId1 == 101) { //获取推流成功
                                btnStartStreaming.setEnabled(true);
                                btnStartStreaming.setBackgroundResource(R.drawable.btn_selector);
                                btnStopStreaming.setEnabled(true);
                                btnStopStreaming.setBackgroundResource(R.drawable.btn_selector);

                                btnScreenshot.setEnabled(true);
                                btnStartRecord.setEnabled(true);
                                btnStopRecord.setEnabled(true);
                                btnScreenshot.setBackgroundResource(R.drawable.btn_selector);
                                btnStartRecord.setBackgroundResource(R.drawable.btn_selector);
                                btnStopRecord.setBackgroundResource(R.drawable.btn_selector);

                                ConstantUtil.getInstance().setRtmpUrl(message1);

                                Log.e("li", message1);

                                logger.error(message1);

                                //开始推流
                                if (!mLiveCameraView.isStreaming()) {

                                    mLiveCameraView.startStreaming(ConstantUtil.getInstance().getRtmpUrl());
                                }

                                /**----------------第二步：代理人人脸验证-------------------**/
//                                if (mLiveCameraView.isStreaming()) {
//                                    JsonUtils.jsonSend(102, "", 1);
//                                    ConstantUtil.getInstance().setTest(5);
//                                    ToastUtils.showToast(LiveMainActivity.this, "开始人脸验证");
//                                }

//                                    if (!issendBtn)
//                                        showRtmpUrlDialog(ConstantUtil.getInstance().getRtmpUrl());


                            } else if (messageId1 == 103 && code1 == 1) {
                                /**----------------第三步：人脸同框-------------------**/
                                JsonUtils.jsonSend(102, "", 3);
//                                ConstantUtil.getInstance().setTest(2);
                                ConstantUtil.getInstance().setAddstep(1);
                                ToastUtils.showToast(LiveMainActivity0819.this, "开始人脸同框检测");
                            }

                            if (subString.contains("data") && subString.contains("uuid")) {
                                Gson gson = new Gson();
                                Face2FaceBean face2FaceBean = gson.fromJson(subString, Face2FaceBean.class);
                                int messageId = face2FaceBean.getMsgId();
                                int type = face2FaceBean.getCheckType();
                                int code = face2FaceBean.getCode();
                                String msg = face2FaceBean.getMsg();

//                                time_face_tv.setVisibility(View.VISIBLE);
//                                time_face_tv.setText(msg);

                                //矩形框的坐标
                                List<List<Float>> boxes = face2FaceBean.getData().getBoxes();
                                //手势
//                            String gesturetype = face2FaceBean.getData().getGesturetype();

                                ViewTreeObserver viewTreeObserver = mLiveCameraView.getViewTreeObserver();
                                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                    @Override
                                    public void onGlobalLayout() {
                                        mLiveCameraView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                                        Log.e("#li#", "Height---" + mLiveCameraView.getHeight()
//                                                + "Width---" + mLiveCameraView.getWidth());
                                    }
                                });

                                /**--------- 判断是否有值才绘制矩形框 ------------ **/
                                if (boxes != null && boxes.size() > 0 && ConstantUtil.getInstance().getAddstep() != 4) {

                                    if (circleImageView == null) {
                                        //动态添加矩形框
                                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                                                FrameLayout.LayoutParams.MATCH_PARENT);
                                        circleImageView = new CircleImageView(LiveMainActivity0819.this, boxes, mLiveCameraView.getHeight(), mLiveCameraView.getWidth());
                                        mLiveCameraView.addView(circleImageView, params);
                                    } else {
                                        //先移除
                                        mLiveCameraView.removeView(circleImageView);
                                        //动态添加矩形框
                                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                                                FrameLayout.LayoutParams.MATCH_PARENT);
                                        circleImageView = new CircleImageView(LiveMainActivity0819.this, boxes, mLiveCameraView.getHeight(), mLiveCameraView.getWidth());
                                        mLiveCameraView.addView(circleImageView, params);

                                    }
                                } else {
                                    if (circleImageView != null) {
                                        mLiveCameraView.removeView(circleImageView);
                                    }

                                }

                                /** ----------  start 新添加步骤不同UI的情况   -----------**/

                                //条款检测
                                if (code == 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 9) { //成功

                                    ConstantUtil.getInstance().setClause(true);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_clause);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));


                                } else if (code != 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 9) {//失败

                                    ConstantUtil.getInstance().setClause(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_error_product);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_clause);
                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }

                                /** ----------    end 新添加步骤不同UI的情况      -----------**/


                                //人脸同框
                                if (code == 1 && type == 3 && ConstantUtil.getInstance().getAddstep() == 1) { //成功

//                                    time_face_tv.setVisibility(View.VISIBLE);
//                                    time_face_tv.setText(msg);

                                    ConstantUtil.getInstance().setFace2Face(true);
                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_face2face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face2face_tv.setTextColor(Color.parseColor("#28FE37"));
                                    check_face2face_tv.setText(R.string.check_resule_face2face);

                                    //语音播报完后进行投保人的语音识别
                                    Message msgVoice = new Message();
                                    msgVoice.what = VOICE_ASR;
                                    mHandler.sendMessage(msgVoice);

                                } else if (code != 1 && type == 3 && ConstantUtil.getInstance().getAddstep() == 1) { //失败

//                                    time_face_tv.setVisibility(View.VISIBLE);
//                                    time_face_tv.setText(msg);

                                    ConstantUtil.getInstance().setFace2Face(false);
                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_face2face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face2face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_face2face_tv.setText(R.string.check_resule_face2face);

                                    //语音播报完后进行投保人的语音识别
                                    Message msgVoice = new Message();
                                    msgVoice.what = VOICE_ASR;
                                    mHandler.sendMessage(msgVoice);

                                }
                                //说明书检测
                                if (code == 1 && type == 5) {

                                    ConstantUtil.getInstance().setClause(true);
                                    time_face_tv.setVisibility(View.GONE);
//                                    btnNext.setEnabled(true);
//                                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
//                                    btnNext.setBackgroundResource(R.drawable.btn_next_bg_normal);

                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_product);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));
                                } else if (code != 1 && type == 5) {

                                    ConstantUtil.getInstance().setClause(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_error_product);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_product);
                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }
                                //条款检测
                                if (code == 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 1) { //成功

                                    ConstantUtil.getInstance().setClause(true);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

//                                    check_face_tv.setText("出示提示书");
                                    String insureContent=ConstantUtil.getInstance().getInsureContent();
                                    check_face_tv.setText(insureContent);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));

                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
                                    check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);

                                    speak("请投保人认真阅读，并签名确认 ");

                                    //发送签字动作
                                    /**----------------第六步： 签字动作和结果-------------------**/
                                    JsonUtils.jsonSend(102, "", 8);
                                    ConstantUtil.getInstance().setTest(7);
                                    ToastUtils.showToast(LiveMainActivity0819.this, "开始签字验证");

                                } else if (code != 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 1) {//失败

                                    ConstantUtil.getInstance().setClause(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_error_insure);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

//                                    check_face_tv.setText("出示提示书");
                                    String insureContent=ConstantUtil.getInstance().getInsureContent();
                                    check_face_tv.setText(insureContent);
                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
                                    check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);

                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }
                                //代理人人脸验证,只要成功一次就不在返回code -1
                                if (code == 1 && type == 1) { //成功

                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));
                                    check_face_tv.setText(R.string.check_resule_face);

                                    /**----------------第三步：人脸同框-------------------**/
                                    JsonUtils.jsonSend(102, "", 3);
                                    ConstantUtil.getInstance().setTest(2);
                                    ConstantUtil.getInstance().setAddstep(1);
                                    ToastUtils.showToast(LiveMainActivity0819.this, "开始人脸同框检测");

                                    initTimer(); //5秒计时


                                } else if (code != 1 && type == 1) { //失败

                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_warn_error_tip);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_face_tv.setText(R.string.check_resule_face);
                                }
                                //身份证检测
                                if (code == 1 && type == 4 && ConstantUtil.getInstance().getAddstep() == 4) { //成功

//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                                    Log.e("#count#", "当前时间为："+ sdf.format(new Date()) );
//                                    count++;
//                                    Log.e("#count#", "推流次数为："+count );

                                    ConstantUtil.getInstance().setCard(true);
                                    lLayout_camera_crop.setVisibility(View.GONE); //证件检测成功后取消遮罩层
                                    time_face_tv.setVisibility(View.GONE);
                                    warn_tv.setText(R.string.check_idcard_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_show_idcard);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));

                                } else if (code != 1 && type == 4 && ConstantUtil.getInstance().getAddstep() == 4) { //失败

//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                                    Log.e("#count#", "当前时间为："+ sdf.format(new Date()) );
//                                    count++;
//                                    Log.e("#count#", "推流次数为："+count );

                                    ConstantUtil.getInstance().setCard(false);
                                    lLayout_camera_crop.setVisibility(View.VISIBLE);//证件检测没有成功显示遮罩层
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_idcard_error_tip);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_show_idcard);
                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));

                                }
                                //签字区域校验
                                if (code == 1 && type == 8) {

                                    ConstantUtil.getInstance().setSign(true);
                                    time_face_tv.setVisibility(View.GONE);

                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face2face_tv.setTextColor(Color.parseColor("#28FE37"));

//                                    speak("请代理人出示签字结果，并在镜头前展示三秒");

                                } else if (code != 1 && type == 8) {

                                    ConstantUtil.getInstance().setSign(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face2face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }

//                            //手势检测步骤方法类
//                            if("B".equals(gesturetype)) {
//
//                                //防止连续多次调用手势检测
//                                if (ClickUtil.isNotFastClick()) {
//                                    gestureNext(gesturetype);
//                                }
//
//                            }
                            }

                        }
//                        else if(ConstantUtil.getInstance().getTest()==10){
//                            ToastUtils.showToast(LiveMainActivity.this, "类型8成功");
//                        }

                    }

                    @Override
                    public void onFailed(Throwable t) {
//                        ToastUtil.s("返回失败");
                        t.printStackTrace();
                    }
                })
                .setSocketStatusListener(new RxSocketManager.OnSocketStatusListener() {
                    @Override
                    public void onConnectSucceed() {

                        LiveMainActivity0819.this.runOnUiThread(() -> {
                            Log.d("li", "------------onConnectSucceed");
                            logger.error("------------onConnectSucceed");
//                            ToastUtils.showToast(LiveMainActivity.this, "已连接");

                            scanValue = getIntent().getStringExtra("scanValue");

                            Gson gson = new Gson();
                            ScanBean scanBean = gson.fromJson(scanValue, ScanBean.class);
                            staffCard = scanBean.getStaffCard();
                            staffName = scanBean.getStaffName();
                            organizationCode = scanBean.getOrganizationCode();

                            Log.e("llll", scanValue);
                            logger.error(scanValue);
//                            scanValue = "{\"policyNum\":\"123456789\",\"insureType\":1,\"insureTypeDesc\":\"保险类型1\",\"policyName\":\"张晓红\",\"policyCard\":\"445464564656842185\",\"organizationCode\":\"597456\",\"staffNum\":\"123456\",\"staffName\":\"黄社会\",\"staffCard\":\"4455465612553156656\"}";
                            SharedPreferencesUtil.putString(LiveMainActivity0819.this, SharedPreferencesUtil.SOCKETIP, ipAddress);
                            SharedPreferencesUtil.putString(LiveMainActivity0819.this, SharedPreferencesUtil.SOCKETPORT, portAddress);

                            btngetUrl.setEnabled(true);
                            btngetUrl.setBackgroundResource(R.drawable.btn_selector);

                            /**
                             * 开始发送登录token给后台
                             */
                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("token", loginToken);

                                sendData(jsonObject.toString() + ConstantUtil.socketStr);
                                issendBtn = false;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            isConnected = true;
                        });
                    }

                    @Override
                    public void onConnectFailed() {
                        Log.d("li", "------------onConnectFailed");
                        logger.error("------------onConnectFailed");
                    }

                    @Override
                    public void onDisConnected() {
                        LiveMainActivity0819.this.runOnUiThread(() -> {
                            Log.d("li", "------------onDisConnected");
                            logger.error("------------onDisConnected");
//                            ToastUtils.showToast(LiveMainActivity0819.this, "已断开连接");
                            isConnected = false;
                            //断开连接的同时不显示检测人数
                            msg.setVisibility(View.GONE);

                            //断开的同时取消绘制框
                            if (circleImageView != null) {
                                mLiveCameraView.removeView(circleImageView);
                            }

                            //停止推流
                            if (mLiveCameraView.isStreaming()) {
                                mLiveCameraView.stopStreaming();

                                if (timerFace != null) {
                                    timerFace.cancel();
                                }
                                if (voiceasr != null) {
                                    voiceasr.cancel();
                                }
                            }
                            btnStartStreaming.setEnabled(false);
                            btnStopStreaming.setEnabled(false);
                            btngetUrl.setEnabled(false);
                        });
                    }
                }).build();

    }

    //手势检测方法类
    private void gestureNext(String gesturetype) {
        /**------           start    手势检测     -------*/
        if (gestureNextFlag == 0 && "B".equals(gesturetype)) {

            nextFlag = 1;
            voiceNextFlag = 1;
            nextStep1();


        } else if (gestureNextFlag == 1 && "B".equals(gesturetype)) {

            nextFlag = 2;
            voiceNextFlag = 2;
            nextStep2();


        } else if (gestureNextFlag == 2 && "B".equals(gesturetype)) { //如实告知

            nextFlag = 3;
            voiceNextFlag = 3;
            nextStep3();

        } else if (gestureNextFlag == 3 && "B".equals(gesturetype)) { //书面文件为准

            nextFlag = 4;
            voiceNextFlag = 4;
            nextStep4();

        } else if (gestureNextFlag == 4 && "B".equals(gesturetype)) { //保障方案说明

            nextFlag = 5;
            voiceNextFlag = 5;
            nextStep5();

        } else if (gestureNextFlag == 5 && "B".equals(gesturetype)) { //产品说明书（人身保证新型产品）

            nextFlag = 6;
            voiceNextFlag = 6;
            nextStep6();

        } else if (gestureNextFlag == 6 && "B".equals(gesturetype)) { //条款介绍

            nextFlag = 7;
            voiceNextFlag = 7;
            nextStep7();

        } else if (gestureNextFlag == 7 && "B".equals(gesturetype)) { //疾病观察期介绍

            nextFlag = 8;
            voiceNextFlag = 8;
            nextStep8();

        } else if (gestureNextFlag == 8 && "B".equals(gesturetype)) { //疾病观察期介绍

            nextFlag = 9;
            voiceNextFlag = 9;
            nextStep9();

        } else if (gestureNextFlag == 9 && "B".equals(gesturetype)) { //宽限期（11/20）

            nextFlag = 10;
            voiceNextFlag = 10;
            nextStep10();

        } else if (gestureNextFlag == 10 && "B".equals(gesturetype)) { //保单利益不确定（人身保证新型产品）（12/20）

            nextFlag = 11;
            voiceNextFlag = 11;
            nextStep11();

        } else if (gestureNextFlag == 11 && "B".equals(gesturetype)) { //重要提示事项（13/20）

            nextFlag = 12;
            voiceNextFlag = 12;
            nextStep12();

        } else if (gestureNextFlag == 12 && "B".equals(gesturetype)) { //投保人确认（14/20）

            nextFlag = 13;
            voiceNextFlag = 13;
            nextStep13();

        } else if (gestureNextFlag == 13 && "B".equals(gesturetype)) { //被保险人证件展示（ 大连 (以死亡为给付条件保险产品)15/20）

            nextFlag = 14;
            voiceNextFlag = 14;
            nextStep14();

        } else if (gestureNextFlag == 14 && "B".equals(gesturetype)) { //被保险人确认（16/20）

            nextFlag = 15;
            voiceNextFlag = 15;
            nextStep15();

        } else if (gestureNextFlag == 15 && "B".equals(gesturetype)) { //投保提示书签字 （17/20）

            nextFlag = 16;
            voiceNextFlag = 16;
            nextStep16();

        } else if (gestureNextFlag == 16 && "B".equals(gesturetype)) { //免责条款签字（18/20）

            nextFlag = 17;
            voiceNextFlag = 17;
            nextStep17();

        } else if (gestureNextFlag == 17 && "B".equals(gesturetype)) { //风险提示语签字（人身保证新型产品）（19/20）

            nextFlag = 18;
            voiceNextFlag = 18;
            nextStep18();

        }

        gestureNextFlag = (gestureNextFlag + 1) % 18;//其余得到循环执行上面20个不同的功能

        /**------           end    手势检测     ---------*/
    }

    private void sendData(String data) {
        try {
            byte[] bytes = data.getBytes();
            RxSocketManager.getInstance().send(bytes, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.btn_startStreaming, R.id.btn_stopStreaming, R.id.btn_startRecord, R.id.btn_stopRecord,
            R.id.btn_filter, R.id.btn_swapCamera, R.id.btn_screenshot, R.id.btn_mirror, R.id.iv_image,
            R.id.btn_socket, R.id.btn_getUrl, R.id.btn_next, R.id.btn_success, R.id.btn_record,
            R.id.setting_imgv,  R.id.close_live_imgv
    })
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_screenshot://截帧
                mLiveCameraView.takeScreenShot(new RESScreenShotListener() {
                    @Override
                    public void onScreenShotResult(Bitmap bitmap) {
                        if(bitmap != null){
                            imageView.setVisibility(View.VISIBLE);
                            imageView.setImageBitmap(bitmap);
                            Log.e("###",bitmap.getWidth() +""+ bitmap.getHeight());
                        }

                    }
                });
                break;

            case R.id.close_live_imgv: //关闭录制主界面
                ConstantUtil.getInstance().setUploadToMain(false);

                failList.clear();
                //退出按钮删除掉没有保存的视频文件
                File file=new File(videoUrl);
                if(file.exists()){
                    file.delete();
                }

                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                }
                Intent intentMain=new Intent(LiveMainActivity0819.this,
                        MainPagerActivity.class);
                startActivity(intentMain);
                ConstantUtil.getInstance().setUploadToMain(true);
                finish();
//                System.exit(0);

                break;

//            case R.id.btn_screenshot://加个测试
//                JsonUtils.jsonSend(102, "", 8);
//                ConstantUtil.getInstance().setTest(10);
//                //添加是为了区分和新增步骤相同检测类型但不同UI的情况
//                ConstantUtil.getInstance().setAddstep(10);
//                ToastUtils.showToast(LiveMainActivity.this, "测试类型8");
//                break;

            case R.id.btn_stopStreaming://停止推流
                if (mLiveCameraView.isStreaming()) {
                    mLiveCameraView.stopStreaming();
                }
                //停止推流的同时断开socket连接
                if (isConnected) {
                    RxSocketManager.getInstance().close();
                    return;
                }
                break;
            case R.id.btn_startRecord://开始录制
                if (!mLiveCameraView.isRecord()) {
                    ToastUtils.showToast(LiveMainActivity0819.this, "开始录制视频");
                    //动态权限申请

                    //处理点击事件
                    mLiveCameraView.startRecord(login_username);
//                    mLiveCameraView.startRecord();
                    chronometer.setFormat("%s");
                    chronometer.setBase(SystemClock.elapsedRealtime());//计时器清零,必须
                    chronometer.start();

                }
                break;
            case R.id.btn_stopRecord://停止录制
                if (mLiveCameraView.isRecord()) {
                    mLiveCameraView.stopRecord();
                    ToastUtils.showToast(LiveMainActivity0819.this, "视频已成功保存至系统根目录的live/文件夹中");

                }
                chronometer.stop();

                break;

            case R.id.btn_swapCamera://切换摄像头
                mLiveCameraView.swapCamera();

                break;

            case R.id.btn_mirror://镜像
                if (isMirror) {
                    mLiveCameraView.setMirror(true, false, false);
                } else {
                    mLiveCameraView.setMirror(true, true, true);
                }
                isMirror = !isMirror;

                break;

            case R.id.iv_image:
                imageView.setVisibility(View.GONE);
                break;

            case R.id.setting_imgv:
//                showSocketDialog();
                break;
            case R.id.btn_success:
                /**
                 * 完成按钮点击
                 **/

                //录制计时结束
                chronometer.stop();
                //语音播报结束了才可以点击下一步
                if( ! ConstantUtil.getInstance().isVoiceing()) {

                    //注销百度TTS
                    SpeechManager.getInstance().onDestroy();
                    if (speechSynthesizer != null) {
                        speechSynthesizer.release();
                        speechSynthesizer.stop();
                    }

                    //跳转到视频录制完成详情界面
                    if (ClickUtil.isNotFastClick()) {

                        if ( ConstantUtil.getInstance().isClause() && ConstantUtil.getInstance().isSign() ){
                            uploadSendBean18.setState(1);
                        }else {
                            uploadSendBean18.setState(0);
                        }
                        if (ConstantUtil.getInstance().isClause() && !ConstantUtil.getInstance().isSign()) {
                            FailListBean bean18= new FailListBean("个人人生保险投保单签字", "18", "签字结果可见");
                            failList.add(bean18);
                        }else if ( !ConstantUtil.getInstance().isClause() && ConstantUtil.getInstance().isSign()){
                            FailListBean bean18= new FailListBean("个人人生保险投保单签字", "18", "个人人生保险投保单清晰展示");
                            failList.add(bean18);
                        }else if ( !ConstantUtil.getInstance().isClause() && !ConstantUtil.getInstance().isSign()){
                            FailListBean bean18= new FailListBean("个人人生保险投保单签字", "18", "个人人生保险投保单清晰展示、签字结果可见");
                            failList.add(bean18);
                        }

                        if (mLiveCameraView.isRecord()) {
                            mLiveCameraView.stopRecord();
                            ToastUtils.showToast(LiveMainActivity0819.this, "视频已成功保存至系统根目录的live/文件夹中");

                        }
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }

                        Intent intentDone = new Intent(LiveMainActivity0819.this,
                                VideoDetailDoneActivity.class);
                        intentDone.putExtra("scanValue",scanValue);
                        intentDone.putExtra("result",jsonArray.toString());
                        intentDone.putExtra("recordTime",recordTime);
                        intentDone.putExtra("failList",(Serializable)failList);
                        startActivity(intentDone);
                        finish();
//                        System.exit(0);

                        //停止录屏
                        //处理点击事件
//                        Intent intentLogin = new Intent(LiveMainActivity.this,
//                                RecordmainActivity.class);
//                        startActivity(intentLogin);
                    }
                }

                break;

            case R.id.btn_next:

                //语音播报结束了才可以点击下一步
                if(! ConstantUtil.getInstance().isVoiceing()) {

                    if (nextFlag == 0) {  //
                        gestureNextFlag = 1;
                        voiceNextFlag = 1;
                        nextStep1();

                    } else if (nextFlag == 1) {
                        gestureNextFlag = 2;
                        voiceNextFlag = 2;
                        nextStep2();

                    } else if (nextFlag == 2) {

                        gestureNextFlag = 3;
                        voiceNextFlag = 3;
                        nextStep4();

                    } else if (nextFlag == 3) {


                        gestureNextFlag = 4;
                        voiceNextFlag = 4;
                        nextStep5();

                    } else if (nextFlag == 4) {

                        gestureNextFlag = 5;
                        voiceNextFlag = 5;
                        nextStep6();

                    } else if (nextFlag == 5) {

                        gestureNextFlag = 6;
                        voiceNextFlag = 6;
                        nextStep7();

                    } else if (nextFlag == 6) {


                        gestureNextFlag = 7;
                        voiceNextFlag = 7;
                        nextStep8();

                    } else if (nextFlag == 7) {

                        gestureNextFlag = 8;
                        voiceNextFlag = 8;
                        nextStep9();

                    } else if (nextFlag == 8) {

                        gestureNextFlag = 9;
                        voiceNextFlag = 9;
                        nextStep10();

                    } else if (nextFlag == 9) {

                        gestureNextFlag = 10;
                        voiceNextFlag = 10;
                        nextStep11();

                    } else if (nextFlag == 10) {

                        gestureNextFlag = 11;
                        voiceNextFlag = 11;
                        nextStep12();

                    } else if (nextFlag == 11) {

                        gestureNextFlag = 12;
                        voiceNextFlag = 12;
                        nextStep13();

                    } else if (nextFlag == 12) {


                        gestureNextFlag = 13;
                        voiceNextFlag = 13;
                        nextStep14();

                    } else if (nextFlag == 13) {

                        gestureNextFlag = 14;
                        voiceNextFlag = 14;
                        nextStep15();

                    } else if (nextFlag == 14) {

                        gestureNextFlag = 15;
                        voiceNextFlag = 15;
                        nextStep16();

                    } else if (nextFlag == 15) {

                        gestureNextFlag = 16;
                        voiceNextFlag = 16;
                        nextStep17();

                    } else if (nextFlag == 16) {

                        gestureNextFlag = 17;
                        voiceNextFlag = 17;
                        nextStep18();

                    }
//                    else if (nextFlag == 17) {
//                        gestureNextFlag = 18;
//                        voiceNextFlag = 18;
//                        nextStep18();
//
//                    }

                    nextFlag = (nextFlag + 1) % 17;
                }
                break;
            case R.id.btn_record:
                showStopRecordDialog();
                break;

            default:
                break;
        }
    }

    private void nextStep1() {  //代理人证件检测

        //
        uploadSendBean2=new UploadSendBean("2",1,"代理人员出示证件",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean2);

        if (ConstantUtil.getInstance().isAgree() && !ConstantUtil.getInstance().isFace2Face()) {
            FailListBean bean1= new FailListBean("征求录像意见", "01", "代理人与投保人人脸同框");
            failList.add(bean1);
        }else if ( !ConstantUtil.getInstance().isAgree() && ConstantUtil.getInstance().isFace2Face()){
            FailListBean bean1= new FailListBean("征求录像意见", "01", "投保人明确答复");
            failList.add(bean1);
        }else if ( !ConstantUtil.getInstance().isAgree() && !ConstantUtil.getInstance().isFace2Face()){
            FailListBean bean1= new FailListBean("征求录像意见", "01", "代理人与投保人人脸同框、投保人明确答复");
            failList.add(bean1);
        }

        //-------第一个步骤
        if ( ConstantUtil.getInstance().isAgree() && ConstantUtil.getInstance().isFace2Face()){
            uploadSendBean1.setState(1);
        }else {
            uploadSendBean1.setState(0);
        }

        if (timerFace != null) {
            timerFace.cancel();
        }
        if (voiceasr != null) {
            voiceasr.cancel();
        }

//        time_face_tv.setVisibility(View.VISIBLE);
//        showMsg_tv.setVisibility(View.VISIBLE);
        speech_tips.setVisibility(View.VISIBLE);

        lLayout_camera_crop.setVisibility(View.VISIBLE);
        /**----------------第四步：身份证检测-------------------**/
        if (mLiveCameraView.isStreaming()) {
            try {
                JSONObject jsonCard = new JSONObject();
                jsonCard.put("cardId", staffCard);
                jsonCard.put("cardName", staffName);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("msgId", 102);
                jsonObject.put("msg", "");
                jsonObject.put("checkType", 4);
                jsonObject.put("checkValue", jsonCard.toString());

                sendData(jsonObject.toString() + ConstantUtil.socketStr);

                //添加是为了区分和新增步骤相同检测类型但不同UI的情况
                ConstantUtil.getInstance().setAddstep(4);
                ToastUtils.showToast(LiveMainActivity0819.this, "开始身份证校验");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText(R.string.check_idcard_title);
        speech_tips.setVisibility(View.VISIBLE);
        speech_tips.setText(R.string.check_idcard_tip);
        voice_tv.setText(getResources().getString(R.string.check_idcard));

        check_face_tv.setText(R.string.check_show_idcard);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);

        speak(getResources().getString(R.string.check_idcard));

    }

    private void nextStep2() {  //代理人介绍

        uploadSendBean3=new UploadSendBean("3",1,"代理人介绍",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean3);

        if (!ConstantUtil.getInstance().isCard()) {
            FailListBean bean2= new FailListBean("代理人员出示证件", "02", "出示有效身份证件");
            failList.add(bean2);
        }

        if ( ConstantUtil.getInstance().isCard() ){
            uploadSendBean2.setState(1);
        }else {
            uploadSendBean2.setState(0);
        }

        if (timerFace != null) {
            timerFace.cancel();
        }
        if (voiceasr != null) {
            voiceasr.cancel();
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);

        result_title.setText(R.string.agent_introduction);
        speech_tips.setText("请你回答是的或者不是");

        voice_tv.setText("为你提供服务的是保险公司的销售人员" + staffName +
                "，工号是" + organizationCode + ",销售人员向你出示的是其本人的证件嘛？");

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak("为你提供服务的是保险公司的销售人员" + staffName +
                "，工号是" + organizationCode + ",销售人员向你出示的是其本人的证件嘛？");
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);
    }

    private void nextStep3() { //投保人识别
        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.GONE);

        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.GONE);
        result_title.setText(R.string.check_policy_distinguish_title);
        speech_tips.setText(R.string.check_policy_distinguish_tips);
        voice_tv.setText(getResources().getString(R.string.check_policy));

        check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);
        check_face2face_tv.setText(R.string.check_resule_face2face);
        check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));

//        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_policy));

        /**----------------第三步：人脸同框-------------------**/
//        JsonUtils.jsonSend(102, "", 3);
//        ConstantUtil.getInstance().setTest(8); //从8开始
//        ConstantUtil.getInstance().setAddstep(1);
//        ToastUtils.showToast(LiveMainActivity.this, "开始人脸同框检测");

    }

    private void nextStep4() { //书面文件为准

        uploadSendBean4=new UploadSendBean("4",0,"书面文件为准",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean4);

        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean3= new FailListBean("代理人介绍", "03", "投保人明确答复");
            failList.add(bean3);
        }

        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean3.setState(1);
        }else {
            uploadSendBean3.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);

        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_written_document_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_written_document));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_written_document));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);

    }

    private void nextStep5() {  //保障方案说明
        uploadSendBean5=new UploadSendBean("5",1,"保障方案说明",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean5);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean4= new FailListBean("书面文件为准", "04", "投保人明确答复");
            failList.add(bean4);
        }

        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean4.setState(1);
        }else {
            uploadSendBean4.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);

        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_safe_plan_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_safe_plan));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_safe_plan));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);

    }

    private void nextStep6() { //条款介绍
        uploadSendBean6=new UploadSendBean("6",1,"条款介绍",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean6);

        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean5= new FailListBean("保障方案说明", "05", "投保人明确答复");
            failList.add(bean5);
        }
        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean5.setState(1);
        }else {
            uploadSendBean5.setState(0);
        }
//        time_face_tv.setVisibility(View.VISIBLE);
//        showMsg_tv.setVisibility(View.VISIBLE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText(R.string.check_clause_title);
        speech_tips.setText(R.string.check_tips_clause);
        voice_tv.setText(R.string.check_clause);


        check_face_tv.setText(R.string.check_result_clause);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);


        speak(getResources().getString(R.string.check_clause));

        /**----------------第五步：产品说明书检测-------------------**/

        JsonUtils.jsonSend(102, "", 7);
        ConstantUtil.getInstance().setTest(3);
        //添加是为了区分和新增步骤相同检测类型但不同UI的情况
        ConstantUtil.getInstance().setAddstep(9);

    }

    private void nextStep7() { //责任确认
        uploadSendBean7=new UploadSendBean("7",1,"责任确认",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean7);
        if (!ConstantUtil.getInstance().isClause()) {
            FailListBean bean6= new FailListBean("条款介绍", "06", "出示清晰的条款提示书");
            failList.add(bean6);
        }
        if ( ConstantUtil.getInstance().isClause() ){
            uploadSendBean6.setState(1);
        }else {
            uploadSendBean6.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);

        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_respone_confirm_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_respone_confirm));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_respone_confirm));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);

    }

    private void nextStep8() { //疾病观察期
        uploadSendBean8=new UploadSendBean("8",0,"疾病观察期",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean8);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean7= new FailListBean("责任确认", "07", "投保人明确答复");
            failList.add(bean7);
        }
        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean7.setState(1);
        }else {
            uploadSendBean7.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_sickness_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_sickness));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_sickness));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);

    }

    private void nextStep9() { //指定医疗机构

        uploadSendBean9=new UploadSendBean("9",0,"指定医疗机构",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean9);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean8= new FailListBean("疾病观察期", "08", "投保人明确答复");
            failList.add(bean8);
        }
        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean8.setState(1);
        }else {
            uploadSendBean8.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_medical_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_medical));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_medical));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);
    }

    private void nextStep10() { //犹豫期
        uploadSendBean10=new UploadSendBean("10",1,"犹豫期",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean10);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean9= new FailListBean("指定医疗机构", "09", "投保人明确答复");
            failList.add(bean9);
        }

        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean9.setState(1);
        }else {
            uploadSendBean9.setState(0);
        }
        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_esitationh_phase_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_esitationh_phase));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_esitationh_phase));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);

    }

    private void nextStep11() { //宽限期
        uploadSendBean11=new UploadSendBean("11",1,"宽限期",
                10000, DataFormatUtil.getDateStrIncludeTimeZone() );
        jsonArray.add(uploadSendBean11);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean10= new FailListBean("犹豫期", "10", "投保人明确答复");
            failList.add(bean10);
        }
        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean10.setState(1);
        }else {
            uploadSendBean10.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_grace_period_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_grace_period));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_grace_period));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);
    }

    private void nextStep12() { //如实告知
        uploadSendBean12=new UploadSendBean("12",1,"如实告知",
                10000, DataFormatUtil.getDateStrIncludeTimeZone() );
        jsonArray.add(uploadSendBean12);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean11= new FailListBean("宽限期", "11", "投保人明确答复");
            failList.add(bean11);
        }
        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean11.setState(1);
        }else {
            uploadSendBean11.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_inform_title);
        speech_tips.setText("请你回答确认或者不确认");
        voice_tv.setText(getResources().getString(R.string.check_inform));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_inform));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);
    }

    private void nextStep13() { //投保人证件展示
        uploadSendBean13=new UploadSendBean("13",0,"投保人证件展示",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean13);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean12= new FailListBean("如实告知", "12", "投保人明确答复");
            failList.add(bean12);
        }
        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean12.setState(1);
        }else {
            uploadSendBean12.setState(0);
        }

//        time_face_tv.setVisibility(View.VISIBLE);
//        showMsg_tv.setVisibility(View.VISIBLE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.VISIBLE);
        /**----------------第四步：身份证检测-------------------**/
        if (mLiveCameraView.isStreaming()) {
            try {
                JSONObject jsonCard = new JSONObject();
                jsonCard.put("cardId", staffCard);
                jsonCard.put("cardName", staffName);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("msgId", 102);
                jsonObject.put("msg", "");
                jsonObject.put("checkType", 4);
                jsonObject.put("checkValue", jsonCard.toString());

                sendData(jsonObject.toString() + ConstantUtil.socketStr);

                //添加是为了区分和新增步骤相同检测类型但不同UI的情况
                ConstantUtil.getInstance().setAddstep(4);
                ToastUtils.showToast(LiveMainActivity0819.this, "开始身份证校验");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText(R.string.check_policy_idcard_title);
        speech_tips.setText(R.string.check_policy_idcard_tip);
        voice_tv.setText(getResources().getString(R.string.check_policy_idcard));

        check_face_tv.setText(R.string.check_show_idcard);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);

        speak(getResources().getString(R.string.check_policy_idcard));
    }

    private void nextStep14() { //投保人确认
        uploadSendBean14=new UploadSendBean("14",1,"投保人确认",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean14);
        if (!ConstantUtil.getInstance().isCard()) {
            FailListBean bean13= new FailListBean("投保人证件展示", "13", "出示有效身份证件");
            failList.add(bean13);
        }
        if ( ConstantUtil.getInstance().isCard() ){
            uploadSendBean13.setState(1);
        }else {
            uploadSendBean13.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_policy_comfirm_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_policy_comfirm));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_policy_comfirm));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);
    }

    private void nextStep15() { //支付提醒
        uploadSendBean15=new UploadSendBean("15",1,"支付提醒",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean15);
        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean14= new FailListBean("投保人确认", "14", "投保人明确答复");
            failList.add(bean14);
        }
        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean14.setState(1);
        }else {
            uploadSendBean14.setState(0);
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);
        result_title.setText(R.string.check_pay_title);
        speech_tips.setText(R.string.speech_tips);
        voice_tv.setText(getResources().getString(R.string.check_pay));

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        JsonUtils.jsonSend(102, "", 0);
        speak(getResources().getString(R.string.check_pay));
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);
    }

    private void nextStep16() { //投保提示书签字
        uploadSendBean16=new UploadSendBean("16",1,"投保提示书签字",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean16);

        if (!ConstantUtil.getInstance().isAgree()) {
            FailListBean bean15= new FailListBean("支付提醒", "15", "投保人明确答复");
            failList.add(bean15);
        }

        if ( ConstantUtil.getInstance().isAgree() ){
            uploadSendBean15.setState(1);
        }else {
            uploadSendBean15.setState(0);
        }
//        time_face_tv.setVisibility(View.VISIBLE);
//        showMsg_tv.setVisibility(View.VISIBLE);
        speech_tips.setVisibility(View.GONE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.VISIBLE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText(R.string.check_insure_title);
//        speech_tips.setText("请代理人将投保提示书在镜头前展示三秒");
        voice_tv.setText(getResources().getString(R.string.check_insure_tips));


        ConstantUtil.getInstance().setInsureContent("投保提示书");
        check_face_tv.setText(R.string.check_insure_book);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);
        check_face2face_tv.setText(R.string.check_result_insure);
        check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);


        speak(getResources().getString(R.string.check_insure_tips));

        /**----------------第六步：投保提示书 签字-------------------**/
        JsonUtils.jsonSend(102, "", 7);
        ConstantUtil.getInstance().setTest(4);
        //添加是为了区分和新增步骤相同检测类型但不同UI的情况
        ConstantUtil.getInstance().setAddstep(1);
        ToastUtils.showToast(LiveMainActivity0819.this, "开始投保提示书检测");
    }

    private void nextStep17() { //免责条款签字
        uploadSendBean17=new UploadSendBean("17",0,"免责条款签字",
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean17);
        if (ConstantUtil.getInstance().isClause() && !ConstantUtil.getInstance().isSign()) {
            FailListBean bean16= new FailListBean("投保提示书签字", "16", "签字结果可见");
            failList.add(bean16);
        }else if ( !ConstantUtil.getInstance().isClause() && ConstantUtil.getInstance().isSign()){
            FailListBean bean16= new FailListBean("投保提示书签字", "16", "投保提示书清晰展示");
            failList.add(bean16);
        }else if ( !ConstantUtil.getInstance().isClause() && !ConstantUtil.getInstance().isSign()){
            FailListBean bean16= new FailListBean("投保提示书签字", "16", "投保提示书清晰展示、签字结果可见");
            failList.add(bean16);
        }

        if ( ConstantUtil.getInstance().isClause() && ConstantUtil.getInstance().isSign() ){
            uploadSendBean16.setState(1);
        }else {
            uploadSendBean16.setState(0);
        }
//        time_face_tv.setVisibility(View.VISIBLE);
//        showMsg_tv.setVisibility(View.VISIBLE);
        speech_tips.setVisibility(View.GONE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.VISIBLE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText(R.string.check_sign_clause_title);
//        speech_tips.setText("请代理人将免责条款知情同意书在镜头前展示三秒");
        voice_tv.setText(getResources().getString(R.string.check_sign_clause));


        ConstantUtil.getInstance().setInsureContent("免责条款");
        check_face_tv.setText(R.string.check_result_sign_clause);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);
        check_face2face_tv.setText(R.string.check_result_insure);
        check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);

        speak(getResources().getString(R.string.check_sign_clause));

        /**----------------第六步：免责条款签字 -------------------**/
        JsonUtils.jsonSend(102, "", 7);
        ConstantUtil.getInstance().setTest(4);
        //添加是为了区分和新增步骤相同检测类型但不同UI的情况
        ConstantUtil.getInstance().setAddstep(1);
        ToastUtils.showToast(LiveMainActivity0819.this, "开始免责条款检测");
    }

    private void nextStep18() { //个人人生保险投保单签字
        uploadSendBean18=new UploadSendBean("18",0,"个人人生保险投保单签字",
                10000, DataFormatUtil.getDateStrIncludeTimeZone() );
        jsonArray.add(uploadSendBean18);
        if (ConstantUtil.getInstance().isClause() && !ConstantUtil.getInstance().isSign()) {
            FailListBean bean17= new FailListBean("免责条款签字", "17", "签字结果可见");
            failList.add(bean17);
        }else if ( !ConstantUtil.getInstance().isClause() && ConstantUtil.getInstance().isSign()){
            FailListBean bean17= new FailListBean("免责条款签字", "17", "免责条款清晰展示");
            failList.add(bean17);
        }else if ( !ConstantUtil.getInstance().isClause() && !ConstantUtil.getInstance().isSign()){
            FailListBean bean17= new FailListBean("免责条款签字", "17", "免责条款清晰展示、签字结果可见");
            failList.add(bean17);
        }

        if ( ConstantUtil.getInstance().isClause() && ConstantUtil.getInstance().isSign() ){
            uploadSendBean17.setState(1);
        }else {
            uploadSendBean17.setState(0);
        }
//        time_face_tv.setVisibility(View.VISIBLE);
//        showMsg_tv.setVisibility(View.VISIBLE);
        speech_tips.setVisibility(View.GONE);
        btnNext.setVisibility(View.GONE);
        btnSuccess.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);
        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.VISIBLE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText(R.string.check_sign_personal_insurance_title);
//        speech_tips.setText("请代理人将个人人生保险投保单在镜头前展示三秒");
        voice_tv.setText(getResources().getString(R.string.check_sign_personal_insurance));

        ConstantUtil.getInstance().setInsureContent("个人人身保险投保单");
        check_face_tv.setText(R.string.check_result_sign_personal_insurance);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);
        check_face2face_tv.setText(R.string.check_result_insure);
        check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);

        speak(getResources().getString(R.string.check_sign_personal_insurance));

        /**----------------第六步：个人人生保险投保单签字-------------------**/
        JsonUtils.jsonSend(102, "", 7);
        ConstantUtil.getInstance().setTest(4);
        //添加是为了区分和新增步骤相同检测类型但不同UI的情况
        ConstantUtil.getInstance().setAddstep(1);
        ToastUtils.showToast(LiveMainActivity0819.this, "个人人生保险投保单签字");
    }

    protected void speak(String text) {

        speechSynthesizer.speak(text);
//        int re = this.speechSynthesizer.speak(text);
//        if (re < 0) {
//            ToastUtils.showToast(LiveMainActivity.this, "error to speak");
//        }
    }

    //初始化TTS
    private void initTTS() {
        this.speechSynthesizer = SpeechSynthesizer.getInstance();
        this.speechSynthesizer.setContext(this);

//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME, "5");//音量 范围["0" - "15"], 不支持小数。 "0" 最轻，"15" 最响。
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "5");//语速 范围["0" - "15"], 不支持小数。 "0" 最慢，"15" 最快
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_PITCH, "5");//语调 范围["0" - "15"], 不支持小数。 "0" 最慢，"15" 最快

        //文本模型路径（离线）
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_TEXT_MODEL_FILE, mSampleDirPath + "/" + TTS_TEXT_MODEL_FILE);
        //设置声学模型（男声、女生）
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_SPEECH_MODEL_FILE, mSampleDirPath + "/" + TTS_SPEECH_MODEL_FILE);
        //发声人
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "0");
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_MIX_MODE, SpeechSynthesizer.MIX_MODE_HIGH_SPEED_SYNTHESIZE);
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "8");//语速 范围["0" - "15"], 不支持小数。 "0" 最慢，"15" 最快

        //请填写你申请到的appid、apikey
        this.speechSynthesizer.setAppId("18168798");
        this.speechSynthesizer.setApiKey("gWVMBAKAMAO73oqXVlmP8rGi", "CNt8yZeH0wlKgGQx9WaRb6lhbBK7fT5G");
        //授权检测接口
        AuthInfo authInfo = this.speechSynthesizer.auth(TtsMode.MIX);

        if (authInfo.isSuccess()) {
            Log.e("li", "connected successed");
        } else {
            Log.e("li", "connected failed");
            String errorMsg = authInfo.getTtsError().getDetailMessage();
        }

        //初始化tts
//        speechSynthesizer.initTts(TtsMode.MIX);
        speechSynthesizer.initTts(TtsMode.ONLINE);

        this.speechSynthesizer.setSpeechSynthesizerListener(new SpeechSynthesizerListener() {
            @Override
            public void onSynthesizeStart(String s) {
                Log.e("lihhh", "onSynthesizeStart------");
                ConstantUtil.getInstance().setVoiceing(true);
            }

            @Override
            public void onSynthesizeDataArrived(String s, byte[] bytes, int i) {
                Log.e("lihhh", "onSynthesizeDataArrived------");

            }

            @Override
            public void onSynthesizeFinish(String s) {
                Log.e("lihhh", "onSynthesizeFinish------");
            }

            @Override
            public void onSpeechStart(String s) {

                Log.e("lihhh", "onSpeechStart------");

            }

            @Override
            public void onSpeechProgressChanged(String s, int i) {
                Log.e("lihhh", "onSpeechProgressChanged------");
            }

            @Override
            public void onSpeechFinish(String s) {
                Log.e("lihhh", "onSpeechFinish------");
                ConstantUtil.getInstance().setVoiceing(false); //结束了才可以点击下一步
            }

            @Override
            public void onError(String s, SpeechError speechError) {
                Log.e("lihhh", "onError------");
                ConstantUtil.getInstance().setVoiceing(false); //报错异常也可以点击下一步
            }
        });
    }

    private void initEnv() {
        if (mSampleDirPath == null) {
            String path = Environment.getExternalStorageDirectory().toString();
            mSampleDirPath = path + "/" + "ASR_TTS";
            File file = new File(mSampleDirPath);
            if (!file.exists()) {
                file.mkdirs();
            }
        }

    }

    private void showSocketDialog() {
        View view = initPopu(R.layout.socket_dialog);
        final EditText ip = (EditText) view
                .findViewById(R.id.modify_password_old);
        final EditText port = (EditText) view
                .findViewById(R.id.modify_password_new);
        //连接成功后传值
//        ip.setText( ConstantUtil.getInstance().getIp() );
//        port.setText( ConstantUtil.getInstance().getPort()  );
        ip.setText(SharedPreferencesUtil.getString(LiveMainActivity0819.this,
                SharedPreferencesUtil.SOCKETIP, ""));
        port.setText(SharedPreferencesUtil.getString(LiveMainActivity0819.this,
                SharedPreferencesUtil.SOCKETPORT, ""));

        Button okButton = (Button) view.findViewById(R.id.modify_password_ok);

        if (isConnected) {
            okButton.setText(this.getResources().getString(R.string.disconnect));
        }
//        else if(RxSocketManager.getInstance().isSocketClosed()){ //socket连接错误关闭
//            okButton.setText("Socket异常");
//            okButton.setEnabled(false);
//        }
        else {
            okButton.setText(this.getResources().getString(R.string.connect));
        }

        try {
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String ipAddress = ip.getText().toString();
                    String portAddress = port.getText().toString();
                    //判断输入的是数字还是字母
                    Pattern p = Pattern.compile("[0-9]*");
                    Matcher m = p.matcher(portAddress);

                    if (!"".equals(ipAddress) && !"".equals(portAddress) && m.matches()) {
                        connect(ipAddress, portAddress);

                        if (popupWindow != null && popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    } else {
                        ToastUtils.showToast(LiveMainActivity0819.this, "IP地址或端口号不能为空或端口号不是数字");
                    }
                }
            });

        } catch (Exception e) {

            SpiderMan.show(e);
        }

    }

    private void showRtmpUrlDialog(String message) {
        View view = initPopu(R.layout.rtmpurl_dialog);
        TextView rtmpUrl = (TextView) view
                .findViewById(R.id.rtmpUrl_tv);
        //成功获取
        rtmpUrl.setText(message);

    }

    private void showStopRecordDialog() {
        View view = initPopu(R.layout.stop_record_dialog);
        Button okButton = (Button) view.findViewById(R.id.confirm_record);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLiveCameraView.destroy();
                finish();
            }
        });

    }


    private View initPopu(int layoutId) {
        View view = View.inflate(this, layoutId, null);
        if (popupWindow == null || !popupWindow.isShowing()) {
            popupWindow = new PopupWindow(view,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            popupWindow.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.transparent));
//            popupWindow.setAnimationStyle(R.style.popwin_anim_style);
            popupWindow.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(false);
            popupWindow.update();
        }
        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
        view.findViewById(R.id.popup_layout).setOnClickListener(this);
        return view;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            ToastUtils.showToast(LiveMainActivity0819.this, "再按一次退出当前录制");
            exitTime = System.currentTimeMillis();
        } else {
            Intent intentMain=new Intent(LiveMainActivity0819.this,
                    MainPagerActivity.class);
            startActivity(intentMain);
            ConstantUtil.getInstance().setUploadToMain(true);
            finish();
//            System.exit(0);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLiveCameraView.destroy();
        if (timerFace != null) {
            timerFace.cancel();
            timerFace = null;
        }
        if (voiceasr != null) {
            voiceasr.cancel();
            voiceasr=null;
        }

        SpeechManager.getInstance().onDestroy();
        if (speechSynthesizer != null) {
            speechSynthesizer.release();
            speechSynthesizer.stop();
        }

        if (mLiveCameraView.isStreaming()) {
            mLiveCameraView.stopStreaming();
        }
        //停止推流的同时断开socket连接
        if (isConnected) {
            RxSocketManager.getInstance().close();
            return;
        }

        chronometer.stop();

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 960;
    }

}
