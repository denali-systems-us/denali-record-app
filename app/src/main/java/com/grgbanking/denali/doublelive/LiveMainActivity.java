package com.grgbanking.denali.doublelive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.aiuisdk.BaseSpeechCallback;
import com.aiuisdk.SpeechManager;
import com.alibaba.fastjson.JSONArray;
import com.baidu.tts.auth.AuthInfo;
import com.baidu.tts.client.SpeechError;
import com.baidu.tts.client.SpeechSynthesizer;
import com.baidu.tts.client.SpeechSynthesizerListener;
import com.baidu.tts.client.TtsMode;
import com.google.gson.Gson;
import com.grgbanking.denali.doublelive.bean.FailListBean;
import com.grgbanking.denali.doublelive.bean.VoiceConfigBean;
import com.grgbanking.denali.doublelive.socketUtil.BaseApplication;
import com.grgbanking.denali.doublelive.socketUtil.RxSocketManager;
import com.grgbanking.denali.doublelive.socketUtil.SocketType;
import com.grgbanking.denali.doublelive.utils.CircleImageView;
import com.grgbanking.denali.doublelive.utils.DataFormatUtil;
import com.grgbanking.denali.doublelive.utils.InputBug;
import com.grgbanking.denali.doublelive.utils.JsonUtils;
import com.grgbanking.denali.doublelive.utils.MyTextView;
import com.grgbanking.denali.doublelive.utils.NetSpeedTimer;
import com.grgbanking.denali.doublelive.videoabout.LocalVideoDetailActivity;
import com.grgbanking.denali.doublelive.videoabout.MainPagerActivity;
import com.grgbanking.denali.doublelive.videoabout.VideoDetailDoneActivity;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;
import com.simple.spiderman.SpiderMan;
import com.grgbanking.denali.doublelive.bean.Face2FaceBean;
import com.grgbanking.denali.doublelive.bean.ScanBean;
import com.grgbanking.denali.doublelive.bean.UploadSendBean;
import com.grgbanking.denali.doublelive.http.ApiService;
import com.grgbanking.denali.doublelive.bean.SocketBean;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.core.listener.RESConnectionListener;
import me.lake.librestreaming.core.listener.RESScreenShotListener;
import me.lake.librestreaming.filter.hardvideofilter.BaseHardVideoFilter;
import me.lake.librestreaming.utils.ClickUtil;
import me.lake.librestreaming.utils.ConstantUtil;
import me.lake.librestreaming.utils.SharedPreferencesMediaUtil;
import me.lake.librestreaming.utils.StringUtil;
import me.lake.librestreaming.utils.ToastUtils;
import me.lake.librestreaming.ws.StreamAVOption;
import me.lake.librestreaming.ws.StreamLiveCameraView;

public class LiveMainActivity extends AppCompatActivity implements View.OnClickListener,
        CustomAdapt {

    //初始化Logger
    protected static Logger logger = Logger.getLogger(LiveMainActivity.class);

    private static final String TAG = LiveMainActivity.class.getSimpleName();
    private StreamLiveCameraView mLiveCameraView;
    private StreamAVOption streamAVOption;
//    private String rtmpUrl = "rtmp://10.252.37.102/live/abc";

    private LiveUIIDCard mLiveUIIDCard;
    private LiveUIProduct mLiveUIProduct;

    private boolean isTCP = true;//默认 TCP   UDP
    private boolean isConnected;

    boolean isFilter = false;
    boolean isMirror = false;
    //是否显示获取推流地址的对话框
    boolean issendBtn = false;

    private PopupWindow popupWindow;
    private long exitTime = 0;


    @BindView(R.id.btn_startStreaming)
    Button btnStartStreaming;
    @BindView(R.id.btn_stopStreaming)
    Button btnStopStreaming;
    @BindView(R.id.btn_startRecord)
    Button btnStartRecord;
    @BindView(R.id.btn_stopRecord)
    Button btnStopRecord;
    @BindView(R.id.btn_filter)
    Button btnFliter;
    @BindView(R.id.btn_swapCamera)
    ImageView btnSwapCamera;
    @BindView(R.id.btn_screenshot)
    Button btnScreenshot;
    @BindView(R.id.btn_mirror)
    ImageView btnMirror;
    @BindView(R.id.btn_socket)
    Button btnSocket;
    @BindView(R.id.btn_getUrl)
    Button btngetUrl;

    @BindView(R.id.iv_image)
    ImageView imageView;

    @BindView(R.id.msg)
    TextView msg;

    @BindView(R.id.net)
    TextView net;

    @BindView(R.id.btn_record)
    TextView btnRecord;
    @BindView(R.id.btn_next)
    TextView btnNext;
    @BindView(R.id.btn_success)
    TextView btnSuccess;

    @BindView(R.id.main_live)
    View parent;

    @BindView(R.id.surfaceview_live)
    SurfaceView surfaceview_live;
    //提示用语
    @BindView(R.id.speech_tips)
    TextView speech_tips;

    @BindView(R.id.time_face_tv)
    TextView time_face_tv;
    @BindView(R.id.showMsg_tv)
    TextView showMsg_tv;
    @BindView(R.id.countdown_tv)
    TextView countdown_tv;

    private NetSpeedTimer mNetSpeedTimer;

    //normal
    @BindView(R.id.warn_tv)
    TextView warn_tv;
    @BindView(R.id.main_camera_layout)
    View main_camera_layout;
    @BindView(R.id.right_camera_layout)
    View right_camera_layout;
    @BindView(R.id.bottom_camera_layout)
    View bottom_camera_layout;
    @BindView(R.id.success_camera_layout)
    View success_camera_layout;

    //top_camera_layout
    @BindView(R.id.result_title)
    TextView result_title;
    @BindView(R.id.setting_imgv)
    ImageView setting_imgv;
    @BindView(R.id.close_live_imgv)
    ImageView close_live_imgv;

    //bottom_camera_layout
    @BindView(R.id.voice_tv)
    MyTextView voice_tv;
    //right_camera_layout
    @BindView(R.id.check_result_face)
    LinearLayout check_result_face;
    @BindView(R.id.check_result_face2face)
    LinearLayout check_result_face2face;
    @BindView(R.id.check_result_reply)
    LinearLayout check_result_reply;
    @BindView(R.id.check_face_imgv)
    ImageView check_face_imgv;
    @BindView(R.id.check_face2face_imgv)
    ImageView check_face2face_imgv;
    @BindView(R.id.check_reply_imgv)
    ImageView check_reply_imgv;
    @BindView(R.id.check_face_tv)
    TextView check_face_tv;
    @BindView(R.id.check_face2face_tv)
    TextView check_face2face_tv;
    @BindView(R.id.check_reply_tv)
    TextView check_reply_tv;

    @BindView(R.id.chronometer)
    Chronometer chronometer;

    //单独为身份证添加
    @BindView(R.id.ll_camera_crop_container)
    View mLlCameraCropContainer;
    @BindView(R.id.iv_camera_crop)
    ImageView mIvCameraCrop;
    @BindView(R.id.fl_camera_option)
    FrameLayout mFlCameraOption;
    @BindView(R.id.lLayout_camera_crop)
    LinearLayout lLayout_camera_crop;

    private CountDownTimer timerFace;
    private CountDownTimer timeDown;
    private CountDownTimer voiceasr;

    private String scanValue;
    private String staffName;
    private String staffCard;
    private String organizationCode;

    //保存全局的登录用户名
    private String login_username;
    //登录验证token
    private String loginToken;
    //token过期后刷新Token的值
    private String refreshToken;
    private VoiceConfigBean voiceConfigBean;

    private int nextFlag = 0;//定义是否可以点击下一步按钮的标记变量
    private int voiceNextFlag = 0; //定义是否可以语音控制下一步的标记变量
    private int gestureNextFlag = 0; //定义是否可以手势控制下一步的标记变量

    private SpeechSynthesizer speechSynthesizer;
    private static final String TTS_TEXT_MODEL_FILE = "bd_etts_text.dat";
    private static final String TTS_SPEECH_MODEL_FILE = "bd_etts_speech_female.dat";
    private String mSampleDirPath; //本地路径

    private SurfaceHolder surfaceHolder;
    private CircleImageView circleImageView;

    JSONArray jsonArray = new JSONArray();
    private String recordTime;
    //录制的本地视频路径
    private String videoUrl;
    private int count;

    UploadSendBean uploadSendBean;
    UploadSendBean uploadSendBean18;

    List<FailListBean> failList = new ArrayList<>();
    private boolean isReply;

    /**
     * 录屏功能相关
     *
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initChatSDK();
        login_username = SharedPreferencesUtil.getString(LiveMainActivity.this, SharedPreferencesUtil.LOGINUSERNAME, "");
        loginToken = SharedPreferencesUtil.getString(LiveMainActivity.this, SharedPreferencesUtil.LOGINTOKEN, "");
        refreshToken = SharedPreferencesUtil.getString(LiveMainActivity.this, SharedPreferencesUtil.REFRESHTOKEN, "");
        voiceConfigBean = (VoiceConfigBean) getIntent().getSerializableExtra("voiceConfigBean");
        // 保持Activity处于唤醒状态
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_live);
        /**
         * 解决 侵入式下  EditText位于屏幕底部时会被软键盘覆盖的问题
         * 在setContentView(R.layout.xxx)之后调用
         */
        InputBug.assistActivity(this);
        ButterKnife.bind(this);

        ConstantUtil.getInstance().setRtmpUrl("rtmp://10.252.37.102/live/abc");
        initLiveConfig(ConstantUtil.getInstance().getRtmpUrl());

        if (!mLiveCameraView.isRecord()) {
            ToastUtils.showToast(LiveMainActivity.this, "开始录制视频");
            //动态权限申请
            //处理点击事件
            mLiveCameraView.startRecord(login_username);
        }
        connect(BaseApplication.getInstance().getBASE_IP(), ApiService.BASE_PORT);
//        connect("122.9.53.187", "31200");
        //一开始初始化语音播报最后一步变量为false
        ConstantUtil.getInstance().setEndStep(false);

        initEnv();
        initTTS();

        initUI();

    }

    //初始化科大讯飞语音识别
    private void initChatSDK() {
        SpeechManager.CreateInstance(getApplicationContext());
        SpeechManager.getInstance().setBaseSpeechCallback(speechCallback);
    }

    BaseSpeechCallback speechCallback = new BaseSpeechCallback() {
        @Override
        public void recognizeResult(String text) {
            Log.e("li", "recognizeResult::" + text);
            if ("确认".equals(text) || "同意".equals(text) || "清楚".equals(text) ||
                    "是的".equals(text) || "可以".equals(text) || "知道".equals(text)) {
                ConstantUtil.getInstance().setAgree(true);
            } else {
                ConstantUtil.getInstance().setAgree(false);
            }

            /**------           start         -------*/

//            if(! ConstantUtil.getInstance().isVoiceing()) {
            //如果是最后一步语音播报不在进行判断
            if (!ConstantUtil.getInstance().isEndStep() && "下一步".equals(text) && voiceConfigBean.getData().getTotal() > 1 ) {
                voiceNextFlag++;
                nextFlag++;

                String voiceDesc = voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getVoiceDesc();
                String name = voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getName();

                //上一个步骤的值
                String voiceDescBack = voiceConfigBean.getData().getVoiceList().get(voiceNextFlag - 1).getVoiceDesc();
                String nameBack = voiceConfigBean.getData().getVoiceList().get(voiceNextFlag - 1).getName();

                if (voiceDesc.contains("征求录像意见") || name.contains("征求录像意见")) {
                    /** -------上一步骤是以下某个检测项的时候。
                     * 顺序要在jsonArray添加uploadSendBean之前---- */
                    stepBackCheck(nameBack,voiceDescBack);
                    uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getId(),
                            1, voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getName(),
                            10000, DataFormatUtil.getDateStrIncludeTimeZone());
                    jsonArray.add(uploadSendBean);

                    if (voiceNextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                        ConstantUtil.getInstance().setEndStep(true);
                        nextStepFace2Face(name, voiceDesc, voiceNextFlag);
                        btnNext.setVisibility(View.GONE);
                        btnSuccess.setVisibility(View.VISIBLE);
                    } else {
                        nextStepFace2Face(name, voiceDesc, voiceNextFlag);
                        btnNext.setVisibility(View.VISIBLE);
                        btnSuccess.setVisibility(View.GONE);
                    }
                } else if (voiceDesc.contains("条款介绍") || name.contains("条款介绍")) {
                    /** -------上一步骤是以下某个检测项的时候。
                     * 顺序要在jsonArray添加uploadSendBean之前---- */
                    stepBackCheck(nameBack,voiceDescBack);
                    uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getId(),
                            1, voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getName(),
                            10000, DataFormatUtil.getDateStrIncludeTimeZone());
                    jsonArray.add(uploadSendBean);

                    if (voiceNextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                        ConstantUtil.getInstance().setEndStep(true);
                        nextStepClause(name, voiceDesc);
                        btnNext.setVisibility(View.GONE);
                        btnSuccess.setVisibility(View.VISIBLE);
                    } else {
                        nextStepClause(name, voiceDesc);
                        btnNext.setVisibility(View.VISIBLE);
                        btnSuccess.setVisibility(View.GONE);
                    }

                } else if (voiceDesc.contains("产品说明书") || name.contains("产品说明书")) {
                    /** -------上一步骤是以下某个检测项的时候。
                     * 顺序要在jsonArray添加uploadSendBean之前---- */
                    stepBackCheck(nameBack,voiceDescBack);
                    uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getId(),
                            1, voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getName(),
                            10000, DataFormatUtil.getDateStrIncludeTimeZone());
                    jsonArray.add(uploadSendBean);

                    if (voiceNextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                        ConstantUtil.getInstance().setEndStep(true);
                        nextStepProduct(name, voiceDesc);
                        btnNext.setVisibility(View.GONE);
                        btnSuccess.setVisibility(View.VISIBLE);
                    } else {
                        nextStepProduct(name, voiceDesc);
                        btnNext.setVisibility(View.VISIBLE);
                        btnSuccess.setVisibility(View.GONE);
                    }
                } else if (voiceDesc.contains("签名确认") || name.contains("签名确认")) {
                    /** -------上一步骤是以下某个检测项的时候。
                     * 顺序要在jsonArray添加uploadSendBean之前---- */
                    stepBackCheck(nameBack,voiceDescBack);
                    uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getId(),
                            1, voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getName(),
                            10000, DataFormatUtil.getDateStrIncludeTimeZone());
                    jsonArray.add(uploadSendBean);
                    if (voiceNextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                        ConstantUtil.getInstance().setEndStep(true);
                        nextStepSign(name, voiceDesc);

                        btnNext.setVisibility(View.GONE);
                        btnSuccess.setVisibility(View.VISIBLE);
                    } else {
                        nextStepSign(name, voiceDesc);

                        btnNext.setVisibility(View.VISIBLE);
                        btnSuccess.setVisibility(View.GONE);
                    }
                } else if (voiceDesc.contains("出示证件") || name.contains("出示证件")) {

                    /** -------上一步骤是以下某个检测项的时候。
                     * 顺序要在jsonArray添加uploadSendBean之前---- */
                    stepBackCheck(nameBack,voiceDescBack);

                    uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getId(),
                            1, voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getName(),
                            10000, DataFormatUtil.getDateStrIncludeTimeZone());
                    jsonArray.add(uploadSendBean);

                    if (voiceNextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                        ConstantUtil.getInstance().setEndStep(true);
                        nextStepCard(name, voiceDesc, voiceNextFlag);
                        btnNext.setVisibility(View.GONE);
                        btnSuccess.setVisibility(View.VISIBLE);
                    } else {
                        nextStepCard(name, voiceDesc, voiceNextFlag);
                        btnNext.setVisibility(View.VISIBLE);
                        btnSuccess.setVisibility(View.GONE);
                    }
                }
                if (  voiceDesc.contains("是否同意") || name.contains("是否同意") ||
                        voiceDesc.contains("清楚吗") || name.contains("清楚吗") ||
                        voiceDesc.contains("是否已清楚") || name.contains("是否已清楚") ||
                        voiceDesc.contains("是其本人的证件嘛") || name.contains("是其本人的证件嘛") ||
                        voiceDesc.contains("确认吗") || name.contains("确认吗") ) {
                    /** -------上一步骤是以下某个检测项的时候。
                     * 顺序要在jsonArray添加uploadSendBean之前---- */
                    stepBackCheck(nameBack,voiceDescBack);
                    uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getId(),
                            1, voiceConfigBean.getData().getVoiceList().get(voiceNextFlag).getName(),
                            10000, DataFormatUtil.getDateStrIncludeTimeZone());
                    jsonArray.add(uploadSendBean);

                    String tipsStr = "";
                    if (voiceDesc.contains("是否同意") || name.contains("是否同意")) {
                        tipsStr = "请你回答同意或不同意";
                    } else if (voiceDesc.contains("清楚吗") || name.contains("清楚吗") ||
                            voiceDesc.contains("是否已清楚") || name.contains("是否已清楚")) {
                        tipsStr = "请你回答清楚或不清楚";
                    } else if (voiceDesc.contains("是其本人的证件嘛") || name.contains("是其本人的证件嘛")) {
                        tipsStr = "请你回答是的或不是";
                    } else if (voiceDesc.contains("确认吗") || name.contains("确认吗")) {
                        tipsStr = "请你回答确认或不确认";
                    }

                    if (voiceNextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步

                        ConstantUtil.getInstance().setEndStep(true);
                        nextStepVoice(name, voiceDesc, tipsStr);
                        btnNext.setVisibility(View.GONE);
                        btnSuccess.setVisibility(View.VISIBLE);
                    } else {
                        nextStepVoice(name, voiceDesc, tipsStr);
                        btnNext.setVisibility(View.VISIBLE);
                        btnSuccess.setVisibility(View.GONE);
                    }
                }

                if (voiceNextFlag < voiceConfigBean.getData().getTotal() - 1 && "下一步".equals(text)) {
                    voiceNextFlag = (voiceNextFlag + 1) % voiceConfigBean.getData().getTotal() - 1;//其余得到循环执行上面18个不同的功能
                }
            }
        }
//        }

    };
    /**
     * -----------------------handle 更新UI-------------------------
     **/
    public final int VOICE_ASR = 23;
    public final int PRODUCT_3_TTS = 24;
    public Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case VOICE_ASR:
                    //朗读完之后进行语音回答
                    initVoiceASR();

                    break;
                case PRODUCT_3_TTS:
                    initCountDown();
                    break;
                // end of added
                default:
                    break;

            }
            return false;
        }
    });

    private void initCountDown() {
        timeDown = new CountDownTimer(4 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时执行的方法
                countdown_tv.setVisibility(View.VISIBLE);
                countdown_tv.setText(millisUntilFinished / 1000 + "");
                Log.e("@li@", millisUntilFinished / 1000 + "");
            }

            @Override
            public void onFinish() {
                //倒计时结束后的方法
                countdown_tv.setVisibility(View.GONE);
                timeDown.cancel();

                //倒计时结束后开始视频录制计时
                chronometer.setFormat("%s");
                chronometer.setBase(SystemClock.elapsedRealtime());//计时器清零,必须
                chronometer.start();

            }
        };
        timeDown.start();//开始倒计时

    }

    private void initVoiceASR() {
        voiceasr = new CountDownTimer(100, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时执行的方法
                Log.e("#li#", millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                //倒计时结束后的方法
                if (ConstantUtil.getInstance().isAgree()) {
                    //回答同意 true
                    check_reply_imgv.setImageResource(R.mipmap.result_yes);
                    check_reply_tv.setTextColor(Color.parseColor("#60C163"));
                    check_reply_tv.setText(R.string.check_resule_reply);
                    //明确回复之后倒计时取消
                    voiceasr.cancel();

                } else {
                    Message msgVoice = new Message();
                    msgVoice.what = VOICE_ASR;
                    mHandler.sendMessage(msgVoice);

                    check_reply_imgv.setImageResource(R.mipmap.result_no);
                    check_reply_tv.setTextColor(Color.parseColor("#E53C3C"));
                    check_reply_tv.setText(R.string.check_resule_reply);
                }

            }
        };
        voiceasr.start();//开始倒计时
    }

    private void initUI() {

        videoUrl = SharedPreferencesMediaUtil.getString(LiveMainActivity.this, SharedPreferencesMediaUtil.RECORDVIDEOPATH, "");
        surfaceview_live.setZOrderOnTop(true);//处于顶层
        surfaceview_live.getHolder().setFormat(PixelFormat.TRANSPARENT);//设置surface为透明
        surfaceHolder = surfaceview_live.getHolder();

        LinearLayout.LayoutParams containerParams1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams cropParams1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 480);
        mLlCameraCropContainer.setLayoutParams(containerParams1);
        mIvCameraCrop.setLayoutParams(cropParams1);
        mIvCameraCrop.setImageResource(R.mipmap.camera_idcard_background);

        //一开始配置固定的rtmpURL的时候不给推流操作
        btnStartStreaming.setEnabled(false);
        btnStopStreaming.setEnabled(false);

        btngetUrl.setEnabled(false);
        btnNext.setVisibility(View.VISIBLE);
        btnSuccess.setVisibility(View.GONE);

        recordTime = DataFormatUtil.getDateStrIncludeTimeZone();
        //倒计时
        Message msg = new Message();
        msg.what = PRODUCT_3_TTS;
        mHandler.sendMessage(msg);

        speech_tips.setText(R.string.speech_tips2);

        String voiceDesc = voiceConfigBean.getData().getVoiceList().get(0).getVoiceDesc();
        String name = voiceConfigBean.getData().getVoiceList().get(0).getName();

        /**
         * 初始化右边检测项的状态
         */
        isReply = voiceDesc.contains("是否同意") || name.contains("是否同意") ||
                voiceDesc.contains("清楚吗") || name.contains("清楚吗") ||
                voiceDesc.contains("是否已清楚") || name.contains("是否已清楚") ||
                voiceDesc.contains("是其本人的证件嘛") || name.contains("是其本人的证件嘛") ||
                voiceDesc.contains("确认吗") || name.contains("确认吗");
        if (voiceDesc.contains("征求录像意见") || name.contains("征求录像意见") ||
                voiceDesc.contains("条款介绍") || name.contains("条款介绍") ||
                voiceDesc.contains("产品说明书") || name.contains("产品说明书") ||
                voiceDesc.contains("签名确认") || name.contains("签名确认") ||
                voiceDesc.contains("出示证件") || name.contains("出示证件")) {
            check_result_face.setVisibility(View.GONE);
            check_result_face2face.setVisibility(View.VISIBLE);
            check_result_reply.setVisibility(View.GONE);
            if (isReply) {
                check_result_face.setVisibility(View.GONE);
                check_result_face2face.setVisibility(View.VISIBLE);
                check_result_reply.setVisibility(View.VISIBLE);
            }

        } else if (isReply) {
            check_result_face.setVisibility(View.GONE);
            check_result_face2face.setVisibility(View.GONE);
            check_result_reply.setVisibility(View.VISIBLE);
        } else {
            check_result_face.setVisibility(View.GONE);
            check_result_face2face.setVisibility(View.GONE);
            check_result_reply.setVisibility(View.GONE);
        }

        //第一步
        jsonArray.clear();

        uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(0).getId(),
                1, voiceConfigBean.getData().getVoiceList().get(0).getName(),
                10000, DataFormatUtil.getDateStrIncludeTimeZone());
        jsonArray.add(uploadSendBean);

        result_title.setText("1/" + voiceConfigBean.getData().getTotal() + "  " + name);
        speak(voiceConfigBean.getData().getVoiceList().get(0).getVoiceDesc());

    }

    /**
     * 设置推流参数
     */
    public void initLiveConfig(String message) {
        mLiveCameraView = (StreamLiveCameraView) findViewById(R.id.stream_previewView);

        //参数配置 start
        streamAVOption = new StreamAVOption();
        streamAVOption.streamUrl = message;
        //参数配置 end

        mLiveCameraView.init(this, streamAVOption);
//        mLiveCameraView.addStreamStateListener(resConnectionListener);
        LinkedList<BaseHardVideoFilter> files = new LinkedList<>();
    }

    RESConnectionListener resConnectionListener = new RESConnectionListener() {
        @Override
        public void onOpenConnectionResult(int result) {
            //result 0成功  1 失败
            Toast.makeText(LiveMainActivity.this, "打开推流连接 状态：" + result, Toast.LENGTH_LONG).show();

        }

        @Override
        public void onWriteError(int errno) {
            Toast.makeText(LiveMainActivity.this, "推流出错,请尝试重连", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCloseConnectionResult(int result) {
            //result 0成功  1 失败
            Toast.makeText(LiveMainActivity.this, "关闭推流连接 状态：" + result, Toast.LENGTH_LONG).show();
        }
    };


    private void connect(String ipAddress, String portAddress) {
        if (isConnected) {
            RxSocketManager.getInstance().close();
            //断开socket连接的同时停止推流

            if (mLiveCameraView.isStreaming()) {
                mLiveCameraView.stopStreaming();
            }
            return;
        }

        String ip = ipAddress;
        int port = Integer.valueOf(portAddress);
        RxSocketManager.getInstance().close();//创建之前 请现在关闭上一次的socket 释放资源
        RxSocketManager.getInstance()
                .setClient(SocketType.TCP, ip, port)
                .setResultCallback(new RxSocketManager.ResultCallback() {
                    @Override
                    public void onSucceed(byte[] data) {

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        Log.e("#count#", "当前时间为：" + sdf.format(new Date()));
                        count++;
                        Log.e("#count#", "推流次数为：" + count);

                        String rec = null;
                        try {
                            rec = new String(data, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
//                        String rec="{\"checkType\":3,\"code\":1,\"data\":{\"boxes\":\"[[360.49905859 148.72525561 517.2071292  345.49290021   0.99936312]\\n [254.98722434  59.73911428 349.79854032 180.87477799   0.99824762]]\",\"num\":2,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":1,\"data\":{\"boxes\":\"[[358.73372208 150.34423414 525.63462262 352.68975373   0.99979287]\\n [240.03068209  63.13652882 334.53340364 183.44977444   0.99355161]]\",\"num\":2,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":1,\"data\":{\"boxes\":\"[[243.94758384  40.05586407 339.75652967 157.07328446   0.99967062]\\n [357.88176507 112.03457971 524.15673418 318.1011143    0.99914634]]\",\"num\":2,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\":\"[]\",\"num\":0,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\":\"[]\",\"num\":0,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\":\"[]\",\"num\":0,\"gesturetype\":\"\",\"status\":1},\"msgId\":103}$_{\"checkType\":3,\"code\":-1,\"data\":{\"boxes\"";
                        int strCount = StringUtil.getKeyCount(rec, ConstantUtil.socketStr);

                        String subString = null;
                        if (strCount == 1) {
                            subString = rec.substring(0, rec.length() - 2);
                        } else if (strCount > 1) {
                            subString = rec.substring(0, rec.indexOf(ConstantUtil.socketStr));
                            Log.e("#li#", "----subString" + subString);
                        }
                        if (subString != null) {

                            Log.e("#li#", "----subString截取后" + subString);
                            logger.error("----subString截取后" + subString);

                            //输出日志
//                            showMsg_tv.setVisibility(View.VISIBLE);
//                            showMsg_tv.setText(subString);

                            Gson gson1 = new Gson();
                            SocketBean socketBean = gson1.fromJson(subString, SocketBean.class);
                            int code1 = socketBean.getCode();
                            String loginMsg = socketBean.getMsg();
                            int messageId1 = socketBean.getMsgId();
                            String message1 = socketBean.getRtmpUrl();
                            if (message1 != null && message1.contains("rtmp://")) {
                                String strUrl1 = message1.substring(0, 7);
                                String strUrl2 = message1.substring(message1.lastIndexOf(":"), message1.length());
                                message1 = strUrl1 + BaseApplication.getInstance().getBASE_IP() + strUrl2;
                            }


                            String uuid = socketBean.getUuid();
                            int checkType = socketBean.getCheckType();

                            //传递登录token后的返回值解析
                            if (code1 == 0 && messageId1 != 101 && !subString.contains("uuid")) {

                                //初始化ASR
                                initChatSDK();

                                Log.e("li", loginMsg);
                                logger.error(loginMsg);
                                /**----------------第一步：获取URL推流-------------------**/
                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("msgId", 101);
                                    jsonObject.put("msg", "");
                                    jsonObject.put("scanValue", scanValue);

                                    sendData(jsonObject.toString() + ConstantUtil.socketStr);
                                    issendBtn = false;
                                    ConstantUtil.getInstance().setTest(1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else if (code1 == -3 && messageId1 != 101) {
                                Log.e("li", loginMsg);
                                logger.error(loginMsg);
                            }

                            if (messageId1 == 101) { //获取推流成功
                                btnStartStreaming.setEnabled(true);
                                btnStartStreaming.setBackgroundResource(R.drawable.btn_selector);
                                btnStopStreaming.setEnabled(true);
                                btnStopStreaming.setBackgroundResource(R.drawable.btn_selector);

                                btnScreenshot.setEnabled(true);
                                btnStartRecord.setEnabled(true);
                                btnStopRecord.setEnabled(true);
                                btnScreenshot.setBackgroundResource(R.drawable.btn_selector);
                                btnStartRecord.setBackgroundResource(R.drawable.btn_selector);
                                btnStopRecord.setBackgroundResource(R.drawable.btn_selector);

                                ConstantUtil.getInstance().setRtmpUrl(message1);

                                Log.e("li", message1);

                                logger.error(message1);

                                //开始推流
                                if (!mLiveCameraView.isStreaming()) {

                                    mLiveCameraView.startStreaming(ConstantUtil.getInstance().getRtmpUrl());
                                }

                                /**----------------第二步：代理人人脸验证-------------------**/
//                                if (mLiveCameraView.isStreaming()) {
//                                    JsonUtils.jsonSend(102, "", 1);
//                                    ConstantUtil.getInstance().setTest(5);
//                                    ToastUtils.showToast(LiveMainActivity.this, "开始人脸验证");
//                                }

//                                    if (!issendBtn)
//                                        showRtmpUrlDialog(ConstantUtil.getInstance().getRtmpUrl());


                            } else if (messageId1 == 103 && code1 == 1) {

                                /**
                                 * 视频流连接成功
                                 */

                                speech_tips.setText(R.string.speech_tips2);

                                String voiceDesc = voiceConfigBean.getData().getVoiceList().get(0).getVoiceDesc();
                                String name = voiceConfigBean.getData().getVoiceList().get(0).getName();

                                //人脸同框
                                if (voiceDesc.contains("征求录像意见") || name.contains("征求录像意见")) {
                                    check_result_face.setVisibility(View.GONE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);
                                    /**----------------第三步：人脸同框-------------------**/
                                    JsonUtils.jsonSend(102, "", 3);
                                    ConstantUtil.getInstance().setAddstep(1);
                                    ToastUtils.showToast(LiveMainActivity.this, "开始人脸同框检测");

                                } else if (voiceDesc.contains("条款介绍") || name.contains("条款介绍")) {
                                    check_result_face.setVisibility(View.GONE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);
                                    /**----------------条款检测-------------------**/

                                    JsonUtils.jsonSend(102, "", 7);
                                    ConstantUtil.getInstance().setTest(3);
                                    //添加是为了区分和新增步骤相同检测类型但不同UI的情况
                                    ConstantUtil.getInstance().setAddstep(9);
                                } else if (voiceDesc.contains("产品说明书") || name.contains("产品说明书")) {
                                    check_result_face.setVisibility(View.GONE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);
                                    /**----------------产品说明书检测-------------------**/

                                    JsonUtils.jsonSend(102, "", 5);
                                    ConstantUtil.getInstance().setTest(3);
                                    //添加是为了区分和新增步骤相同检测类型但不同UI的情况
                                    ConstantUtil.getInstance().setAddstep(9);
                                } else if (voiceDesc.contains("签名确认") || name.contains("签名确认")) {
                                    check_result_face.setVisibility(View.GONE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);
                                    //发送签字动作
                                    /**---------------- 签字结果-------------------**/
                                    JsonUtils.jsonSend(102, "", 8);
                                    ConstantUtil.getInstance().setTest(7);
                                    ToastUtils.showToast(LiveMainActivity.this, "签字结果验证");
                                } else if (voiceDesc.contains("出示证件") || name.contains("出示证件")) {
                                    check_result_face.setVisibility(View.GONE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

                                    lLayout_camera_crop.setVisibility(View.VISIBLE);
                                    /**----------------第四步：身份证检测-------------------**/
                                    if (mLiveCameraView.isStreaming()) {
                                        try {
                                            JSONObject jsonCard = new JSONObject();
                                            jsonCard.put("cardId", staffCard);
                                            jsonCard.put("cardName", staffName);

                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("msgId", 102);
                                            jsonObject.put("msg", "");
                                            jsonObject.put("checkType", 4);
                                            jsonObject.put("checkValue", jsonCard.toString());

                                            sendData(jsonObject.toString() + ConstantUtil.socketStr);

                                            //添加是为了区分和新增步骤相同检测类型但不同UI的情况
                                            ConstantUtil.getInstance().setAddstep(4);
                                            ToastUtils.showToast(LiveMainActivity.this, "开始身份证校验");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                if (voiceDesc.contains("是否同意") || name.contains("是否同意")) {
                                    check_result_reply.setVisibility(View.VISIBLE);
                                    //语音播报完后进行投保人的语音识别
                                    Message msgVoice = new Message();
                                    msgVoice.what = VOICE_ASR;
                                    mHandler.sendMessage(msgVoice);
                                }
                                if (voiceDesc.contains("清楚吗") || name.contains("清楚吗") ||
                                        voiceDesc.contains("是否已清楚") || name.contains("是否已清楚")) {
                                    check_result_reply.setVisibility(View.VISIBLE);
                                    //语音播报完后进行投保人的语音识别
                                    Message msgVoice = new Message();
                                    msgVoice.what = VOICE_ASR;
                                    mHandler.sendMessage(msgVoice);
                                }
                                if (voiceDesc.contains("是其本人的证件嘛") || name.contains("是其本人的证件嘛")) {
                                    check_result_reply.setVisibility(View.VISIBLE);
                                    //语音播报完后进行投保人的语音识别
                                    Message msgVoice = new Message();
                                    msgVoice.what = VOICE_ASR;
                                    mHandler.sendMessage(msgVoice);
                                }
                                if (voiceDesc.contains("确认吗") || name.contains("确认吗")) {
                                    check_result_reply.setVisibility(View.VISIBLE);
                                    //语音播报完后进行投保人的语音识别
                                    Message msgVoice = new Message();
                                    msgVoice.what = VOICE_ASR;
                                    mHandler.sendMessage(msgVoice);
                                }

                            }

                            if (subString.contains("data") && subString.contains("uuid")) {
                                Gson gson = new Gson();
                                Face2FaceBean face2FaceBean = gson.fromJson(subString, Face2FaceBean.class);
                                int messageId = face2FaceBean.getMsgId();
                                int type = face2FaceBean.getCheckType();
                                int code = face2FaceBean.getCode();
                                String msg = face2FaceBean.getMsg();

//                                time_face_tv.setVisibility(View.VISIBLE);
//                                time_face_tv.setText(msg);

                                //矩形框的坐标
                                List<List<Float>> boxes = face2FaceBean.getData().getBoxes();
                                //手势
//                            String gesturetype = face2FaceBean.getData().getGesturetype();

                                ViewTreeObserver viewTreeObserver = mLiveCameraView.getViewTreeObserver();
                                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                    @Override
                                    public void onGlobalLayout() {
                                        mLiveCameraView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                                        Log.e("#li#", "Height---" + mLiveCameraView.getHeight()
//                                                + "Width---" + mLiveCameraView.getWidth());
                                    }
                                });

                                /**--------- 判断是否有值才绘制矩形框 ------------ **/
                                if (boxes != null && boxes.size() > 0 && ConstantUtil.getInstance().getAddstep() != 4) {

                                    if (circleImageView == null) {
                                        //动态添加矩形框
                                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                                                FrameLayout.LayoutParams.MATCH_PARENT);
                                        circleImageView = new CircleImageView(LiveMainActivity.this, boxes, mLiveCameraView.getHeight(), mLiveCameraView.getWidth());
                                        mLiveCameraView.addView(circleImageView, params);
                                    } else {
                                        //先移除
                                        mLiveCameraView.removeView(circleImageView);
                                        //动态添加矩形框
                                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                                                FrameLayout.LayoutParams.MATCH_PARENT);
                                        circleImageView = new CircleImageView(LiveMainActivity.this, boxes, mLiveCameraView.getHeight(), mLiveCameraView.getWidth());
                                        mLiveCameraView.addView(circleImageView, params);

                                    }
                                } else {
                                    if (circleImageView != null) {
                                        mLiveCameraView.removeView(circleImageView);
                                    }

                                }

                                /** ----------  start 新添加步骤不同UI的情况   -----------**/

                                //条款检测
                                if (code == 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 9) { //成功

                                    ConstantUtil.getInstance().setClause(true);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_clause);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));


                                } else if (code != 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 9) {//失败

                                    ConstantUtil.getInstance().setClause(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_error_product);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_clause);
                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }

                                /** ----------    end 新添加步骤不同UI的情况      -----------**/


                                //人脸同框
                                if (code == 1 && type == 3 && ConstantUtil.getInstance().getAddstep() == 1) { //成功

//                                    time_face_tv.setVisibility(View.VISIBLE);
//                                    time_face_tv.setText(msg);

                                    ConstantUtil.getInstance().setFace2Face(true);
                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_face2face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face2face_tv.setTextColor(Color.parseColor("#28FE37"));
                                    check_face2face_tv.setText(R.string.check_resule_face2face);

                                } else if (code != 1 && type == 3 && ConstantUtil.getInstance().getAddstep() == 1) { //失败

//                                    time_face_tv.setVisibility(View.VISIBLE);
//                                    time_face_tv.setText(msg);

                                    ConstantUtil.getInstance().setFace2Face(false);
                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_face2face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face2face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_face2face_tv.setText(R.string.check_resule_face2face);

                                }
                                //说明书检测
                                if (code == 1 && type == 5) {

                                    ConstantUtil.getInstance().setProduct(true);
                                    time_face_tv.setVisibility(View.GONE);
//                                    btnNext.setEnabled(true);
//                                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
//                                    btnNext.setBackgroundResource(R.drawable.btn_next_bg_normal);

                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_product);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));
                                } else if (code != 1 && type == 5) {

                                    ConstantUtil.getInstance().setProduct(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_error_product);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_result_product);
                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }
                                //条款检测
                                if (code == 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 1) { //成功

                                    ConstantUtil.getInstance().setClause(true);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_warn_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.GONE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

//                                    check_face_tv.setText("出示提示书");
                                    String insureContent = ConstantUtil.getInstance().getInsureContent();
                                    check_face_tv.setText(insureContent);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));

                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
                                    check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);

                                    speak("请投保人认真阅读，并签名确认 ");

                                    //发送签字动作
                                    /**----------------第六步： 签字动作和结果-------------------**/
                                    JsonUtils.jsonSend(102, "", 8);
                                    ConstantUtil.getInstance().setTest(7);
                                    ToastUtils.showToast(LiveMainActivity.this, "开始签字验证");

                                } else if (code != 1 && type == 7 && ConstantUtil.getInstance().getAddstep() == 1) {//失败

                                    ConstantUtil.getInstance().setClause(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_error_insure);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.GONE);
                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

//                                    check_face_tv.setText("出示提示书");
                                    String insureContent = ConstantUtil.getInstance().getInsureContent();
                                    check_face_tv.setText(insureContent);
                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
                                    check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);

                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }
                                //身份证检测
                                if (code == 1 && type == 4 && ConstantUtil.getInstance().getAddstep() == 4) { //成功

                                    ConstantUtil.getInstance().setCard(true);
                                    lLayout_camera_crop.setVisibility(View.GONE); //证件检测成功后取消遮罩层
                                    time_face_tv.setVisibility(View.GONE);
                                    warn_tv.setText(R.string.check_idcard_tip);
                                    warn_tv.setTextColor(Color.parseColor("#FFFFFF"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_show_idcard);
                                    check_face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face_tv.setTextColor(Color.parseColor("#28FE37"));

                                } else if (code != 1 && type == 4 && ConstantUtil.getInstance().getAddstep() == 4) { //失败

//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                                    Log.e("#count#", "当前时间为："+ sdf.format(new Date()) );
//                                    count++;
//                                    Log.e("#count#", "推流次数为："+count );

                                    ConstantUtil.getInstance().setCard(false);
                                    lLayout_camera_crop.setVisibility(View.VISIBLE);//证件检测没有成功显示遮罩层
                                    time_face_tv.setVisibility(View.GONE);

                                    warn_tv.setText(R.string.check_idcard_error_tip);
                                    warn_tv.setTextColor(Color.parseColor("#E53C3C"));
                                    check_result_face.setVisibility(View.VISIBLE);
                                    check_result_face2face.setVisibility(View.GONE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face_tv.setText(R.string.check_show_idcard);
                                    check_face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face_tv.setTextColor(Color.parseColor("#E53C3C"));

                                }
                                //签字区域校验
                                if (code == 1 && type == 8) {

                                    ConstantUtil.getInstance().setSign(true);
                                    time_face_tv.setVisibility(View.GONE);

                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_imgv.setImageResource(R.mipmap.result_yes);
                                    check_face2face_tv.setTextColor(Color.parseColor("#28FE37"));

//                                    speak("请代理人出示签字结果，并在镜头前展示三秒");

                                } else if (code != 1 && type == 8) {

                                    ConstantUtil.getInstance().setSign(false);
                                    time_face_tv.setVisibility(View.GONE);

                                    check_result_face2face.setVisibility(View.VISIBLE);
                                    check_result_reply.setVisibility(View.GONE);

                                    check_face2face_tv.setText(R.string.check_result_insure);
                                    check_face2face_imgv.setImageResource(R.mipmap.result_no);
                                    check_face2face_tv.setTextColor(Color.parseColor("#E53C3C"));
                                }

                            }

                        }

                    }

                    @Override
                    public void onFailed(Throwable t) {
//                        ToastUtil.s("返回失败");
                        t.printStackTrace();
                    }
                })
                .setSocketStatusListener(new RxSocketManager.OnSocketStatusListener() {
                    @Override
                    public void onConnectSucceed() {

                        LiveMainActivity.this.runOnUiThread(() -> {
                            Log.d("li", "------------onConnectSucceed");
                            logger.error("------------onConnectSucceed");
//                            ToastUtils.showToast(LiveMainActivity.this, "已连接");

                            scanValue = getIntent().getStringExtra("scanValue");

                            Gson gson = new Gson();
                            ScanBean scanBean = gson.fromJson(scanValue, ScanBean.class);
                            staffCard = scanBean.getStaffCard();
                            staffName = scanBean.getStaffName();
                            organizationCode = scanBean.getOrganizationCode();

                            Log.e("llll", scanValue);
                            logger.error(scanValue);
//                            scanValue = "{\"policyNum\":\"123456789\",\"insureType\":1,\"insureTypeDesc\":\"保险类型1\",\"policyName\":\"张晓红\",\"policyCard\":\"445464564656842185\",\"organizationCode\":\"597456\",\"staffNum\":\"123456\",\"staffName\":\"黄社会\",\"staffCard\":\"4455465612553156656\"}";
                            SharedPreferencesUtil.putString(LiveMainActivity.this, SharedPreferencesUtil.SOCKETIP, ipAddress);
                            SharedPreferencesUtil.putString(LiveMainActivity.this, SharedPreferencesUtil.SOCKETPORT, portAddress);

                            btngetUrl.setEnabled(true);
                            btngetUrl.setBackgroundResource(R.drawable.btn_selector);

                            /**
                             * 开始发送登录token给后台
                             */
                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("token", loginToken);

                                sendData(jsonObject.toString() + ConstantUtil.socketStr);
                                issendBtn = false;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            isConnected = true;
                        });
                    }

                    @Override
                    public void onConnectFailed() {
                        Log.d("li", "------------onConnectFailed");
                        logger.error("------------onConnectFailed");
                    }

                    @Override
                    public void onDisConnected() {
                        LiveMainActivity.this.runOnUiThread(() -> {
                            Log.d("li", "------------onDisConnected");
                            logger.error("------------onDisConnected");
//                            ToastUtils.showToast(LiveMainActivity.this, "已断开连接");
                            isConnected = false;
                            //断开连接的同时不显示检测人数
                            msg.setVisibility(View.GONE);

                            //断开的同时取消绘制框
                            if (circleImageView != null) {
                                mLiveCameraView.removeView(circleImageView);
                            }

                            //停止推流
                            if (mLiveCameraView.isStreaming()) {
                                mLiveCameraView.stopStreaming();

                                if (timerFace != null) {
                                    timerFace.cancel();
                                }
                                if (voiceasr != null) {
                                    voiceasr.cancel();
                                }
                            }
                            btnStartStreaming.setEnabled(false);
                            btnStopStreaming.setEnabled(false);
                            btngetUrl.setEnabled(false);
                        });
                    }
                }).build();

    }

    private void sendData(String data) {
        try {
            byte[] bytes = data.getBytes();
            RxSocketManager.getInstance().send(bytes, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.btn_startStreaming, R.id.btn_stopStreaming, R.id.btn_startRecord, R.id.btn_stopRecord,
            R.id.btn_filter, R.id.btn_swapCamera, R.id.btn_screenshot, R.id.btn_mirror, R.id.iv_image,
            R.id.btn_socket, R.id.btn_getUrl, R.id.btn_next, R.id.btn_success, R.id.btn_record,
            R.id.setting_imgv, R.id.close_live_imgv
    })
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_screenshot://截帧
                mLiveCameraView.takeScreenShot(new RESScreenShotListener() {
                    @Override
                    public void onScreenShotResult(Bitmap bitmap) {
                        if (bitmap != null) {
                            imageView.setVisibility(View.VISIBLE);
                            imageView.setImageBitmap(bitmap);
                            Log.e("###", bitmap.getWidth() + "" + bitmap.getHeight());
                        }

                    }
                });
                break;

            case R.id.close_live_imgv: //关闭录制主界面

                failList.clear();
                //退出按钮删除掉没有保存的视频文件
                File file = new File(videoUrl);
                if (file.exists()) {
                    file.delete();
                }

                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                }
                Intent intentMain=new Intent(LiveMainActivity.this,
                        MainPagerActivity.class);
                startActivity(intentMain);
                ConstantUtil.getInstance().setUploadToMain(true);
                finish();
//                System.exit(0);

                break;

//            case R.id.btn_screenshot://加个测试
//                JsonUtils.jsonSend(102, "", 8);
//                ConstantUtil.getInstance().setTest(10);
//                //添加是为了区分和新增步骤相同检测类型但不同UI的情况
//                ConstantUtil.getInstance().setAddstep(10);
//                ToastUtils.showToast(LiveMainActivity.this, "测试类型8");
//                break;

            case R.id.btn_stopStreaming://停止推流
                if (mLiveCameraView.isStreaming()) {
                    mLiveCameraView.stopStreaming();
                }
                //停止推流的同时断开socket连接
                if (isConnected) {
                    RxSocketManager.getInstance().close();
                    return;
                }
                break;
            case R.id.btn_startRecord://开始录制
                if (!mLiveCameraView.isRecord()) {
                    ToastUtils.showToast(LiveMainActivity.this, "开始录制视频");
                    //动态权限申请

                    //处理点击事件
                    mLiveCameraView.startRecord(login_username);
//                    mLiveCameraView.startRecord();
                    chronometer.setFormat("%s");
                    chronometer.setBase(SystemClock.elapsedRealtime());//计时器清零,必须
                    chronometer.start();

                }
                break;
            case R.id.btn_stopRecord://停止录制
                if (mLiveCameraView.isRecord()) {
                    mLiveCameraView.stopRecord();
                    ToastUtils.showToast(LiveMainActivity.this, "视频已成功保存至系统根目录的live/文件夹中");

                }
                chronometer.stop();

                break;

            case R.id.btn_swapCamera://切换摄像头
                mLiveCameraView.swapCamera();

                break;

            case R.id.btn_mirror://镜像
                if (isMirror) {
                    mLiveCameraView.setMirror(true, false, false);
                } else {
                    mLiveCameraView.setMirror(true, true, true);
                }
                isMirror = !isMirror;

                break;

            case R.id.iv_image:
                imageView.setVisibility(View.GONE);
                break;

            case R.id.setting_imgv:
//                showSocketDialog();
                break;
            case R.id.btn_success:
                /**
                 * 完成按钮点击
                 **/

                //录制计时结束
                chronometer.stop();
                //语音播报结束了才可以点击下一步
                if (!ConstantUtil.getInstance().isVoiceing()) {

                    //注销百度TTS
                    SpeechManager.getInstance().onDestroy();
                    if (speechSynthesizer != null) {
                        speechSynthesizer.release();
                        speechSynthesizer.stop();
                    }

                    //跳转到视频录制完成详情界面
                    if (ClickUtil.isNotFastClick()) {

                        /** -------上一步骤是以下某个检测项的时候。---- */
                        //上一个步骤的值
                        String voiceDescBack = voiceConfigBean.getData().getVoiceList().get(voiceConfigBean.getData().getTotal()-1).getVoiceDesc();
                        String nameBack = voiceConfigBean.getData().getVoiceList().get(voiceConfigBean.getData().getTotal()-1).getName();
                        stepBackCheck(nameBack,voiceDescBack);

                        if (mLiveCameraView.isRecord()) {
                            mLiveCameraView.stopRecord();
                            ToastUtils.showToast(LiveMainActivity.this, "视频已成功保存至系统根目录的live/文件夹中");

                        }
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }

                        Intent intentDone = new Intent(LiveMainActivity.this,
                                VideoDetailDoneActivity.class);
                        intentDone.putExtra("scanValue", scanValue);
                        intentDone.putExtra("result", jsonArray.toString());
                        intentDone.putExtra("recordTime", recordTime);
                        intentDone.putExtra("failList", (Serializable) failList);
                        startActivity(intentDone);
                        finish();
//                        System.exit(0);

                        //停止录屏
                        //处理点击事件
//                        Intent intentLogin = new Intent(LiveMainActivity.this,
//                                RecordmainActivity.class);
//                        startActivity(intentLogin);
                    }
                }

                break;

            case R.id.btn_next:

                //语音播报结束了才可以点击下一步
                if (!ConstantUtil.getInstance().isVoiceing() && voiceConfigBean.getData().getTotal() > 1) {
                    nextFlag++;
                    voiceNextFlag++;

                    String voiceDesc = voiceConfigBean.getData().getVoiceList().get(nextFlag).getVoiceDesc();
                    String name = voiceConfigBean.getData().getVoiceList().get(nextFlag).getName();

                    //上一个步骤的值
                    String voiceDescBack = voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getVoiceDesc();
                    String nameBack = voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getName();

                    if (voiceDesc.contains("征求录像意见") || name.contains("征求录像意见")) {
                        /** -------上一步骤是以下某个检测项的时候。
                         * 顺序要在jsonArray添加uploadSendBean之前---- */
                        stepBackCheck(nameBack,voiceDescBack);
                        uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(nextFlag).getId(),
                                1, voiceConfigBean.getData().getVoiceList().get(nextFlag).getName(),
                                10000, DataFormatUtil.getDateStrIncludeTimeZone());
                        jsonArray.add(uploadSendBean);

                        if (nextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                            nextStepFace2Face(name, voiceDesc, nextFlag);
                            btnNext.setVisibility(View.GONE);
                            btnSuccess.setVisibility(View.VISIBLE);
                        } else {
                            nextStepFace2Face(name, voiceDesc, nextFlag);
                            btnNext.setVisibility(View.VISIBLE);
                            btnSuccess.setVisibility(View.GONE);
                        }
                    } else if (voiceDesc.contains("条款介绍") || name.contains("条款介绍")) {
                        /** -------上一步骤是以下某个检测项的时候。
                         * 顺序要在jsonArray添加uploadSendBean之前---- */
                        stepBackCheck(nameBack,voiceDescBack);
                        uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(nextFlag).getId(),
                                1, voiceConfigBean.getData().getVoiceList().get(nextFlag).getName(),
                                10000, DataFormatUtil.getDateStrIncludeTimeZone());
                        jsonArray.add(uploadSendBean);

                        if (nextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                            nextStepClause(name, voiceDesc);
                            btnNext.setVisibility(View.GONE);
                            btnSuccess.setVisibility(View.VISIBLE);
                        } else {
                            nextStepClause(name, voiceDesc);
                            btnNext.setVisibility(View.VISIBLE);
                            btnSuccess.setVisibility(View.GONE);
                        }

                    } else if (voiceDesc.contains("产品说明书") || name.contains("产品说明书")) {
                        /** -------上一步骤是以下某个检测项的时候。
                         * 顺序要在jsonArray添加uploadSendBean之前---- */
                        stepBackCheck(nameBack,voiceDescBack);
                        uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(nextFlag).getId(),
                                1, voiceConfigBean.getData().getVoiceList().get(nextFlag).getName(),
                                10000, DataFormatUtil.getDateStrIncludeTimeZone());
                        jsonArray.add(uploadSendBean);

                        if (nextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                            nextStepProduct(name, voiceDesc);
                            btnNext.setVisibility(View.GONE);
                            btnSuccess.setVisibility(View.VISIBLE);
                        } else {
                            nextStepProduct(name, voiceDesc);
                            btnNext.setVisibility(View.VISIBLE);
                            btnSuccess.setVisibility(View.GONE);
                        }
                    } else if (voiceDesc.contains("签名确认") || name.contains("签名确认")) {
                        /** -------上一步骤是以下某个检测项的时候。
                         * 顺序要在jsonArray添加uploadSendBean之前---- */
                        stepBackCheck(nameBack,voiceDescBack);
                        uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(nextFlag).getId(),
                                1, voiceConfigBean.getData().getVoiceList().get(nextFlag).getName(),
                                10000, DataFormatUtil.getDateStrIncludeTimeZone());
                        jsonArray.add(uploadSendBean);
                        if (nextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                            nextStepSign(name, voiceDesc);

                            btnNext.setVisibility(View.GONE);
                            btnSuccess.setVisibility(View.VISIBLE);
                        } else {
                            nextStepSign(name, voiceDesc);

                            btnNext.setVisibility(View.VISIBLE);
                            btnSuccess.setVisibility(View.GONE);
                        }
                    } else if (voiceDesc.contains("出示证件") || name.contains("出示证件")) {

                        /** -------上一步骤是以下某个检测项的时候。
                         * 顺序要在jsonArray添加uploadSendBean之前---- */
                        stepBackCheck(nameBack,voiceDescBack);

                        uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(nextFlag).getId(),
                                1, voiceConfigBean.getData().getVoiceList().get(nextFlag).getName(),
                                10000, DataFormatUtil.getDateStrIncludeTimeZone());
                        jsonArray.add(uploadSendBean);

                        if (nextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                            nextStepCard(name, voiceDesc, nextFlag);
                            btnNext.setVisibility(View.GONE);
                            btnSuccess.setVisibility(View.VISIBLE);
                        } else {
                            nextStepCard(name, voiceDesc, nextFlag);
                            btnNext.setVisibility(View.VISIBLE);
                            btnSuccess.setVisibility(View.GONE);
                        }
                    }
                    if (  voiceDesc.contains("是否同意") || name.contains("是否同意") ||
                            voiceDesc.contains("清楚吗") || name.contains("清楚吗") ||
                            voiceDesc.contains("是否已清楚") || name.contains("是否已清楚") ||
                            voiceDesc.contains("是其本人的证件嘛") || name.contains("是其本人的证件嘛") ||
                            voiceDesc.contains("确认吗") || name.contains("确认吗") ) {
                        /** -------上一步骤是以下某个检测项的时候。
                         * 顺序要在jsonArray添加uploadSendBean之前---- */
                        stepBackCheck(nameBack,voiceDescBack);
                        uploadSendBean = new UploadSendBean(voiceConfigBean.getData().getVoiceList().get(nextFlag).getId(),
                                1, voiceConfigBean.getData().getVoiceList().get(nextFlag).getName(),
                                10000, DataFormatUtil.getDateStrIncludeTimeZone());
                        jsonArray.add(uploadSendBean);

                        String tipsStr = "";
                        if (voiceDesc.contains("是否同意") || name.contains("是否同意")) {
                            tipsStr = "请你回答同意或不同意";
                        } else if (voiceDesc.contains("清楚吗") || name.contains("清楚吗") ||
                                voiceDesc.contains("是否已清楚") || name.contains("是否已清楚")) {
                            tipsStr = "请你回答清楚或不清楚";
                        } else if (voiceDesc.contains("是其本人的证件嘛") || name.contains("是其本人的证件嘛")) {
                            tipsStr = "请你回答是的或不是";
                        } else if (voiceDesc.contains("确认吗") || name.contains("确认吗")) {
                            tipsStr = "请你回答确认或不确认";
                        }

                        if (nextFlag + 1 == voiceConfigBean.getData().getTotal()) { //如果是最后一步
                            nextStepVoice(name, voiceDesc, tipsStr);
                            btnNext.setVisibility(View.GONE);
                            btnSuccess.setVisibility(View.VISIBLE);
                        } else {
                            nextStepVoice(name, voiceDesc, tipsStr);
                            btnNext.setVisibility(View.VISIBLE);
                            btnSuccess.setVisibility(View.GONE);
                        }
                    }

                    nextFlag = (nextFlag + 1) % voiceConfigBean.getData().getTotal() - 1;

                }
                break;
            case R.id.btn_record:
                break;

            default:
                break;
        }
    }

    private void stepBackCheck(String nameBack,String voiceDescBack){
        /** -------上一步骤是以下某个检测项的时候。---- */
        if (voiceDescBack.contains("征求录像意见") || nameBack.contains("征求录像意见")) {
            if (ConstantUtil.getInstance().isFace2Face()) {
                uploadSendBean.setState(1);
            } else {
                FailListBean bean = new FailListBean(voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getName(),
                        voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getId(), "代理人与投保人人脸同框");
                failList.add(bean);
                uploadSendBean.setState(0);
            }
        } else if (voiceDescBack.contains("条款介绍") || nameBack.contains("条款介绍")) {
            if (ConstantUtil.getInstance().isClause()) {
                uploadSendBean.setState(1);
            } else {
                FailListBean bean = new FailListBean(voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getName(),
                        voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getId(), "出示清晰的条款提示书");
                failList.add(bean);

                uploadSendBean.setState(0);
            }
        } else if (voiceDescBack.contains("产品说明书") || nameBack.contains("产品说明书")) {
            if (ConstantUtil.getInstance().isProduct()) {
                uploadSendBean.setState(1);
            } else {
                FailListBean bean = new FailListBean(voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getName(),
                        voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getId(), "出示产品说明书");
                failList.add(bean);
                uploadSendBean.setState(0);
            }
        } else if (voiceDescBack.contains("签名确认") || nameBack.contains("签名确认")) {
            if (ConstantUtil.getInstance().isSign()) {
                uploadSendBean.setState(1);
            } else {
                FailListBean bean = new FailListBean(voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getName(),
                        voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getId(), "签字结果可见");
                failList.add(bean);

                uploadSendBean.setState(0);
            }
        } else if (voiceDescBack.contains("出示证件") || nameBack.contains("出示证件")) {
            if (ConstantUtil.getInstance().isCard()) {
                uploadSendBean.setState(1);
            } else {
                FailListBean bean = new FailListBean(voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getName(),
                        voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getId(), "出示有效身份证件");
                failList.add(bean);

                uploadSendBean.setState(0);
            }
        }
        else if ( voiceDescBack.contains("是否同意") || nameBack.contains("是否同意") ||
                voiceDescBack.contains("清楚吗") || nameBack.contains("清楚吗") ||
                voiceDescBack.contains("是否已清楚") || nameBack.contains("是否已清楚") ||
                voiceDescBack.contains("是其本人的证件嘛") || nameBack.contains("是其本人的证件嘛") ||
                voiceDescBack.contains("确认吗") || nameBack.contains("确认吗") ) {
            if (ConstantUtil.getInstance().isAgree()) {
                uploadSendBean.setState(1);
            } else {
                if (nextFlag  == -1 ) { //如果是最后一步
                    FailListBean bean = new FailListBean(voiceConfigBean.getData().getVoiceList().get(voiceConfigBean.getData().getTotal() - 1).getName(),
                            voiceConfigBean.getData().getVoiceList().get(voiceConfigBean.getData().getTotal() - 1).getId(), "投保人明确答复");
                    failList.add(bean);

                    uploadSendBean.setState(0);
                }else {
                    FailListBean bean = new FailListBean(voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getName(),
                            voiceConfigBean.getData().getVoiceList().get(nextFlag - 1).getId(), "投保人明确答复");
                    failList.add(bean);

                    uploadSendBean.setState(0);
                }
            }
        }
        /** -------                   end ----------     **/
    }

    private void nextStepFace2Face(String name, String voiceDesc, int nextFlag) {  //当人脸同框不是第一步的时候，人脸同框

        if (timerFace != null) {
            timerFace.cancel();
        }
        if (voiceasr != null) {
            voiceasr.cancel();
        }

        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.VISIBLE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText((nextFlag + 1) + "/" + voiceConfigBean.getData().getTotal() + "  " + name);
        speech_tips.setText("请代理人与投保人保持同框");

        voice_tv.setText(voiceDesc);

        check_face2face_tv.setText(R.string.check_resule_face2face);
        check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);

        speak(voiceDesc);
        /**----------------第三步：人脸同框-------------------**/
        JsonUtils.jsonSend(102, "", 3);
        ConstantUtil.getInstance().setAddstep(1);
        ToastUtils.showToast(LiveMainActivity.this, "开始人脸同框检测");
    }

    private void nextStepProduct(String name, String voiceDesc) { //产品说明书

        time_face_tv.setVisibility(View.VISIBLE);
        showMsg_tv.setVisibility(View.VISIBLE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText((nextFlag + 1) + "/" + voiceConfigBean.getData().getTotal() + "  " + name);
        speech_tips.setText("请代理人打开产品说明书，在镜头前展示三秒");
        voice_tv.setText(voiceDesc);


        check_face_tv.setText("产品说明书");
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);


        speak(voiceDesc);

        /**----------------第五步：产品说明书检测-------------------**/

        JsonUtils.jsonSend(102, "", 5);
        ConstantUtil.getInstance().setTest(3);

    }

    private void nextStepClause(String name, String voiceDesc) { //条款介绍
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText((nextFlag + 1) + "/" + voiceConfigBean.getData().getTotal() + "  " + name);
        speech_tips.setText("请代理人打开条款提示书，在镜头前展示三秒");
        voice_tv.setText(voiceDesc);


        check_face_tv.setText(R.string.check_result_clause);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);


        speak(voiceDesc);

        /**----------------第五步：条款检测-------------------**/

        JsonUtils.jsonSend(102, "", 7);
        ConstantUtil.getInstance().setTest(3);
        //添加是为了区分和新增步骤相同检测类型但不同UI的情况
        ConstantUtil.getInstance().setAddstep(9);

    }

    private void nextStepCard(String name, String voiceDesc, int nextFlag) {  //代理人证件检测

        if (timerFace != null) {
            timerFace.cancel();
        }
        if (voiceasr != null) {
            voiceasr.cancel();
        }

        speech_tips.setVisibility(View.VISIBLE);

        lLayout_camera_crop.setVisibility(View.VISIBLE);
        /**----------------第四步：身份证检测-------------------**/
        if (mLiveCameraView.isStreaming()) {
            try {
                JSONObject jsonCard = new JSONObject();
                jsonCard.put("cardId", staffCard);
                jsonCard.put("cardName", staffName);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("msgId", 102);
                jsonObject.put("msg", "");
                jsonObject.put("checkType", 4);
                jsonObject.put("checkValue", jsonCard.toString());

                sendData(jsonObject.toString() + ConstantUtil.socketStr);

                //添加是为了区分和新增步骤相同检测类型但不同UI的情况
                ConstantUtil.getInstance().setAddstep(4);
                ToastUtils.showToast(LiveMainActivity.this, "开始身份证校验");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        check_result_face.setVisibility(View.VISIBLE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText((nextFlag + 1) + "/" + voiceConfigBean.getData().getTotal() + "  " + name);
        speech_tips.setVisibility(View.VISIBLE);
        speech_tips.setText("请将证件出示在提示框内,保持3秒");
        voice_tv.setText(voiceDesc);

        check_face_tv.setText(R.string.check_show_idcard);
        check_face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face_imgv.setImageResource(R.mipmap.no_detection_dot);

        speak(voiceDesc);

    }

    private void nextStepVoice(String name, String voiceDesc, String tipsStr) {  //语音

        if (timerFace != null) {
            timerFace.cancel();
        }
        if (voiceasr != null) {
            voiceasr.cancel();
        }
        JsonUtils.jsonSend(102, "", 0);
        time_face_tv.setVisibility(View.GONE);
        showMsg_tv.setVisibility(View.GONE);
        speech_tips.setVisibility(View.VISIBLE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.GONE);
        check_result_reply.setVisibility(View.VISIBLE);

        result_title.setText((nextFlag + 1) + "/" + voiceConfigBean.getData().getTotal() + "  " + name);
        speech_tips.setText(tipsStr);

        voice_tv.setText(voiceDesc);

        check_reply_tv.setText(R.string.check_resule_reply);
        check_reply_tv.setTextColor(Color.parseColor("#CACACA"));
        check_reply_imgv.setImageResource(R.mipmap.no_detection_dot);

        ConstantUtil.getInstance().setAgree(false);
        speak(voiceDesc);
        //语音播报完后进行投保人的语音识别
        Message msgVoice = new Message();
        msgVoice.what = VOICE_ASR;
        mHandler.sendMessage(msgVoice);
    }

    private void nextStepSign(String name, String voiceDesc) { //签字结果

        speech_tips.setVisibility(View.GONE);
        lLayout_camera_crop.setVisibility(View.GONE);

        check_result_face.setVisibility(View.GONE);
        check_result_face2face.setVisibility(View.VISIBLE);
        check_result_reply.setVisibility(View.GONE);

        result_title.setText((nextFlag + 1) + "/" + voiceConfigBean.getData().getTotal() + "  " + name);
        voice_tv.setText(voiceDesc);

        check_face2face_tv.setText(R.string.check_result_insure);
        check_face2face_tv.setTextColor(Color.parseColor("#CACACA"));
        check_face2face_imgv.setImageResource(R.mipmap.no_detection_dot);


        speak(voiceDesc);

        //发送投保提示书签字动作
        /**----------------第六步： 签字动作和结果-------------------**/
        JsonUtils.jsonSend(102, "", 8);
        ConstantUtil.getInstance().setTest(7);
        ToastUtils.showToast(LiveMainActivity.this, "开始签字验证");

    }

    protected void speak(String text) {

        speechSynthesizer.speak(text);
//        int re = this.speechSynthesizer.speak(text);
//        if (re < 0) {
//            ToastUtils.showToast(LiveMainActivity.this, "error to speak");
//        }
    }

    //初始化TTS
    private void initTTS() {
        this.speechSynthesizer = SpeechSynthesizer.getInstance();
        this.speechSynthesizer.setContext(this);

//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME, "5");//音量 范围["0" - "15"], 不支持小数。 "0" 最轻，"15" 最响。
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "5");//语速 范围["0" - "15"], 不支持小数。 "0" 最慢，"15" 最快
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_PITCH, "5");//语调 范围["0" - "15"], 不支持小数。 "0" 最慢，"15" 最快

        //文本模型路径（离线）
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_TEXT_MODEL_FILE, mSampleDirPath + "/" + TTS_TEXT_MODEL_FILE);
        //设置声学模型（男声、女生）
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_SPEECH_MODEL_FILE, mSampleDirPath + "/" + TTS_SPEECH_MODEL_FILE);
        //发声人
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "0");
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_MIX_MODE, SpeechSynthesizer.MIX_MODE_HIGH_SPEED_SYNTHESIZE);
        this.speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "8");//语速 范围["0" - "15"], 不支持小数。 "0" 最慢，"15" 最快

        //请填写你申请到的appid、apikey
        this.speechSynthesizer.setAppId("18168798");
        this.speechSynthesizer.setApiKey("gWVMBAKAMAO73oqXVlmP8rGi", "CNt8yZeH0wlKgGQx9WaRb6lhbBK7fT5G");
        //授权检测接口
        AuthInfo authInfo = this.speechSynthesizer.auth(TtsMode.MIX);

        if (authInfo.isSuccess()) {
            Log.e("li", "connected successed");
        } else {
            Log.e("li", "connected failed");
            String errorMsg = authInfo.getTtsError().getDetailMessage();
        }

        //初始化tts
//        speechSynthesizer.initTts(TtsMode.MIX);
        speechSynthesizer.initTts(TtsMode.ONLINE);

        this.speechSynthesizer.setSpeechSynthesizerListener(new SpeechSynthesizerListener() {
            @Override
            public void onSynthesizeStart(String s) {
                Log.e("lihhh", "onSynthesizeStart------");
                ConstantUtil.getInstance().setVoiceing(true);
            }

            @Override
            public void onSynthesizeDataArrived(String s, byte[] bytes, int i) {
                Log.e("lihhh", "onSynthesizeDataArrived------");

            }

            @Override
            public void onSynthesizeFinish(String s) {
                Log.e("lihhh", "onSynthesizeFinish------");
            }

            @Override
            public void onSpeechStart(String s) {

                Log.e("lihhh", "onSpeechStart------");

            }

            @Override
            public void onSpeechProgressChanged(String s, int i) {
                Log.e("lihhh", "onSpeechProgressChanged------");
            }

            @Override
            public void onSpeechFinish(String s) {
                Log.e("lihhh", "onSpeechFinish------");
                ConstantUtil.getInstance().setVoiceing(false); //结束了才可以点击下一步
            }

            @Override
            public void onError(String s, SpeechError speechError) {
                Log.e("lihhh", "onError------");
                ConstantUtil.getInstance().setVoiceing(false); //报错异常也可以点击下一步
            }
        });
    }

    private void initEnv() {
        if (mSampleDirPath == null) {
            String path = Environment.getExternalStorageDirectory().toString();
            mSampleDirPath = path + "/" + "ASR_TTS";
            File file = new File(mSampleDirPath);
            if (!file.exists()) {
                file.mkdirs();
            }
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            ToastUtils.showToast(LiveMainActivity.this, "再按一次退出当前录制");
            exitTime = System.currentTimeMillis();
        } else {
            Intent intentMain=new Intent(LiveMainActivity.this,
                    MainPagerActivity.class);
            startActivity(intentMain);
            ConstantUtil.getInstance().setUploadToMain(true);
            finish();
//            System.exit(0);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLiveCameraView.destroy();
        if (timerFace != null) {
            timerFace.cancel();
            timerFace = null;
        }
        if (voiceasr != null) {
            voiceasr.cancel();
            voiceasr = null;
        }

        SpeechManager.getInstance().onDestroy();
        if (speechSynthesizer != null) {
            speechSynthesizer.release();
            speechSynthesizer.stop();
        }

        if (mLiveCameraView.isStreaming()) {
            mLiveCameraView.stopStreaming();
        }
        //停止推流的同时断开socket连接
        if (isConnected) {
            RxSocketManager.getInstance().close();
            return;
        }

        chronometer.stop();

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 960;
    }

}
