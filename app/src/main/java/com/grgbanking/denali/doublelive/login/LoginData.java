/*
 *     (C) Copyright 2019, ForgetSky.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.grgbanking.denali.doublelive.login;

public class LoginData {


    /**
     * code : 0
     * msg : 成功
     * data : {"access_token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRUYWciOm51bGwsInVzZXJfbmFtZSI6ImFkbWluIiwicm9sZVR5cGUiOm51bGwsImF1dGhvcml0aWVzIjpbImFkZEJ1Y2tDZ2RzIiwic2VlQ2VudGVyIiwicm9sZXNTdGF0dXMiLCJlZGl0X3NwY19kYXRhc291cmNlIiwic2VhcmNoX2F1dGgiLCJkZWxldGVEc0dyb3VwIiwic2VlQXBwcyIsImRsdF9zcGNfZGF0YXNvdXJjZSIsImRlbGV0ZURvY3VtZW50IiwiZWRpdFR5cGUiLCJkZWxldGVDZ2RzR3JvdXAiLCJmaW5kUmVxdWVzdE1vZGVsIiwiZWRpdHNlcnZpY2UiLCJkb1VwbG9hZEFwcHMiLCJ2aWV3X2RldGFpbCIsInJvbGVzRGVsIiwiYWxnb0FkZCIsImVkaXRUZW5hbnQiLCJsaW5rQ2dkcyIsImNyZWF0ZUFwcCIsImZpbmRDZ2RzIiwic2VlUmVuZXdhTGlzdCIsImRvd25sb2FkQ29uZmlnIiwiZ3JvdXBzRGVsIiwiZWRpdERzR3JvdXAiLCJkb3dubG9hZERhdGEiLCJlZGl0Q29udGFjdCIsInVzZXJTdGF0dXMiLCJ2aWV3LXRlbXBsZXRlIiwiYWRkVGVuYW50IiwiYWRkRHNHcm91cCIsImZpbmlzaFJlcXVlc3QiLCJmaW5kQXBwc1J1biIsImFkZF9zcGNfYm9hcmQiLCJlbmFibGVTZXJ2aWNlIiwiZGVwbG95QXBwIiwiYWRkX2NoYXJ0Iiwic2VlIiwiZmluZEFwcHNMaXN0IiwiY2xvc2VSZXF1ZXN0IiwiaW52b2tlTGljZW5jZSIsImRlbGV0ZU9yZGVyIiwiYWRkR3JvdXAiLCJlZGl0UHJvamVjdCIsInNlZVRlbmFudExpc3QiLCJzdGFydFJlcXVlc3RzIiwiZWRpdENnZHMiLCJncm91cFN0YXR1cyIsInJvbGVzRWRpdCIsImZpbmRSb2xlcyIsImFwcEVkaXQiLCJmaW5kV29ya1NwYWNlIiwiYWRkUHJvamVjdCIsInNlZVNlcnZpY2VEZXRhaWwiLCJkZWxldGVDb250YWN0IiwiZGx0X3NwY19ib2FyZCIsInNlbmRSZXF1ZXN0IiwiZmluZFByb2R1Y3RDb25maWciLCJkZWxldGVSZXF1ZXN0IiwicmViYWNrUmVxdWVzdCIsInNlZVNlcnZpY2VMaXN0IiwiZmluZERhdGEiLCJnb1BheSIsImVkaXRDdXN0b21lciIsImF1dGhFZGl0IiwiZGVsZXRlU2VydmljZSIsImVkaXRVc2VyIiwiYWRkQ2dkc0dyb3VwIiwiZGlzYWJsZVNlcnZpY2UiLCJwdXNoUmVxdWVzdHMiLCJhbGdvRWRpdCIsImFkZEN1c3RvbWVyIiwiYWxnb1F1ZXJ5IiwiZWRpdENnZHNHcm91cCIsImZpbmRBbGxQcm9qZWN0cyIsImFkZERvY3VtZW50IiwiYWRkIiwiYWRkUmVxdWVzdCIsImRvd25Mb2FkIiwidGVzdEFsZ29yaXRobSIsImZpbmRUZW1wbGF0ZSIsImRlbGV0ZVRlbmFudCIsImRlbGV0ZVR5cGUiLCJlZGl0UmVxdWVzdCIsInNlZVVzZVN0YXR1cyIsImRlbGV0ZVByb2plY3QiLCJhZGRDZ2RzIiwic3RvcEFwcHMiLCJzZWVEYXNoQm9yZGUiLCJncm91cEVkaXQiLCJzZW5kIiwic2VlQWxnb01vbml0b3IiLCJzZWVSdW4iLCJzZWVVc2VMaXN0IiwicnVuQXBwcyIsImFwcERlbCIsImZpbmRVcGxvYWRlZEFwcHMiLCJkZWxldGVDb25maWciLCJhZGRQcm9qZWN0Q29uZmlnIiwiZmluZFJlcXVlc3RzIiwiYWRkU2VydmljZSIsImdyb3VwTWVtYmVyIiwiZmluZEFsbEFwcHMiLCJmaW5kSW52b2tlTXNnIiwiYmFja1JlcXVlc3RzIiwic2VlLXZpZXciLCJmaW5kVXNlcnMiLCJhZGRfc3BjX2RhdGFzb3VyY2UiLCJtYXBwaW5nX3NwY19kdHNzIiwiYWxnb0RlbCIsInNlZVB1cmNoYXNlSW5mbyIsInNlZU9yZGVyTGlzdCIsImFkZEN1c3RvbWVyQ29udGFjdCIsInNlZURhdGEiLCJlZGl0Q29uZmlnIiwidXNlckFkZCIsImNoYW5nZVBhc3N3b3JkIiwiYWRkVHlwZSIsImZpbmRSb2xlR3JvdXBzIiwiYWRkQXBwIiwiZG93blNyYyIsImZpbmRDdXN0b21lckxpc3QiXSwidGlkIjpudWxsLCJjbGllbnRfaWQiOiI2OTI3NmU5NTdiYTQ0ODMyYjI2ZGUwOWRlYTYxZGYwNyIsInVpZCI6MSwicmVhbE5hbWUiOiLotoXnuqfnrqHnkIblkZgiLCJ0ZW5hbnROYW1lIjpudWxsLCJzY29wZSI6WyJhbGwiXSwidXNlclR5cGUiOjEsImV4cCI6MTU5MDA5MDkyMCwianRpIjoiMzc4YTA5ODItMGQxMS00ZmM5LTk1NWEtMWEyYzVkZmNiNzMxIn0.GQk5JL0fqYuIc3bJCTEhLjtMdWwUlENDDqGbZ0KU9N6a2i07TSOBwktNsGfzY1-mC5Jk-9C7uilhR1MVkkbC-P8Ap6dOfNgDZjUqGoYpsUJ_4r0bd_lfDo4jcbo1FKUie3CSn0sI8X9u8T8namLVvAxnj8CXnk2B8auAec4ThhM728aQybsT12ZN6Bu3W_RbvMGnYB3LBnGWLv6sLGMraJR7Ro9210E5B4vctz3N1mpeQRKI8YUv70SNHLE2HfJ9eMD7jAkj7pL6Rrz7uqgGQCvCAXWQCJMutzrGmi4MQM0ox0qrXrXf3k74rfDlsIPFW6nXURhbcdV_cOLckonPEw","token_type":"bearer","refresh_token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRUYWciOm51bGwsInVzZXJfbmFtZSI6ImFkbWluIiwicm9sZVR5cGUiOm51bGwsImF1dGhvcml0aWVzIjpbImFkZEJ1Y2tDZ2RzIiwic2VlQ2VudGVyIiwicm9sZXNTdGF0dXMiLCJlZGl0X3NwY19kYXRhc291cmNlIiwic2VhcmNoX2F1dGgiLCJkZWxldGVEc0dyb3VwIiwic2VlQXBwcyIsImRsdF9zcGNfZGF0YXNvdXJjZSIsImRlbGV0ZURvY3VtZW50IiwiZWRpdFR5cGUiLCJkZWxldGVDZ2RzR3JvdXAiLCJmaW5kUmVxdWVzdE1vZGVsIiwiZWRpdHNlcnZpY2UiLCJkb1VwbG9hZEFwcHMiLCJ2aWV3X2RldGFpbCIsInJvbGVzRGVsIiwiYWxnb0FkZCIsImVkaXRUZW5hbnQiLCJsaW5rQ2dkcyIsImNyZWF0ZUFwcCIsImZpbmRDZ2RzIiwic2VlUmVuZXdhTGlzdCIsImRvd25sb2FkQ29uZmlnIiwiZ3JvdXBzRGVsIiwiZWRpdERzR3JvdXAiLCJkb3dubG9hZERhdGEiLCJlZGl0Q29udGFjdCIsInVzZXJTdGF0dXMiLCJ2aWV3LXRlbXBsZXRlIiwiYWRkVGVuYW50IiwiYWRkRHNHcm91cCIsImZpbmlzaFJlcXVlc3QiLCJmaW5kQXBwc1J1biIsImFkZF9zcGNfYm9hcmQiLCJlbmFibGVTZXJ2aWNlIiwiZGVwbG95QXBwIiwiYWRkX2NoYXJ0Iiwic2VlIiwiZmluZEFwcHNMaXN0IiwiY2xvc2VSZXF1ZXN0IiwiaW52b2tlTGljZW5jZSIsImRlbGV0ZU9yZGVyIiwiYWRkR3JvdXAiLCJlZGl0UHJvamVjdCIsInNlZVRlbmFudExpc3QiLCJzdGFydFJlcXVlc3RzIiwiZWRpdENnZHMiLCJncm91cFN0YXR1cyIsInJvbGVzRWRpdCIsImZpbmRSb2xlcyIsImFwcEVkaXQiLCJmaW5kV29ya1NwYWNlIiwiYWRkUHJvamVjdCIsInNlZVNlcnZpY2VEZXRhaWwiLCJkZWxldGVDb250YWN0IiwiZGx0X3NwY19ib2FyZCIsInNlbmRSZXF1ZXN0IiwiZmluZFByb2R1Y3RDb25maWciLCJkZWxldGVSZXF1ZXN0IiwicmViYWNrUmVxdWVzdCIsInNlZVNlcnZpY2VMaXN0IiwiZmluZERhdGEiLCJnb1BheSIsImVkaXRDdXN0b21lciIsImF1dGhFZGl0IiwiZGVsZXRlU2VydmljZSIsImVkaXRVc2VyIiwiYWRkQ2dkc0dyb3VwIiwiZGlzYWJsZVNlcnZpY2UiLCJwdXNoUmVxdWVzdHMiLCJhbGdvRWRpdCIsImFkZEN1c3RvbWVyIiwiYWxnb1F1ZXJ5IiwiZWRpdENnZHNHcm91cCIsImZpbmRBbGxQcm9qZWN0cyIsImFkZERvY3VtZW50IiwiYWRkIiwiYWRkUmVxdWVzdCIsImRvd25Mb2FkIiwidGVzdEFsZ29yaXRobSIsImZpbmRUZW1wbGF0ZSIsImRlbGV0ZVRlbmFudCIsImRlbGV0ZVR5cGUiLCJlZGl0UmVxdWVzdCIsInNlZVVzZVN0YXR1cyIsImRlbGV0ZVByb2plY3QiLCJhZGRDZ2RzIiwic3RvcEFwcHMiLCJzZWVEYXNoQm9yZGUiLCJncm91cEVkaXQiLCJzZW5kIiwic2VlQWxnb01vbml0b3IiLCJzZWVSdW4iLCJzZWVVc2VMaXN0IiwicnVuQXBwcyIsImFwcERlbCIsImZpbmRVcGxvYWRlZEFwcHMiLCJkZWxldGVDb25maWciLCJhZGRQcm9qZWN0Q29uZmlnIiwiZmluZFJlcXVlc3RzIiwiYWRkU2VydmljZSIsImdyb3VwTWVtYmVyIiwiZmluZEFsbEFwcHMiLCJmaW5kSW52b2tlTXNnIiwiYmFja1JlcXVlc3RzIiwic2VlLXZpZXciLCJmaW5kVXNlcnMiLCJhZGRfc3BjX2RhdGFzb3VyY2UiLCJtYXBwaW5nX3NwY19kdHNzIiwiYWxnb0RlbCIsInNlZVB1cmNoYXNlSW5mbyIsInNlZU9yZGVyTGlzdCIsImFkZEN1c3RvbWVyQ29udGFjdCIsInNlZURhdGEiLCJlZGl0Q29uZmlnIiwidXNlckFkZCIsImNoYW5nZVBhc3N3b3JkIiwiYWRkVHlwZSIsImZpbmRSb2xlR3JvdXBzIiwiYWRkQXBwIiwiZG93blNyYyIsImZpbmRDdXN0b21lckxpc3QiXSwidGlkIjpudWxsLCJjbGllbnRfaWQiOiI2OTI3NmU5NTdiYTQ0ODMyYjI2ZGUwOWRlYTYxZGYwNyIsInVpZCI6MSwicmVhbE5hbWUiOiLotoXnuqfnrqHnkIblkZgiLCJ0ZW5hbnROYW1lIjpudWxsLCJzY29wZSI6WyJhbGwiXSwiYXRpIjoiMzc4YTA5ODItMGQxMS00ZmM5LTk1NWEtMWEyYzVkZmNiNzMxIiwidXNlclR5cGUiOjEsImV4cCI6MTU5MjYzOTcyMCwianRpIjoiNGQ4OWJmY2ItOWQzNy00Mzk2LTg3NjAtY2I3OTcyZDFiNzQwIn0.atE0ar-C4Ot4VC6iIm5cWBxvUf82TiB3LBHEq6IQmeWz894r9o8IJOdC5fL3C4RlOrpyu1AX7QVhxI_CQfubZPF_RvajSwW26AlFo4BRVMsvjzGCIehA5kTDYg9fB4oPFnnScGGDLzAjzF8lPWQR-vdSVVKfegqC7lQ_Vf1bfSvrQneBVEKtLAMZFi95Z3R96RJdJFXn0-4Z19lL5U127dZhjxNCZWhKGVjeGLfhi2WijBQW1Hvz6lZInKy-vbIb2N0iAagQ3I3NUrlwVTkdFttYDpZ-GPwRLOk3q_U-N774WZ3UKABl6SDk25aiusvp1_-gcAdD1j1NZA1CjBkYhg","expires_in":43198,"scope":"all","tid":null,"tenantName":null,"tenantTag":null,"uid":1,"realName":"超级管理员","userType":1,"roleType":null,"jti":"378a0982-0d11-4fc9-955a-1a2c5dfcb731"}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * access_token : eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRUYWciOm51bGwsInVzZXJfbmFtZSI6ImFkbWluIiwicm9sZVR5cGUiOm51bGwsImF1dGhvcml0aWVzIjpbImFkZEJ1Y2tDZ2RzIiwic2VlQ2VudGVyIiwicm9sZXNTdGF0dXMiLCJlZGl0X3NwY19kYXRhc291cmNlIiwic2VhcmNoX2F1dGgiLCJkZWxldGVEc0dyb3VwIiwic2VlQXBwcyIsImRsdF9zcGNfZGF0YXNvdXJjZSIsImRlbGV0ZURvY3VtZW50IiwiZWRpdFR5cGUiLCJkZWxldGVDZ2RzR3JvdXAiLCJmaW5kUmVxdWVzdE1vZGVsIiwiZWRpdHNlcnZpY2UiLCJkb1VwbG9hZEFwcHMiLCJ2aWV3X2RldGFpbCIsInJvbGVzRGVsIiwiYWxnb0FkZCIsImVkaXRUZW5hbnQiLCJsaW5rQ2dkcyIsImNyZWF0ZUFwcCIsImZpbmRDZ2RzIiwic2VlUmVuZXdhTGlzdCIsImRvd25sb2FkQ29uZmlnIiwiZ3JvdXBzRGVsIiwiZWRpdERzR3JvdXAiLCJkb3dubG9hZERhdGEiLCJlZGl0Q29udGFjdCIsInVzZXJTdGF0dXMiLCJ2aWV3LXRlbXBsZXRlIiwiYWRkVGVuYW50IiwiYWRkRHNHcm91cCIsImZpbmlzaFJlcXVlc3QiLCJmaW5kQXBwc1J1biIsImFkZF9zcGNfYm9hcmQiLCJlbmFibGVTZXJ2aWNlIiwiZGVwbG95QXBwIiwiYWRkX2NoYXJ0Iiwic2VlIiwiZmluZEFwcHNMaXN0IiwiY2xvc2VSZXF1ZXN0IiwiaW52b2tlTGljZW5jZSIsImRlbGV0ZU9yZGVyIiwiYWRkR3JvdXAiLCJlZGl0UHJvamVjdCIsInNlZVRlbmFudExpc3QiLCJzdGFydFJlcXVlc3RzIiwiZWRpdENnZHMiLCJncm91cFN0YXR1cyIsInJvbGVzRWRpdCIsImZpbmRSb2xlcyIsImFwcEVkaXQiLCJmaW5kV29ya1NwYWNlIiwiYWRkUHJvamVjdCIsInNlZVNlcnZpY2VEZXRhaWwiLCJkZWxldGVDb250YWN0IiwiZGx0X3NwY19ib2FyZCIsInNlbmRSZXF1ZXN0IiwiZmluZFByb2R1Y3RDb25maWciLCJkZWxldGVSZXF1ZXN0IiwicmViYWNrUmVxdWVzdCIsInNlZVNlcnZpY2VMaXN0IiwiZmluZERhdGEiLCJnb1BheSIsImVkaXRDdXN0b21lciIsImF1dGhFZGl0IiwiZGVsZXRlU2VydmljZSIsImVkaXRVc2VyIiwiYWRkQ2dkc0dyb3VwIiwiZGlzYWJsZVNlcnZpY2UiLCJwdXNoUmVxdWVzdHMiLCJhbGdvRWRpdCIsImFkZEN1c3RvbWVyIiwiYWxnb1F1ZXJ5IiwiZWRpdENnZHNHcm91cCIsImZpbmRBbGxQcm9qZWN0cyIsImFkZERvY3VtZW50IiwiYWRkIiwiYWRkUmVxdWVzdCIsImRvd25Mb2FkIiwidGVzdEFsZ29yaXRobSIsImZpbmRUZW1wbGF0ZSIsImRlbGV0ZVRlbmFudCIsImRlbGV0ZVR5cGUiLCJlZGl0UmVxdWVzdCIsInNlZVVzZVN0YXR1cyIsImRlbGV0ZVByb2plY3QiLCJhZGRDZ2RzIiwic3RvcEFwcHMiLCJzZWVEYXNoQm9yZGUiLCJncm91cEVkaXQiLCJzZW5kIiwic2VlQWxnb01vbml0b3IiLCJzZWVSdW4iLCJzZWVVc2VMaXN0IiwicnVuQXBwcyIsImFwcERlbCIsImZpbmRVcGxvYWRlZEFwcHMiLCJkZWxldGVDb25maWciLCJhZGRQcm9qZWN0Q29uZmlnIiwiZmluZFJlcXVlc3RzIiwiYWRkU2VydmljZSIsImdyb3VwTWVtYmVyIiwiZmluZEFsbEFwcHMiLCJmaW5kSW52b2tlTXNnIiwiYmFja1JlcXVlc3RzIiwic2VlLXZpZXciLCJmaW5kVXNlcnMiLCJhZGRfc3BjX2RhdGFzb3VyY2UiLCJtYXBwaW5nX3NwY19kdHNzIiwiYWxnb0RlbCIsInNlZVB1cmNoYXNlSW5mbyIsInNlZU9yZGVyTGlzdCIsImFkZEN1c3RvbWVyQ29udGFjdCIsInNlZURhdGEiLCJlZGl0Q29uZmlnIiwidXNlckFkZCIsImNoYW5nZVBhc3N3b3JkIiwiYWRkVHlwZSIsImZpbmRSb2xlR3JvdXBzIiwiYWRkQXBwIiwiZG93blNyYyIsImZpbmRDdXN0b21lckxpc3QiXSwidGlkIjpudWxsLCJjbGllbnRfaWQiOiI2OTI3NmU5NTdiYTQ0ODMyYjI2ZGUwOWRlYTYxZGYwNyIsInVpZCI6MSwicmVhbE5hbWUiOiLotoXnuqfnrqHnkIblkZgiLCJ0ZW5hbnROYW1lIjpudWxsLCJzY29wZSI6WyJhbGwiXSwidXNlclR5cGUiOjEsImV4cCI6MTU5MDA5MDkyMCwianRpIjoiMzc4YTA5ODItMGQxMS00ZmM5LTk1NWEtMWEyYzVkZmNiNzMxIn0.GQk5JL0fqYuIc3bJCTEhLjtMdWwUlENDDqGbZ0KU9N6a2i07TSOBwktNsGfzY1-mC5Jk-9C7uilhR1MVkkbC-P8Ap6dOfNgDZjUqGoYpsUJ_4r0bd_lfDo4jcbo1FKUie3CSn0sI8X9u8T8namLVvAxnj8CXnk2B8auAec4ThhM728aQybsT12ZN6Bu3W_RbvMGnYB3LBnGWLv6sLGMraJR7Ro9210E5B4vctz3N1mpeQRKI8YUv70SNHLE2HfJ9eMD7jAkj7pL6Rrz7uqgGQCvCAXWQCJMutzrGmi4MQM0ox0qrXrXf3k74rfDlsIPFW6nXURhbcdV_cOLckonPEw
         * token_type : bearer
         * refresh_token : eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRUYWciOm51bGwsInVzZXJfbmFtZSI6ImFkbWluIiwicm9sZVR5cGUiOm51bGwsImF1dGhvcml0aWVzIjpbImFkZEJ1Y2tDZ2RzIiwic2VlQ2VudGVyIiwicm9sZXNTdGF0dXMiLCJlZGl0X3NwY19kYXRhc291cmNlIiwic2VhcmNoX2F1dGgiLCJkZWxldGVEc0dyb3VwIiwic2VlQXBwcyIsImRsdF9zcGNfZGF0YXNvdXJjZSIsImRlbGV0ZURvY3VtZW50IiwiZWRpdFR5cGUiLCJkZWxldGVDZ2RzR3JvdXAiLCJmaW5kUmVxdWVzdE1vZGVsIiwiZWRpdHNlcnZpY2UiLCJkb1VwbG9hZEFwcHMiLCJ2aWV3X2RldGFpbCIsInJvbGVzRGVsIiwiYWxnb0FkZCIsImVkaXRUZW5hbnQiLCJsaW5rQ2dkcyIsImNyZWF0ZUFwcCIsImZpbmRDZ2RzIiwic2VlUmVuZXdhTGlzdCIsImRvd25sb2FkQ29uZmlnIiwiZ3JvdXBzRGVsIiwiZWRpdERzR3JvdXAiLCJkb3dubG9hZERhdGEiLCJlZGl0Q29udGFjdCIsInVzZXJTdGF0dXMiLCJ2aWV3LXRlbXBsZXRlIiwiYWRkVGVuYW50IiwiYWRkRHNHcm91cCIsImZpbmlzaFJlcXVlc3QiLCJmaW5kQXBwc1J1biIsImFkZF9zcGNfYm9hcmQiLCJlbmFibGVTZXJ2aWNlIiwiZGVwbG95QXBwIiwiYWRkX2NoYXJ0Iiwic2VlIiwiZmluZEFwcHNMaXN0IiwiY2xvc2VSZXF1ZXN0IiwiaW52b2tlTGljZW5jZSIsImRlbGV0ZU9yZGVyIiwiYWRkR3JvdXAiLCJlZGl0UHJvamVjdCIsInNlZVRlbmFudExpc3QiLCJzdGFydFJlcXVlc3RzIiwiZWRpdENnZHMiLCJncm91cFN0YXR1cyIsInJvbGVzRWRpdCIsImZpbmRSb2xlcyIsImFwcEVkaXQiLCJmaW5kV29ya1NwYWNlIiwiYWRkUHJvamVjdCIsInNlZVNlcnZpY2VEZXRhaWwiLCJkZWxldGVDb250YWN0IiwiZGx0X3NwY19ib2FyZCIsInNlbmRSZXF1ZXN0IiwiZmluZFByb2R1Y3RDb25maWciLCJkZWxldGVSZXF1ZXN0IiwicmViYWNrUmVxdWVzdCIsInNlZVNlcnZpY2VMaXN0IiwiZmluZERhdGEiLCJnb1BheSIsImVkaXRDdXN0b21lciIsImF1dGhFZGl0IiwiZGVsZXRlU2VydmljZSIsImVkaXRVc2VyIiwiYWRkQ2dkc0dyb3VwIiwiZGlzYWJsZVNlcnZpY2UiLCJwdXNoUmVxdWVzdHMiLCJhbGdvRWRpdCIsImFkZEN1c3RvbWVyIiwiYWxnb1F1ZXJ5IiwiZWRpdENnZHNHcm91cCIsImZpbmRBbGxQcm9qZWN0cyIsImFkZERvY3VtZW50IiwiYWRkIiwiYWRkUmVxdWVzdCIsImRvd25Mb2FkIiwidGVzdEFsZ29yaXRobSIsImZpbmRUZW1wbGF0ZSIsImRlbGV0ZVRlbmFudCIsImRlbGV0ZVR5cGUiLCJlZGl0UmVxdWVzdCIsInNlZVVzZVN0YXR1cyIsImRlbGV0ZVByb2plY3QiLCJhZGRDZ2RzIiwic3RvcEFwcHMiLCJzZWVEYXNoQm9yZGUiLCJncm91cEVkaXQiLCJzZW5kIiwic2VlQWxnb01vbml0b3IiLCJzZWVSdW4iLCJzZWVVc2VMaXN0IiwicnVuQXBwcyIsImFwcERlbCIsImZpbmRVcGxvYWRlZEFwcHMiLCJkZWxldGVDb25maWciLCJhZGRQcm9qZWN0Q29uZmlnIiwiZmluZFJlcXVlc3RzIiwiYWRkU2VydmljZSIsImdyb3VwTWVtYmVyIiwiZmluZEFsbEFwcHMiLCJmaW5kSW52b2tlTXNnIiwiYmFja1JlcXVlc3RzIiwic2VlLXZpZXciLCJmaW5kVXNlcnMiLCJhZGRfc3BjX2RhdGFzb3VyY2UiLCJtYXBwaW5nX3NwY19kdHNzIiwiYWxnb0RlbCIsInNlZVB1cmNoYXNlSW5mbyIsInNlZU9yZGVyTGlzdCIsImFkZEN1c3RvbWVyQ29udGFjdCIsInNlZURhdGEiLCJlZGl0Q29uZmlnIiwidXNlckFkZCIsImNoYW5nZVBhc3N3b3JkIiwiYWRkVHlwZSIsImZpbmRSb2xlR3JvdXBzIiwiYWRkQXBwIiwiZG93blNyYyIsImZpbmRDdXN0b21lckxpc3QiXSwidGlkIjpudWxsLCJjbGllbnRfaWQiOiI2OTI3NmU5NTdiYTQ0ODMyYjI2ZGUwOWRlYTYxZGYwNyIsInVpZCI6MSwicmVhbE5hbWUiOiLotoXnuqfnrqHnkIblkZgiLCJ0ZW5hbnROYW1lIjpudWxsLCJzY29wZSI6WyJhbGwiXSwiYXRpIjoiMzc4YTA5ODItMGQxMS00ZmM5LTk1NWEtMWEyYzVkZmNiNzMxIiwidXNlclR5cGUiOjEsImV4cCI6MTU5MjYzOTcyMCwianRpIjoiNGQ4OWJmY2ItOWQzNy00Mzk2LTg3NjAtY2I3OTcyZDFiNzQwIn0.atE0ar-C4Ot4VC6iIm5cWBxvUf82TiB3LBHEq6IQmeWz894r9o8IJOdC5fL3C4RlOrpyu1AX7QVhxI_CQfubZPF_RvajSwW26AlFo4BRVMsvjzGCIehA5kTDYg9fB4oPFnnScGGDLzAjzF8lPWQR-vdSVVKfegqC7lQ_Vf1bfSvrQneBVEKtLAMZFi95Z3R96RJdJFXn0-4Z19lL5U127dZhjxNCZWhKGVjeGLfhi2WijBQW1Hvz6lZInKy-vbIb2N0iAagQ3I3NUrlwVTkdFttYDpZ-GPwRLOk3q_U-N774WZ3UKABl6SDk25aiusvp1_-gcAdD1j1NZA1CjBkYhg
         * expires_in : 43198
         * scope : all
         * tid : null
         * tenantName : null
         * tenantTag : null
         * uid : 1
         * realName : 超级管理员
         * userType : 1
         * roleType : null
         * jti : 378a0982-0d11-4fc9-955a-1a2c5dfcb731
         */

        private String access_token;
        private String token_type;
        private String refresh_token;
        private int expires_in;
        private String scope;
        private Object tid;
        private Object tenantName;
        private Object tenantTag;
        private int uid;
        private String realName;
        private int userType;
        private Object roleType;
        private String jti;

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }

        public int getExpires_in() {
            return expires_in;
        }

        public void setExpires_in(int expires_in) {
            this.expires_in = expires_in;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public Object getTid() {
            return tid;
        }

        public void setTid(Object tid) {
            this.tid = tid;
        }

        public Object getTenantName() {
            return tenantName;
        }

        public void setTenantName(Object tenantName) {
            this.tenantName = tenantName;
        }

        public Object getTenantTag() {
            return tenantTag;
        }

        public void setTenantTag(Object tenantTag) {
            this.tenantTag = tenantTag;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public int getUserType() {
            return userType;
        }

        public void setUserType(int userType) {
            this.userType = userType;
        }

        public Object getRoleType() {
            return roleType;
        }

        public void setRoleType(Object roleType) {
            this.roleType = roleType;
        }

        public String getJti() {
            return jti;
        }

        public void setJti(String jti) {
            this.jti = jti;
        }
    }
}
