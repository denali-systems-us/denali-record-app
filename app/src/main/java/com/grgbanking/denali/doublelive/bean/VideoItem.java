package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;

public class VideoItem implements Serializable {
    private String recordtime = "";// 录制时间 格式y-m-d h:i:s
    private String durationtime = "";// 录制时长 格式i:s
    private String policyname;  //投保人姓名
    private String insuretype;  //保险种类
    private String statu;      //视频状态
    private String url;       //视频的URL图片

    public VideoItem(String recordtime, String durationtime, String policyname, String insuretype, String statu, String url) {
        this.recordtime = recordtime;
        this.durationtime = durationtime;
        this.policyname = policyname;
        this.insuretype = insuretype;
        this.statu = statu;
        this.url = url;
    }

    public String getRecordtime() {
        return recordtime;
    }

    public void setRecordtime(String recordtime) {
        this.recordtime = recordtime;
    }

    public String getDurationtime() {
        return durationtime;
    }

    public void setDurationtime(String durationtime) {
        this.durationtime = durationtime;
    }

    public String getPolicyname() {
        return policyname;
    }

    public void setPolicyname(String policyname) {
        this.policyname = policyname;
    }

    public String getInsuretype() {
        return insuretype;
    }

    public void setInsuretype(String insuretype) {
        this.insuretype = insuretype;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
