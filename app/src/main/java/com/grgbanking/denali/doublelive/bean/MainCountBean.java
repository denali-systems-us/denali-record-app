package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;
import java.util.List;

public class MainCountBean  implements Serializable {

    /**
     * code : 0
     * msg : 成功
     * data : {"totalCount":26,"failCount":0,"monthCounts":[{"month":"6","count":26}]}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * totalCount : 26
         * failCount : 0
         * monthCounts : [{"month":"6","count":26}]
         */

        private int totalCount;
        private int failCount;
        private List<MonthCountsBean> monthCounts;

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getFailCount() {
            return failCount;
        }

        public void setFailCount(int failCount) {
            this.failCount = failCount;
        }

        public List<MonthCountsBean> getMonthCounts() {
            return monthCounts;
        }

        public void setMonthCounts(List<MonthCountsBean> monthCounts) {
            this.monthCounts = monthCounts;
        }

        public static class MonthCountsBean {
            /**
             * month : 6
             * count : 26
             */

            private String month;
            private int count;

            public String getMonth() {
                return month;
            }

            public void setMonth(String month) {
                this.month = month;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }
        }
    }
}
