/*
 *     (C) Copyright 2019, ForgetSky.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.grgbanking.denali.doublelive.lfilepickerlibrary.adapter;

import android.net.Uri;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.utils.FileUtils;
import com.leon.lfilepickerlibrary.R;
import com.grgbanking.denali.doublelive.bean.VideoItem;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;


public class FileAdapter extends BaseQuickAdapter<VideoItem, BaseViewHolder> {





    public FileAdapter(int layoutResId, @Nullable List<VideoItem> list) {
        super(layoutResId,list);

    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, VideoItem videoItem) {
        baseViewHolder.setText(R.id.item_policyname_tv,videoItem.getPolicyname());
        baseViewHolder.setText(R.id.item_insuretype_tv,videoItem.getInsuretype());

        ImageView imgv =(ImageView) baseViewHolder.getView(R.id.item_video_imgv);
        Glide.with( getContext() )
                .load( Uri.fromFile( new File( videoItem.getUrl() ) ) )
                .into( imgv );

        if(FileUtils.getPlayTime( videoItem.getUrl() )!=null ){
            long playTime = Long.parseLong(FileUtils.getPlayTime( videoItem.getUrl()));
            String duration = FileUtils.timeParse(playTime);
            baseViewHolder.setText(R.id.item_duration_tv, duration);
        }else {
            baseViewHolder.setText(R.id.item_duration_tv, "时长未知");
        }

        baseViewHolder.setText(R.id.item_recordtime_tv,videoItem.getRecordtime());
        baseViewHolder.setText(R.id.btn_status, "上传");
    }


//    @Override
//    protected void convert(BaseViewHolder helper, File file) {
//
//        if (file.isFile()) {
//
////            helper.setText(R.id.item_policyname_tv,file.getPolicyName());
////            helper.setText(R.id.item_insuretype_tv,file.getInsureTypeDesc());
//
//            ImageView imgv =(ImageView) helper.getView(R.id.item_video_imgv);
//            Glide.with( getContext() )
//                    .load( Uri.fromFile( new File( file.getPath() ) ) )
//                    .into( imgv );
//
//            if(FileUtils.getPlayTime(file.getPath())!=null ){
//                long playTime = Long.parseLong(FileUtils.getPlayTime(file.getPath()));
//                String duration = FileUtils.timeParse(playTime);
//                helper.setText(R.id.item_duration_tv, duration);
//            }else {
//                helper.setText(R.id.item_duration_tv, "时长未知");
//            }
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            String recordtime=sdf.format(file.lastModified());
//            helper.setText(R.id.item_recordtime_tv,recordtime);
//            helper.setText(R.id.btn_status, "上传");
//
//        }
//
//    }

}
