package com.grgbanking.denali.doublelive.videoabout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.grgbanking.denali.doublelive.scan.CustomCaptureActivity;
import com.grgbanking.denali.doublelive.socketUtil.BaseApplication;
import com.grgbanking.denali.doublelive.utils.GlideImageLoader;
import com.grgbanking.denali.doublelive.utils.HomeKeyWatcher;
import com.king.zxing.Intents;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;
import com.grgbanking.denali.doublelive.R;
import com.grgbanking.denali.doublelive.base.activity.AbstractSimpleActivity;
import com.grgbanking.denali.doublelive.bean.VideoItem2;
import com.grgbanking.denali.doublelive.http.ApiService;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.utils.FileUtils;
import com.grgbanking.denali.doublelive.scan.ScanActivity2;
import com.grgbanking.denali.doublelive.utils.ImageUtils;
import com.rxjava.rxlife.RxLife;
import com.xiao.nicevideoplayer.NiceVideoPlayer;
import com.xiao.nicevideoplayer.NiceVideoPlayerManager;
import com.xiao.nicevideoplayer.TxVideoPlayerController;
import com.yanzhenjie.permission.AndPermission;
//import com.yzq.zxinglibrary.android.CaptureActivity;
//import com.yzq.zxinglibrary.bean.ZxingConfig;
//import com.yzq.zxinglibrary.common.Constant;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.ResourceSubscriber;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.utils.ClickUtil;
import me.lake.librestreaming.utils.ConstantUtil;
import me.lake.librestreaming.utils.ToastUtils;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rxhttp.wrapper.param.RxHttp;

/**
 * 待上传，待审核，未通过视频详情页
 */
public class VideoDetailActivity extends AbstractSimpleActivity implements CustomAdapt {

    /**
     * ----------------标题栏------------------
     **/
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mTitle;
    @BindView(R.id.delete_tv)
    TextView delete_tv;

    /**
     * ----------------main_info------------------
     **/
    @BindView(R.id.duration_tv)
    TextView duration_tv;
    @BindView(R.id.fail_msg_tv)
    TextView fail_msg_tv;

    @BindView(R.id.scan_info_num)
    TextView scan_info_num;
    @BindView(R.id.scan_info_type)
    TextView scan_info_type;
    @BindView(R.id.video_status)
    TextView video_status;

    @BindView(R.id.scan_info_name)
    TextView scan_info_name;
    @BindView(R.id.scan_info_id_num)
    TextView scan_info_id_num;
    @BindView(R.id.record_time_tv)
    TextView record_time_tv;
    @BindView(R.id.upload_time_tv)
    TextView upload_time_tv;
    @BindView(R.id.check_time_tv)
    TextView check_time_tv;
    @BindView(R.id.btn_record_once_more)
    TextView btn_record_once_more;
    @BindView(R.id.nice_video_player)
    NiceVideoPlayer mNiceVideoPlayer;
    @BindView(R.id.fail_lLayout)
    LinearLayout fail_lLayout;
    @BindView(R.id.check_time_lLayout)
    LinearLayout check_time_lLayout;

    private boolean pressedHome;
    private HomeKeyWatcher mHomeKeyWatcher;
    /**
     * 0：待审核
     * 1：通过
     * 2：未通过
     */
    private String status;
    private VideoItem2 videoItem;
    private String videoUrl;
    private String duration;
    private int REQUEST_CODE_SCAN = 1113;
    private static final int STORAGE_REQUEST_CODE = 1021;

    private String loginToken;

    private ApiService mApiService;
    private boolean isContinuousScan;
    private int REQUEST_CODE_SCAN2 = 1123;

    @Override
    protected void initView() {
        setToolbarTitle(R.string.video_detail_title);
        initRetrofit();
        loginToken = SharedPreferencesUtil.getString(VideoDetailActivity.this, SharedPreferencesUtil.LOGINTOKEN, "");

        videoItem = (VideoItem2) getIntent().getSerializableExtra("videoItem");
        status = getIntent().getStringExtra("status");
        if ("2".equals(status)) {
            check_time_lLayout.setVisibility(View.VISIBLE);
            fail_lLayout.setVisibility(View.VISIBLE);
            btn_record_once_more.setVisibility(View.VISIBLE);
            btn_record_once_more.setText("重新录制");
            video_status.setText("未通过");
            video_status.setTextColor(Color.parseColor("#E9552B"));
        } else if ("0".equals(status)) {
            check_time_lLayout.setVisibility(View.GONE);
            video_status.setText("待审核");
            video_status.setTextColor(Color.parseColor("#F5A52F"));
            fail_lLayout.setVisibility(View.GONE);
            btn_record_once_more.setVisibility(View.GONE);
        } else if ("1".equals(status)) {
            check_time_lLayout.setVisibility(View.VISIBLE);
            video_status.setText("已通过");
            video_status.setTextColor(Color.parseColor("#60C163"));
            fail_lLayout.setVisibility(View.GONE);
            btn_record_once_more.setVisibility(View.GONE);
        }
        delete_tv.setVisibility(View.GONE);
        ConstantUtil.getInstance().setUploadToMain(false);

        if (videoItem.getAuditDesc() != null) {
            fail_msg_tv.setText((String) videoItem.getAuditDesc());
        } else {
            fail_msg_tv.setText("审核不通过");
        }
        scan_info_num.setText(videoItem.getPolicyNum());
        scan_info_type.setText(videoItem.getInsureTypeDesc());
        scan_info_name.setText(videoItem.getPolicyName());
        scan_info_id_num.setText(videoItem.getPolicyCard());
        record_time_tv.setText(videoItem.getRecordTime());
        upload_time_tv.setText(videoItem.getCreateTime());
        if (videoItem.getAuditTime() != null) {
            check_time_tv.setText((String) videoItem.getAuditTime());
        }


        mHomeKeyWatcher = new HomeKeyWatcher(this);
        mHomeKeyWatcher.setOnHomePressedListener(new HomeKeyWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                pressedHome = true;
            }
        });
        pressedHome = false;
        mHomeKeyWatcher.startWatch();

        mNiceVideoPlayer.setPlayerType(NiceVideoPlayer.TYPE_IJK); // IjkPlayer or MediaPlayer
        videoUrl = videoItem.getVideoUrl();
//        String videoUrl = "http://tanzi27niu.cdsb.mobi/wps/wp-content/uploads/2017/05/2017-05-17_17-33-30.mp4";
//        String videoUrl = "http://10.252.51.11:31000/api/v1/file/play?fileId=1JRsrsyiJn4ZBZeDuiEj0dJabW6EXlwCEyXeayrZgZao7C24xY7jZYttGYrk0cij1GyMB5RzTuMTepQpsmQmf61A51Oa69oFtMXxvTKUMgBkGIxAehuCsKkikYSwIPlCapTDncNq4akwQxKuNkF";
//        videoUrl = Environment.getExternalStorageDirectory().getPath().concat("/办公室小野.mp4");
        if (videoUrl != null) {
            mNiceVideoPlayer.setUp(videoUrl, null);
            TxVideoPlayerController controller = new TxVideoPlayerController(this);
            controller.setTitle("");
            if (FileUtils.getPlayTime(videoUrl) != null) {
                long playTime = Long.parseLong(FileUtils.getPlayTime(videoUrl));
                controller.setLenght(playTime);
            } else {
                controller.setLenght(0);
            }
            if (videoItem.getImgUrl() != null) {
                controller.imageView().setImageBitmap(ImageUtils.sendImage(videoItem.getImgUrl()));
            } else {

                GlideImageLoader.load(this, videoUrl,
                        controller.imageView());
            }
            mNiceVideoPlayer.setController(controller);

//        String destPath = getExternalCacheDir() + "/" + System.currentTimeMillis() + ".mp4";
//        RxHttp.get(ApiService.BASE_URL_cloud + "api/v1/file/download")
//                .add("fileId ", "1JRsrsyiJn4ZBZeDuiEj0dJabW6EXlwCEyXeayrZgZao7C24xY7jZYttGYrk0cij1GyMB5RzTuMTepQpsmQmf61A51Oa69oFtMXxvTKUMgBkGIxAehuCsKkikYSwIPlCapTDncNq4akwQxKuNkF")//添加参数，非必须
//                .addHeader("Authorization", "Bearer " + loginToken)
//                .asDownload(destPath)
//                .as(RxLife.asOnMain(this))
//                .subscribe(s -> {
//                    //成功
//                    ToastUtils.showToast(VideoDetailActivity.this, "成功");
//                }, throwable -> {
//                    //失败
//
//                    ToastUtils.showToast(VideoDetailActivity.this, "失败");
//                });
//
//        mNiceVideoPlayer.setPlayerType(NiceVideoPlayer.TYPE_IJK); // IjkPlayer or MediaPlayer
//
//        if (destPath != null) {
//            mNiceVideoPlayer.setUp(destPath, null);
//            TxVideoPlayerController controller = new TxVideoPlayerController(this);
//            controller.setTitle("");
//            if (FileUtils.getPlayTime(destPath) != null) {
//                long playTime = Long.parseLong(FileUtils.getPlayTime(destPath));
//                controller.setLenght(playTime);
//            } else {
//                controller.setLenght(0);
//            }
//
//            GlideImageLoader.load(this, destPath,
//                    controller.imageView());
//            mNiceVideoPlayer.setController(controller);

//            mApiService.getPlayer("1JRsrsyiJn4ZBZeDuiEj0dJabW6EXlwCEyXeayrZgZao7C24xY7jZYttGYrk0cij1GyMB5RzTuMTepQpsmQmf61A51Oa69oFtMXxvTKUMgBkGIxAehuCsKkikYSwIPlCapTDncNq4akwQxKuNkF","Bearer " + loginToken)
//                    .subscribeOn(Schedulers.io())//运行在io线程
//                    .observeOn(AndroidSchedulers.mainThread())//回调在主线程
//                    .subscribeWith(new ResourceSubscriber<String>() {
//
//
//                        @Override
//                        public void onNext(String s) {
//                            ToastUtils.showToast( VideoDetailActivity.this, "成功");
//                        }
//
//                        @Override
//                        public void onError(Throwable t) {
//                            t.printStackTrace();
//
//                            ToastUtils.showToast( VideoDetailActivity.this, "失败");
//                        }
//
//                        @Override
//                        public void onComplete() {
//
//                        }
//                    });

        } else {
//            String videoUrl = "http://10.252.206.5:32000/prod/api/v1/file/play?fileId=dc4d2324abae4e77a308dc123f7dfa1f";
//            mNiceVideoPlayer.setUp(videoUrl, null);
//            TxVideoPlayerController controller = new TxVideoPlayerController(this);
//            controller.setTitle("视频资源不存在");
//            controller.setLenght(0);
//            GlideImageLoader.load(this, videoUrl,
//                    controller.imageView());
//            mNiceVideoPlayer.setController(controller);
        }
    }

    private void initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(ApiService.BASE_URL_cloud)//请求url
                .baseUrl(BaseApplication.getInstance().getBASE_URL_cloud())//请求url
                //增加转换器，这一步能直接Json字符串转换为实体对象
                .addConverterFactory(GsonConverterFactory.create())
                //加入 RxJava转换器
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        mApiService = retrofit.create(ApiService.class);
    }

    public void setToolbarTitle(int resId) {
        mTitle.setText(resId);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_detail;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mToolbar.setNavigationOnClickListener(v -> onBackPressedSupport());
    }

    @Override
    protected void initEventAndData() {

    }

    @OnClick({R.id.btn_record_once_more, R.id.delete_tv})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_record_once_more:
                if (ClickUtil.isNotFastClick()) {
                    //处理点击事件
//                    initScan();
                    initScan2();

                }
                break;

            case R.id.delete_tv: //删除
                if (ClickUtil.isNotFastClick()) {

                }
                break;
            default:
                break;
        }
    }

    private void initScan2() {
        isContinuousScan = true;
        if (ContextCompat.checkSelfPermission(VideoDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
        }

        AndPermission.with(this)
                .permission(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                // 准备方法，和 okhttp 的拦截器一样，在请求权限之前先运行改方法，已经拥有权限不会触发该方法
                .rationale((context, permissions, executor) -> {
                    // 此处可以选择显示提示弹窗
                    executor.execute();
                })
                // 用户给权限了
                .onGranted(permissions -> {
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeCustomAnimation(this, R.anim.in, R.anim.out);
//                    Intent intent = new Intent(this, CaptureFragmentActivity.class);
                    Intent intent = new Intent(this, CustomCaptureActivity.class);
                    intent.putExtra("key_title", "");
                    intent.putExtra("key_continuous_scan", isContinuousScan);
                    ActivityCompat.startActivityForResult(VideoDetailActivity.this, intent, REQUEST_CODE_SCAN2, optionsCompat.toBundle());
                })
                // 用户拒绝权限，包括不再显示权限弹窗也在此列
                .onDenied(permissions -> {
                    // 判断用户是不是不再显示权限弹窗了，若不再显示的话进入权限设置页
                    if (AndPermission.hasAlwaysDeniedPermission(VideoDetailActivity.this, permissions)) {
                        // 打开权限设置页
                        AndPermission.permissionSetting(VideoDetailActivity.this).execute();
                        return;
                    }
                })
                .start();
    }

    private void initScan() {

//        if (ContextCompat.checkSelfPermission(VideoDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
//        }
//
//        AndPermission.with(this)
//                .permission(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
//                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                // 准备方法，和 okhttp 的拦截器一样，在请求权限之前先运行改方法，已经拥有权限不会触发该方法
//                .rationale((context, permissions, executor) -> {
//                    // 此处可以选择显示提示弹窗
//                    executor.execute();
//                })
//                // 用户给权限了
//                .onGranted(permissions -> {
//                    Intent intent = new Intent(VideoDetailActivity.this, CaptureActivity.class);
//                    /*ZxingConfig是配置类
//                     *可以设置是否显示底部布局，闪光灯，相册，
//                     * 是否播放提示音  震动
//                     * 设置扫描框颜色等
//                     * 也可以不传这个参数
//                     * */
//                    ZxingConfig config = new ZxingConfig();
//                    // config.setPlayBeep(false);//是否播放扫描声音 默认为true
//                    //  config.setShake(false);//是否震动  默认为true
//                    // config.setDecodeBarCode(false);//是否扫描条形码 默认为true
////                                config.setReactColor(R.color.colorAccent);//设置扫描框四个角的颜色 默认为白色
////                                config.setFrameLineColor(R.color.colorAccent);//设置扫描框边框颜色 默认无色
////                                config.setScanLineColor(R.color.colorAccent);//设置扫描线的颜色 默认白色
//                    config.setFullScreenScan(true);//是否全屏扫描  默认为true  设为false则只会在扫描框中扫描
//                    intent.putExtra(Constant.INTENT_ZXING_CONFIG, config);
//                    startActivityForResult(intent, REQUEST_CODE_SCAN);
//                })
//                // 用户拒绝权限，包括不再显示权限弹窗也在此列
//                .onDenied(permissions -> {
//                    // 判断用户是不是不再显示权限弹窗了，若不再显示的话进入权限设置页
//                    if (AndPermission.hasAlwaysDeniedPermission(VideoDetailActivity.this, permissions)) {
//                        // 打开权限设置页
//                        AndPermission.permissionSetting(VideoDetailActivity.this).execute();
//                        return;
//                    }
//                })
//                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // 扫描二维码/条码回传
//        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
//            if (data != null) {
//
//                String content = data.getStringExtra(Constant.CODED_CONTENT);
//                //扫码成功返回更新布局到保单信息
//                Intent intentScan = new Intent(VideoDetailActivity.this,
//                        ScanActivity2.class);
//                intentScan.putExtra("scanValue", content);
//                startActivity(intentScan);
//                setResult(RESULT_OK, intentScan);
//            }
//        }
        if (requestCode == REQUEST_CODE_SCAN2 && resultCode == RESULT_OK) {
            if (data != null) {
                String content = data.getStringExtra(Intents.Scan.RESULT);
                //扫码成功返回更新布局到保单信息
                Intent intentScan = new Intent(VideoDetailActivity.this,
                        ScanActivity2.class);
                intentScan.putExtra("scanValue", content);
                startActivity(intentScan);
                setResult(RESULT_OK, intentScan);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    @Override
    protected void onRestart() {
        mHomeKeyWatcher.startWatch();
        pressedHome = false;
        super.onRestart();
        NiceVideoPlayerManager.instance().resumeNiceVideoPlayer();
    }

    @Override
    protected void onStop() {
        // 在OnStop中是release还是suspend播放器，需要看是不是因为按了Home键
        if (pressedHome) {
            NiceVideoPlayerManager.instance().suspendNiceVideoPlayer();
        } else {
            NiceVideoPlayerManager.instance().releaseNiceVideoPlayer();
        }
        super.onStop();
        mHomeKeyWatcher.stopWatch();
    }

    @Override
    public void onBackPressedSupport() {
        if (NiceVideoPlayerManager.instance().onBackPressd()) return;
        super.onBackPressedSupport();
    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}
