package com.grgbanking.denali.doublelive.videoabout;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import androidx.annotation.Nullable;

import java.io.File;

/**
 */

// 继承自SQLiteOpenHelper数据库类的子类
public class UploadSQLiteOpenHelper extends SQLiteOpenHelper {

    private static String name = "upload.db";
    private static Integer version = 1;

    private static UploadSQLiteOpenHelper uploadSQLiteOpenHelper;
    public UploadSQLiteOpenHelper(Context context) {
        super(context, name, null, version);
    }

    public UploadSQLiteOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, getMyDatabaseName(name), null, version);
    }

    private static String getMyDatabaseName(String name){
        String databasename = name;
        boolean isSdcardEnable = false;
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){//SDCard是否插入
            isSdcardEnable = true;
        }
        String dbPath = null;
        if(isSdcardEnable){
            dbPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/live/database/";
        }else{//未插入SDCard，建在内存中

        }
        File dbp = new File(dbPath);
        if(!dbp.exists()){
            dbp.mkdirs();
        }
        databasename = dbPath + databasename;
        return databasename;
    }
    //单例模式
//    public static SQLiteDatabase getInstance(Context context) {
//        if (uploadSQLiteOpenHelper == null) {
//            uploadSQLiteOpenHelper = new UploadSQLiteOpenHelper(context);
//        }
//        return uploadSQLiteOpenHelper.getWritableDatabase();
//    }
//
//    //单例模式
//    public static UploadSQLiteOpenHelper getDatabase(Context context) {
//        if (uploadSQLiteOpenHelper == null) {
//            uploadSQLiteOpenHelper = new UploadSQLiteOpenHelper(context);
//        }
//        return uploadSQLiteOpenHelper;
//    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // 打开数据库 & 建立了一个叫uploads的表，里面只有一列name来存储历史记录：
        db.execSQL("create table uploads(id integer primary key autoincrement, videoName varchar(30)," +
                "scanValue varchar(800) , result varchar(3000) ,recordTime varchar(30),videoUrl varchar(120),loginName varchar(30) )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
