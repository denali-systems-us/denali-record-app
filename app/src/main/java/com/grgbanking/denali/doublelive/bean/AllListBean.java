package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;
import java.util.List;

public class AllListBean implements Serializable {


    /**
     * code : 0
     * msg : 查询成功
     * data : {"rows":[{"id":10,"policyCard":"445464564656842888","organizationCode":"597456","policyNum":"883456789","staffNum":"131313","createTime":"2020-07-14T13:36:00.000+0800","recordTime":"2020-07-14T21:35:32.000+0800","tenantTag":null,"videoId":"f61a3be625ac4b45b4525a05742d6211","staffName":"吴昌宇","staffCard":"4455465612553156688","policyName":"王小皮","insureType":"2","result":"[{\"msg\":\"征求录像意见\",\"node_id\":\"1\",\"node_start_time\":\"2020-07-01 11:42:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1},{\"name\":\"同框检测\",\"value\":0}],\"time\":30000},{\"msg\":\"代理人员出示证件\",\"node_id\":\"2\",\"node_start_time\":\"2020-07-02 11:42:53\",\"state\":[{\"name\":\"出示有效证件\",\"value\":1}],\"time\":23000},{\"msg\":\"代理人介绍\",\"node_id\":\"3\",\"node_start_time\":\"2020-07-02 11:43:13\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"书面文件为准\",\"node_id\":\"4\",\"node_start_time\":\"2020-07-02 11:43:40\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":21000},{\"msg\":\"保障方案说明\",\"node_id\":\"5\",\"node_start_time\":\"2020-07-02 11:44:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"条款介绍\",\"node_id\":\"6\",\"node_start_time\":\"2020-07-02 11:44:20\",\"state\":[{\"name\":\"条款提示书\",\"value\":1}],\"time\":20000},{\"msg\":\"责任确认\",\"node_id\":\"7\",\"node_start_time\":\"2020-07-02 11:44:40\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"疾病观察期\",\"node_id\":\"8\",\"node_start_time\":\"2020-07-02 11:45:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"指定医疗机构\",\"node_id\":\"9\",\"node_start_time\":\"2020-07-02 11:45:20\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"犹豫期\",\"node_id\":\"10\",\"node_start_time\":\"2020-07-02 11:45:40\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"宽限期\",\"node_id\":\"11\",\"node_start_time\":\"2020-07-02 11:46:00\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"如实告知\",\"node_id\":\"12\",\"node_start_time\":\"2020-07-02 11:46:20\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"投保人证件展示\",\"node_id\":\"13\",\"node_start_time\":\"2020-07-02 11:46:40\",\"state\":[{\"name\":\"投保人人出示有效证件\",\"value\":1}],\"time\":20000},{\"msg\":\"投保人确认\",\"node_id\":\"14\",\"node_start_time\":\"2020-07-02 11:47:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"支付提醒\",\"node_id\":\"15\",\"node_start_time\":\"2020-07-02 11:47:20\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"投保提示书签字\",\"node_id\":\"16\",\"node_start_time\":\"2020-07-02 11:47:30\",\"state\":[{\"name\":\"投保提示书\",\"value\":1},{\"name\":\"签字区域\",\"value\":1}],\"time\":10000},{\"msg\":\"免责条款签字\",\"node_id\":\"17\",\"node_start_time\":\"2020-07-02 11:47:40\",\"state\":[{\"name\":\"免责条款\",\"value\":1},{\"name\":\"签字区域\",\"value\":0}],\"time\":10000},{\"msg\":\"个人人生保险投保单签字\",\"node_id\":\"18\",\"node_start_time\":\"2020-07-02 11:47:50\",\"state\":[{\"name\":\"个人人生保险投保单\",\"value\":0},{\"name\":\"签字区域\",\"value\":0}],\"time\":10000}]","creator":"admin","auditor":null,"auditDesc":null,"auditTime":null,"auditStatus":0,"dataAuthIdList":null,"insureTypeDesc":"人身险","duration":"00:16","videoUrl":"/storage/emulated/0/live/admin/2020-06-09-09-27-19.mp4"}],"total":10}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * rows : [{"id":10,"policyCard":"445464564656842888","organizationCode":"597456","policyNum":"883456789","staffNum":"131313","createTime":"2020-07-14T13:36:00.000+0800","recordTime":"2020-07-14T21:35:32.000+0800","tenantTag":null,"videoId":"f61a3be625ac4b45b4525a05742d6211","staffName":"吴昌宇","staffCard":"4455465612553156688","policyName":"王小皮","insureType":"2","result":"[{\"msg\":\"征求录像意见\",\"node_id\":\"1\",\"node_start_time\":\"2020-07-01 11:42:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1},{\"name\":\"同框检测\",\"value\":0}],\"time\":30000},{\"msg\":\"代理人员出示证件\",\"node_id\":\"2\",\"node_start_time\":\"2020-07-02 11:42:53\",\"state\":[{\"name\":\"出示有效证件\",\"value\":1}],\"time\":23000},{\"msg\":\"代理人介绍\",\"node_id\":\"3\",\"node_start_time\":\"2020-07-02 11:43:13\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"书面文件为准\",\"node_id\":\"4\",\"node_start_time\":\"2020-07-02 11:43:40\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":21000},{\"msg\":\"保障方案说明\",\"node_id\":\"5\",\"node_start_time\":\"2020-07-02 11:44:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"条款介绍\",\"node_id\":\"6\",\"node_start_time\":\"2020-07-02 11:44:20\",\"state\":[{\"name\":\"条款提示书\",\"value\":1}],\"time\":20000},{\"msg\":\"责任确认\",\"node_id\":\"7\",\"node_start_time\":\"2020-07-02 11:44:40\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"疾病观察期\",\"node_id\":\"8\",\"node_start_time\":\"2020-07-02 11:45:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"指定医疗机构\",\"node_id\":\"9\",\"node_start_time\":\"2020-07-02 11:45:20\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"犹豫期\",\"node_id\":\"10\",\"node_start_time\":\"2020-07-02 11:45:40\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"宽限期\",\"node_id\":\"11\",\"node_start_time\":\"2020-07-02 11:46:00\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"如实告知\",\"node_id\":\"12\",\"node_start_time\":\"2020-07-02 11:46:20\",\"state\":[{\"name\":\"明确答复\",\"value\":0}],\"time\":20000},{\"msg\":\"投保人证件展示\",\"node_id\":\"13\",\"node_start_time\":\"2020-07-02 11:46:40\",\"state\":[{\"name\":\"投保人人出示有效证件\",\"value\":1}],\"time\":20000},{\"msg\":\"投保人确认\",\"node_id\":\"14\",\"node_start_time\":\"2020-07-02 11:47:00\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"支付提醒\",\"node_id\":\"15\",\"node_start_time\":\"2020-07-02 11:47:20\",\"state\":[{\"name\":\"明确答复\",\"value\":1}],\"time\":20000},{\"msg\":\"投保提示书签字\",\"node_id\":\"16\",\"node_start_time\":\"2020-07-02 11:47:30\",\"state\":[{\"name\":\"投保提示书\",\"value\":1},{\"name\":\"签字区域\",\"value\":1}],\"time\":10000},{\"msg\":\"免责条款签字\",\"node_id\":\"17\",\"node_start_time\":\"2020-07-02 11:47:40\",\"state\":[{\"name\":\"免责条款\",\"value\":1},{\"name\":\"签字区域\",\"value\":0}],\"time\":10000},{\"msg\":\"个人人生保险投保单签字\",\"node_id\":\"18\",\"node_start_time\":\"2020-07-02 11:47:50\",\"state\":[{\"name\":\"个人人生保险投保单\",\"value\":0},{\"name\":\"签字区域\",\"value\":0}],\"time\":10000}]","creator":"admin","auditor":null,"auditDesc":null,"auditTime":null,"auditStatus":0,"dataAuthIdList":null,"insureTypeDesc":"人身险","duration":"00:16","videoUrl":"/storage/emulated/0/live/admin/2020-06-09-09-27-19.mp4"}]
         * total : 10
         */

        private int total;
        private List<RowsBean> rows;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<RowsBean> getRows() {
            return rows;
        }

        public void setRows(List<RowsBean> rows) {
            this.rows = rows;
        }

        public static class RowsBean {
            /**
             * id : 10
             * policyCard : 445464564656842888
             * organizationCode : 597456
             * policyNum : 883456789
             * staffNum : 131313
             * createTime : 2020-07-14T13:36:00.000+0800
             * recordTime : 2020-07-14T21:35:32.000+0800
             * tenantTag : null
             * videoId : f61a3be625ac4b45b4525a05742d6211
             * staffName : 吴昌宇
             * staffCard : 4455465612553156688
             * policyName : 王小皮
             * insureType : 2
             * result : [{"msg":"征求录像意见","node_id":"1","node_start_time":"2020-07-01 11:42:00","state":[{"name":"明确答复","value":1},{"name":"同框检测","value":0}],"time":30000},{"msg":"代理人员出示证件","node_id":"2","node_start_time":"2020-07-02 11:42:53","state":[{"name":"出示有效证件","value":1}],"time":23000},{"msg":"代理人介绍","node_id":"3","node_start_time":"2020-07-02 11:43:13","state":[{"name":"明确答复","value":0}],"time":20000},{"msg":"书面文件为准","node_id":"4","node_start_time":"2020-07-02 11:43:40","state":[{"name":"明确答复","value":1}],"time":21000},{"msg":"保障方案说明","node_id":"5","node_start_time":"2020-07-02 11:44:00","state":[{"name":"明确答复","value":1}],"time":20000},{"msg":"条款介绍","node_id":"6","node_start_time":"2020-07-02 11:44:20","state":[{"name":"条款提示书","value":1}],"time":20000},{"msg":"责任确认","node_id":"7","node_start_time":"2020-07-02 11:44:40","state":[{"name":"明确答复","value":0}],"time":20000},{"msg":"疾病观察期","node_id":"8","node_start_time":"2020-07-02 11:45:00","state":[{"name":"明确答复","value":1}],"time":20000},{"msg":"指定医疗机构","node_id":"9","node_start_time":"2020-07-02 11:45:20","state":[{"name":"明确答复","value":1}],"time":20000},{"msg":"犹豫期","node_id":"10","node_start_time":"2020-07-02 11:45:40","state":[{"name":"明确答复","value":1}],"time":20000},{"msg":"宽限期","node_id":"11","node_start_time":"2020-07-02 11:46:00","state":[{"name":"明确答复","value":0}],"time":20000},{"msg":"如实告知","node_id":"12","node_start_time":"2020-07-02 11:46:20","state":[{"name":"明确答复","value":0}],"time":20000},{"msg":"投保人证件展示","node_id":"13","node_start_time":"2020-07-02 11:46:40","state":[{"name":"投保人人出示有效证件","value":1}],"time":20000},{"msg":"投保人确认","node_id":"14","node_start_time":"2020-07-02 11:47:00","state":[{"name":"明确答复","value":1}],"time":20000},{"msg":"支付提醒","node_id":"15","node_start_time":"2020-07-02 11:47:20","state":[{"name":"明确答复","value":1}],"time":20000},{"msg":"投保提示书签字","node_id":"16","node_start_time":"2020-07-02 11:47:30","state":[{"name":"投保提示书","value":1},{"name":"签字区域","value":1}],"time":10000},{"msg":"免责条款签字","node_id":"17","node_start_time":"2020-07-02 11:47:40","state":[{"name":"免责条款","value":1},{"name":"签字区域","value":0}],"time":10000},{"msg":"个人人生保险投保单签字","node_id":"18","node_start_time":"2020-07-02 11:47:50","state":[{"name":"个人人生保险投保单","value":0},{"name":"签字区域","value":0}],"time":10000}]
             * creator : admin
             * auditor : null
             * auditDesc : null
             * auditTime : null
             * auditStatus : 0
             * dataAuthIdList : null
             * insureTypeDesc : 人身险
             * duration : 00:16
             * videoUrl : /storage/emulated/0/live/admin/2020-06-09-09-27-19.mp4
             */

            private int id;
            private String policyCard;
            private String organizationCode;
            private String policyNum;
            private String staffNum;
            private String createTime;
            private String recordTime;
            private Object tenantTag;
            private String videoId;
            private String staffName;
            private String staffCard;
            private String policyName;
            private String insureType;
            private String result;
            private String creator;
            private Object auditor;
            private Object auditDesc;
            private Object auditTime;
            private int auditStatus;
            private Object dataAuthIdList;
            private String insureTypeDesc;
            private String duration;
            private String videoUrl;
            private String imgUrl;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPolicyCard() {
                return policyCard;
            }

            public void setPolicyCard(String policyCard) {
                this.policyCard = policyCard;
            }

            public String getOrganizationCode() {
                return organizationCode;
            }

            public void setOrganizationCode(String organizationCode) {
                this.organizationCode = organizationCode;
            }

            public String getPolicyNum() {
                return policyNum;
            }

            public void setPolicyNum(String policyNum) {
                this.policyNum = policyNum;
            }

            public String getStaffNum() {
                return staffNum;
            }

            public void setStaffNum(String staffNum) {
                this.staffNum = staffNum;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getRecordTime() {
                return recordTime;
            }

            public void setRecordTime(String recordTime) {
                this.recordTime = recordTime;
            }

            public Object getTenantTag() {
                return tenantTag;
            }

            public void setTenantTag(Object tenantTag) {
                this.tenantTag = tenantTag;
            }

            public String getVideoId() {
                return videoId;
            }

            public void setVideoId(String videoId) {
                this.videoId = videoId;
            }

            public String getStaffName() {
                return staffName;
            }

            public void setStaffName(String staffName) {
                this.staffName = staffName;
            }

            public String getStaffCard() {
                return staffCard;
            }

            public void setStaffCard(String staffCard) {
                this.staffCard = staffCard;
            }

            public String getPolicyName() {
                return policyName;
            }

            public void setPolicyName(String policyName) {
                this.policyName = policyName;
            }

            public String getInsureType() {
                return insureType;
            }

            public void setInsureType(String insureType) {
                this.insureType = insureType;
            }

            public String getResult() {
                return result;
            }

            public void setResult(String result) {
                this.result = result;
            }

            public String getCreator() {
                return creator;
            }

            public void setCreator(String creator) {
                this.creator = creator;
            }

            public Object getAuditor() {
                return auditor;
            }

            public void setAuditor(Object auditor) {
                this.auditor = auditor;
            }

            public Object getAuditDesc() {
                return auditDesc;
            }

            public void setAuditDesc(Object auditDesc) {
                this.auditDesc = auditDesc;
            }

            public Object getAuditTime() {
                return auditTime;
            }

            public void setAuditTime(Object auditTime) {
                this.auditTime = auditTime;
            }

            public int getAuditStatus() {
                return auditStatus;
            }

            public void setAuditStatus(int auditStatus) {
                this.auditStatus = auditStatus;
            }

            public Object getDataAuthIdList() {
                return dataAuthIdList;
            }

            public void setDataAuthIdList(Object dataAuthIdList) {
                this.dataAuthIdList = dataAuthIdList;
            }

            public String getInsureTypeDesc() {
                return insureTypeDesc;
            }

            public void setInsureTypeDesc(String insureTypeDesc) {
                this.insureTypeDesc = insureTypeDesc;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            public String getVideoUrl() {
                return videoUrl;
            }

            public void setVideoUrl(String videoUrl) {
                this.videoUrl = videoUrl;
            }

            public String getImgUrl() {
                return imgUrl;
            }

            public void setImgUrl(String imgUrl) {
                this.imgUrl = imgUrl;
            }
        }
    }
}
