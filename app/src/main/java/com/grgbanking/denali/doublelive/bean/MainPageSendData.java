package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;

public class MainPageSendData implements Serializable {
    /**
     * endTime : 2020-06-23T07:47:16.713Z
     * keyword : 张三
     * order : string
     * page : 1
     * rows : 20
     * sort : string
     * startTime : 2020-01-23T07:47:16.713Z
     * status : 0
     */

    private String endTime;
    private String keyword;
    private String order;
    private int page;
    private int rows;
    private String sort;
    private String startTime;
    private int status;

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
