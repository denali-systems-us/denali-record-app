package com.grgbanking.denali.doublelive.bean;

public class UploadSendBean {

    public UploadSendBean( String node_id, int state, String msg, int time, String node_start_time) {
        this.state = state;
        this.node_id = node_id;
        this.msg = msg;
        this.time = time;
        this.node_start_time = node_start_time;
    }

    /**
     * state : 1
     * node_id : 1
     * msg : 征求录像意见
     * time : 30000
     * node_start_time  : 2020-07-02 11:42:00
     */

    private int state;
    private String node_id;
    private String msg;
    private int time;
    private String node_start_time;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getNode_start_time() {
        return node_start_time;
    }

    public void setNode_start_time(String node_start_time) {
        this.node_start_time = node_start_time;
    }

//    public UploadSendBean(String node_id, String msg, int time, String node_start_time, List<StateBean> state) {
//        this.node_id = node_id;
//        this.msg = msg;
//        this.time = time;
//        this.node_start_time = node_start_time;
//        this.state = state;
//    }
//
//    /**
//     * state : [{"name":"明确答复","value":1},{"name":"同框检测","value":0}]
//     * node_id : 1
//     * msg : 征求录像意见
//     * time : 30000
//     * node_start_time  : 2020-07-02 11:42:00
//     */
//
//    private String node_id;
//    private String msg;
//    private int time;
//    private String node_start_time;
//    private List<StateBean> state;
//
//    public String getNode_id() {
//        return node_id;
//    }
//
//    public void setNode_id(String node_id) {
//        this.node_id = node_id;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public int getTime() {
//        return time;
//    }
//
//    public void setTime(int time) {
//        this.time = time;
//    }
//
//    public String getNode_start_time() {
//        return node_start_time;
//    }
//
//    public void setNode_start_time(String node_start_time) {
//        this.node_start_time = node_start_time;
//    }
//
//    public List<StateBean> getState() {
//        return state;
//    }
//
//    public void setState(List<StateBean> state) {
//        this.state = state;
//    }
//
//    public static class StateBean {
//        /**
//         * name : 明确答复
//         * value : 1
//         */
//
//        private String name;
//        private int value;
//
//        public StateBean(String name, int value) {
//            this.name = name;
//            this.value = value;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public int getValue() {
//            return value;
//        }
//
//        public void setValue(int value) {
//            this.value = value;
//        }
//    }

}
