package com.grgbanking.denali.doublelive.bean;

import java.io.Serializable;
import java.util.List;

public class UploadSendData implements Serializable {


    /**
     * uploaddata : [{"state":[{"name":"明确答复","value":1},{"name":"同框检测","value":0}],"node_id":"1","msg":"征求录像意见","time":30000,"node_start_time ":"2020-07-02 11:42:00"},{"state":[{"name":"出示有效证件","value":1}],"node_id":"2","msg":"代理人员出示证件","time":23000,"node_start_time ":"2020-07-02 11:42:53"},{"state":[{"name":"明确答复","value":0}],"node_id":"3","msg":"代理人介绍","time":20000,"node_start_time ":"2020-07-02 11:43:13"},{"state":[{"name":"明确答复","value":0}],"node_id":"4","msg":"书面文件为准","time":27000,"node_start_time ":"2020-07-02 11:43:40"},{"state":[{"name":"明确答复","value":1}],"node_id":"5","msg":"保障方案说明","time":20000,"node_start_time ":"2020-07-02 11:44:00"},{"state":[{"name":"条款提示书","value":1}],"node_id":"6","msg":"条款介绍","time":20000,"node_start_time ":"2020-07-02 11:44:20"},{"state":[{"name":"明确答复","value":1}],"node_id":"7","msg":"责任确认","time":20000,"node_start_time ":"2020-07-02 11:44:40"},{"state":[{"name":"明确答复","value":1}],"node_id":"8","msg":"疾病观察期","time":20000,"node_start_time ":"2020-07-02 11:45:00"},{"state":[{"name":"明确答复","value":1}],"node_id":"9","msg":"指定医疗机构","time":20000,"node_start_time ":"2020-07-02 11:45:20"},{"state":[{"name":"明确答复","value":1}],"node_id":"10","msg":"犹豫期","time":20000,"node_start_time ":"2020-07-02 11:45:40"},{"state":[{"name":"明确答复","value":1}],"node_id":"11","msg":"宽限期","time":20000,"node_start_time ":"2020-07-02 11:46:00"},{"state":[{"name":"明确答复","value":0}],"node_id":"12","msg":"如实告知","time":20000,"node_start_time ":"2020-07-02 11:46:20"},{"state":[{"name":"投保人人出示有效证件","value":1}],"node_id":"13","msg":"投保人证件展示","time":20000,"node_start_time ":"2020-07-02 11:46:40"},{"state":[{"name":"明确答复","value":0}],"node_id":"14","msg":"投保人确认","time":20000,"node_start_time ":"2020-07-02 11:47:00"},{"state":[{"name":"明确答复","value":0}],"node_id":"15","msg":"支付提醒","time":20000,"node_start_time ":"2020-07-02 11:47:20"},{"state":[{"name":"投保提示书","value":1},{"name":"签字区域","value":1}],"node_id":"16","msg":"投保提示书签字","time":10000,"node_start_time ":"2020-07-02 11:47:30"},{"state":[{"name":"免责条款","value":1},{"name":"签字区域","value":0}],"node_id":"17","msg":"免责条款签字","time":10000,"node_start_time ":"2020-07-02 11:47:40"},{"state":[{"name":"个人人生保险投保单","value":0},{"name":"签字区域","value":0}],"node_id":"18","msg":"个人人生保险投保单签字","time":10000,"node_start_time ":"2020-07-02 11:47:50"}]
     * recordTime : 2020-07-02  9:47:33
     */

    private String recordTime;
    private List<UploaddataBean> uploaddata;

    public String getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(String recordTime) {
        this.recordTime = recordTime;
    }

    public List<UploaddataBean> getUploaddata() {
        return uploaddata;
    }

    public void setUploaddata(List<UploaddataBean> uploaddata) {
        this.uploaddata = uploaddata;
    }

    public static class UploaddataBean {
        public UploaddataBean(String node_id, String msg, int time, String node_start_time, List<StateBean> state) {
            this.node_id = node_id;
            this.msg = msg;
            this.time = time;
            this.node_start_time = node_start_time;
            this.state = state;
        }

        /**
         * state : [{"name":"明确答复","value":1},{"name":"同框检测","value":0}]
         * node_id : 1
         * msg : 征求录像意见
         * time : 30000
         * node_start_time  : 2020-07-02 11:42:00
         */

        private String node_id;
        private String msg;
        private int time;
        private String node_start_time;
        private List<StateBean> state;

        public String getNode_id() {
            return node_id;
        }

        public void setNode_id(String node_id) {
            this.node_id = node_id;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }

        public String getNode_start_time() {
            return node_start_time;
        }

        public void setNode_start_time(String node_start_time) {
            this.node_start_time = node_start_time;
        }

        public List<StateBean> getState() {
            return state;
        }

        public void setState(List<StateBean> state) {
            this.state = state;
        }

        public static class StateBean {
            /**
             * name : 明确答复
             * value : 1
             */

            private String name;
            private int value;

            public StateBean(String name, int value) {
                this.name = name;
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }
        }
    }
}
