//package com.grgbanking.denali.doublelive.videoabout;
//
//import android.content.Context;
//import android.graphics.Color;
//import android.util.AttributeSet;
//import android.view.MotionEvent;
//import android.view.VelocityTracker;
//import android.view.View;
//import android.view.ViewConfiguration;
//import android.view.ViewGroup;
//import android.webkit.WebView;
//import android.widget.FrameLayout;
//import android.widget.GridView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.OverScroller;
//import android.widget.ScrollView;
//import android.widget.TextView;
//
//import androidx.appcompat.widget.Toolbar;
//
//import com.grgbanking.denali.doublelive.R;
//
//
//public class UserHomePageLayout extends LinearLayout {
//
//    private View mTop;
//    private View mNav;
//
//    private int mTopViewHeight,changeHeight;
//    private ViewGroup mInnerScrollView;
//    private boolean isTopHidden = false;
//    private FrameLayout frameLayout;
//    private Toolbar toolbar;
//    private TextView titlebar_title;
//    private OverScroller mScroller;
//    private VelocityTracker mVelocityTracker;
//    private int mTouchSlop;
//    private int mMaximumVelocity, mMinimumVelocity;
//
//    private float mLastY;
//    private boolean mDragging;
//    private Context mContext;
//    private boolean isInControl = false;
//
//    public UserHomePageLayout(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        setOrientation(LinearLayout.VERTICAL);
//        mContext = context;
//        mScroller = new OverScroller(context);
//        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
//        mMaximumVelocity = ViewConfiguration.get(context)
//                .getScaledMaximumFlingVelocity();
//        mMinimumVelocity = ViewConfiguration.get(context)
//                .getScaledMinimumFlingVelocity();
//
//    }
//    //获取状态栏高度
//    public int getStatusBarHeight() {
//        int result = 0;
//        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
//        if (resourceId > 0) {
//            result = getResources().getDimensionPixelSize(resourceId);
//        }
//        return result;
//    }
//    @Override
//    protected void onFinishInflate() {
//        super.onFinishInflate();
//        mTop = findViewById(R.id.homepage_bg);
//        mNav = findViewById(R.id.nav_layout);
//        titlebar_title = (TextView) findViewById(R.id.titlebar_title);
//        frameLayout = (FrameLayout) findViewById(R.id.message_board);
//        mInnerScrollView = (ViewGroup) frameLayout.getRootView().findViewById(R.id.id_stickynavlayout_innerscrollview);
//
//    }
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        toolbar = (Toolbar) ((ViewGroup)this.getParent()).getChildAt(1);
//        //重新设置toolbar的高度，toolbar高度 + statusbar高度
//        ViewGroup.LayoutParams params1 = toolbar.getLayoutParams();
//        params1.height = LiushenUtil.dip2px(mContext,48) + getStatusBarHeight();
//        changeHeight = mTop.getMeasuredHeight() -toolbar.getMeasuredHeight();
//        mTopViewHeight = mTop.getMeasuredHeight() + mNav.getMeasuredHeight() - toolbar.getMeasuredHeight();
//        //frameLayout高度设置
//        ViewGroup.LayoutParams params = frameLayout.getLayoutParams();
//        params.height = getMeasuredHeight() -params1.height;
//    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//    }
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        int action = ev.getAction();
//        float y = ev.getY();
//
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                mLastY = y;
//
//                break;
//            case MotionEvent.ACTION_MOVE:
//                float dy = y - mLastY;
//                getCurrentScrollView();
//
//                if (mInnerScrollView instanceof ScrollView) {
//                    if (mInnerScrollView.getScrollY() == 0 && isTopHidden && dy > 0
//                            && !isInControl) {
//                        isInControl = true;
//                        ev.setAction(MotionEvent.ACTION_CANCEL);
//                        MotionEvent ev2 = MotionEvent.obtain(ev);
//                        dispatchTouchEvent(ev);
//                        ev2.setAction(MotionEvent.ACTION_DOWN);
//                        return dispatchTouchEvent(ev2);
//                    }
//                } else if (mInnerScrollView instanceof RefreshGridView) {
//
//                    RefreshGridView lv1 = (RefreshGridView) mInnerScrollView;
//                    GridView lv = lv1.getContentView();
//                    View c = lv.getChildAt(lv.getFirstVisiblePosition());
//
//                    if (!isInControl && c != null && c.getTop() == 0 && isTopHidden
//                            && dy > 0) {
//                        isInControl = true;
//                        ev.setAction(MotionEvent.ACTION_CANCEL);
//                        MotionEvent ev2 = MotionEvent.obtain(ev);
//                        dispatchTouchEvent(ev);
//                        ev2.setAction(MotionEvent.ACTION_DOWN);
//                        return dispatchTouchEvent(ev2);
//                    }
//                }  else if (mInnerScrollView instanceof ListView) {
//
//                    ListView lv = (ListView) mInnerScrollView;
//                    View c = lv.getChildAt(lv.getFirstVisiblePosition());
//
//                    if (!isInControl && c != null && c.getTop() == 0 && isTopHidden
//                            && dy > 0) {
//                        isInControl = true;
//                        ev.setAction(MotionEvent.ACTION_CANCEL);
//                        MotionEvent ev2 = MotionEvent.obtain(ev);
//                        dispatchTouchEvent(ev);
//                        ev2.setAction(MotionEvent.ACTION_DOWN);
//                        return dispatchTouchEvent(ev2);
//                    }
//                }
//                else if (mInnerScrollView instanceof WebView) {
//                    if (mInnerScrollView.getScrollY() == 0 && isTopHidden && dy > 0
//                            && !isInControl) {
//                        isInControl = true;
//                        ev.setAction(MotionEvent.ACTION_CANCEL);
//                        MotionEvent ev2 = MotionEvent.obtain(ev);
//                        dispatchTouchEvent(ev);
//                        ev2.setAction(MotionEvent.ACTION_DOWN);
//                        return dispatchTouchEvent(ev2);
//                    }
//                }
//                break;
//        }
//        return super.dispatchTouchEvent(ev);
//
//    }
//
//    /**
//     *
//     */
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        final int action = ev.getAction();
//        float y = ev.getY();
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                mLastY = y;
//
//                break;
//            case MotionEvent.ACTION_MOVE:
//                float dy = y - mLastY;
//                getCurrentScrollView();
//                if (Math.abs(dy) > mTouchSlop) {
//                    mDragging = true;
//                    if (mInnerScrollView instanceof ScrollView) {
//
//                        if (!isTopHidden
//                                || (mInnerScrollView.getScrollY() == 0
//                                && isTopHidden && dy > 0)) {
//
//                            initVelocityTrackerIfNotExists();
//                            mVelocityTracker.addMovement(ev);
//                            mLastY = y;
//                            return true;
//                        }
//                    } else if (mInnerScrollView instanceof GridView) {
//
//                        // ListView lv = (ListView) mInnerScrollView;
//                        RefreshGridView lv1 = (RefreshGridView) mInnerScrollView;
//                        GridView lv = lv1.getContentView();
//                        View c = lv.getChildAt(lv.getFirstVisiblePosition());
//
//                        if (!isTopHidden || //
//                                (c != null //
//                                        && c.getTop() == 0//
//                                        && isTopHidden && dy > 0)) {
//
//                            initVelocityTrackerIfNotExists();
//                            mVelocityTracker.addMovement(ev);
//                            mLastY = y;
//                            return true;
//                        }
//                    }  else if (mInnerScrollView instanceof ListView) {
//
//                        ListView lv = (ListView) mInnerScrollView;
//                        View c = lv.getChildAt(lv.getFirstVisiblePosition());
//                        // 濡傛灉topView娌℃湁闅愯棌
//                        // 鎴杝c鐨刲istView鍦ㄩ《閮�&& topView闅愯棌 && 涓嬫媺锛屽垯鎷︽埅
//
//                        if (!isTopHidden || //
//                                (c != null //
//                                        && c.getTop() == 0//
//                                        && isTopHidden && dy > 0)) {
//
//                            initVelocityTrackerIfNotExists();
//                            mVelocityTracker.addMovement(ev);
//                            mLastY = y;
//                            return true;
//                        }
//                    }
//                    else if (mInnerScrollView instanceof WebView) {
//                        if (!isTopHidden
//                                || (mInnerScrollView.getScrollY() == 0
//                                && isTopHidden && dy > 0)) {
//
//                            initVelocityTrackerIfNotExists();
//                            mVelocityTracker.addMovement(ev);
//                            mLastY = y;
//                            return true;
//                        }
//                    } else if (mInnerScrollView instanceof RefreshGridView) {
//                        RefreshGridView lv1 = (RefreshGridView) mInnerScrollView;
//                        GridView lv = lv1.getContentView();
//                        View c = lv.getChildAt(lv.getFirstVisiblePosition());
//                        if (!isTopHidden || //
//                                (c != null //
//                                        && c.getTop() == 0//
//                                        && isTopHidden && dy > 0)) {
//
//                            initVelocityTrackerIfNotExists();
//                            mVelocityTracker.addMovement(ev);
//                            mLastY = y;
//                            return true;
//                        }
//                    }
//
//                }
//                break;
//            case MotionEvent.ACTION_CANCEL:
//            case MotionEvent.ACTION_UP:
//                mDragging = false;
//                recycleVelocityTracker();
//                break;
//        }
//        return super.onInterceptTouchEvent(ev);
//
//    }
//
//    private void getCurrentScrollView() {
//        mInnerScrollView = (ViewGroup) frameLayout.getRootView().findViewById(R.id.id_stickynavlayout_innerscrollview);
//
////        int currentItem = mViewPager.getCurrentItem();
////        PagerAdapter a = mViewPager.getAdapter();
////        if (a instanceof FragmentPagerAdapter) {
////            FragmentPagerAdapter fadapter = (FragmentPagerAdapter) a;
////            Fragment item = (Fragment) fadapter.instantiateItem(mViewPager,
////                    currentItem);
////
////        } else if (a instanceof FragmentStatePagerAdapter) {
////            FragmentStatePagerAdapter fsAdapter = (FragmentStatePagerAdapter) a;
////            Fragment item = (Fragment) fsAdapter.instantiateItem(mViewPager,
////                    currentItem);
////            mInnerScrollView = (ViewGroup) (item.getView()
////                    .findViewById(R.id.id_stickynavlayout_innerscrollview));
////        }
//
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        initVelocityTrackerIfNotExists();
//        mVelocityTracker.addMovement(event);
//        int action = event.getAction();
//        float y = event.getY();
//
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                if (!mScroller.isFinished())
//                    mScroller.abortAnimation();
//                mLastY = y;
//                return true;
//            case MotionEvent.ACTION_MOVE:
//                float dy = y - mLastY;
//
//
//                if (!mDragging && Math.abs(dy) > mTouchSlop) {
//                    mDragging = true;
//                }
//                if (mDragging) {
//                    scrollBy(0, (int) -dy);
//
//                    // 濡傛灉topView闅愯棌锛屼笖涓婃粦鍔ㄦ椂锛屽垯鏀瑰彉褰撳墠浜嬩欢涓篈CTION_DOWN
//                    if (getScrollY() == mTopViewHeight && dy < 0) {
//                        event.setAction(MotionEvent.ACTION_DOWN);
//                        dispatchTouchEvent(event);
//                        isInControl = false;
//                    }
//                }
//
//                mLastY = y;
//                break;
//            case MotionEvent.ACTION_CANCEL:
//                mDragging = false;
//                recycleVelocityTracker();
//                if (!mScroller.isFinished()) {
//                    mScroller.abortAnimation();
//                }
//                break;
//            case MotionEvent.ACTION_UP:
//                mDragging = false;
//                mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
//                int velocityY = (int) mVelocityTracker.getYVelocity();
//                if (Math.abs(velocityY) > mMinimumVelocity) {
//                    fling(-velocityY);
//                }
//                recycleVelocityTracker();
//                break;
//        }
//
//        return super.onTouchEvent(event);
//    }
//
//    public void fling(int velocityY) {
//        mScroller.fling(0, getScrollY(), 0, velocityY, 0, 0, 0, mTopViewHeight);
//        invalidate();
//    }
//
//    @Override
//    public void scrollTo(int x, int y) {
//        if (y < 0) {
//            y = 0;
//        }
////        LogUtil.i("liushen","mTopViewHeight"+mTopViewHeight);
////
////        LogUtil.i("liushen","y"+y);
////        LogUtil.i("liushen","getStatusBarHeight"+getStatusBarHeight());
////        LogUtil.i("liushen","toolbar.getMeasuredHeight()"+toolbar.getMeasuredHeight());
////        LogUtil.i("liushen","mTop.getMeasuredHeight()"+mTop.getMeasuredHeight());
//
//
////        mTopViewHeight = mTop.getMeasuredHeight() + getStatusBarHeight()+mNav.getMeasuredHeight();
////        changeHeight = mTop.getMeasuredHeight() - getStatusBarHeight();
//        //滑动指定位置改变toolbar背景
//        if(y>changeHeight){
//            toolbar.setBackgroundResource(R.color.xiaoji_titlebar_bg_new);
//            toolbar.findViewById(R.id.titlebar_title).setVisibility(View.VISIBLE);
//        }else{
//            //根据滑动距离改变透明度
//            float scale = (float) y / changeHeight;
//            float alpha = (255 * scale);
//            //4c9be5
//            int color = Color.argb((int) alpha, 76,155,229);
//            toolbar.setBackgroundColor(color);
//            toolbar.findViewById(R.id.titlebar_title).setVisibility(View.INVISIBLE);
//        }
//        if (y > mTopViewHeight) {
//            y = mTopViewHeight;
//        }
//        if (y != getScrollY()) {
//            super.scrollTo(x, y);
//        }
//
//        isTopHidden = getScrollY() == mTopViewHeight;
//
//    }
//
//    @Override
//    public void computeScroll() {
//        if (mScroller.computeScrollOffset()) {
//            scrollTo(0, mScroller.getCurrY());
//            invalidate();
//        }
//    }
//
//    private void initVelocityTrackerIfNotExists() {
//        if (mVelocityTracker == null) {
//            mVelocityTracker = VelocityTracker.obtain();
//        }
//    }
//
//    private void recycleVelocityTracker() {
//        if (mVelocityTracker != null) {
//            mVelocityTracker.recycle();
//            mVelocityTracker = null;
//        }
//    }
//
//}
