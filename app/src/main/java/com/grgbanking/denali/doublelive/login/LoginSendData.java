package com.grgbanking.denali.doublelive.login;

public class LoginSendData {

    /**
     * username : admin
     * password : 123456
     * grant_type : password
     * client_id : 5fbabd410ed248ada782b0c6815e6f29
     * client_secret : cf823db9a1f944d9b37c55318045921b8fcee9b3705b4bf6ae9a6f4a3e762f0a
     */

    private String username;
    private String password;
    private String grant_type;
    private String client_id;
    private String client_secret;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }
}
