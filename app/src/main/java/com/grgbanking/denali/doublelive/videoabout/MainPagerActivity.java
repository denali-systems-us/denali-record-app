package com.grgbanking.denali.doublelive.videoabout;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarEntry;
import com.grgbanking.denali.doublelive.scan.CustomCaptureActivity;
import com.grgbanking.denali.doublelive.socketUtil.BaseApplication;
import com.grgbanking.denali.doublelive.utils.AlertAnimationLoading;
import com.grgbanking.denali.doublelive.utils.DataFormatUtil;
import com.king.zxing.Intents;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;
import com.grgbanking.denali.doublelive.R;
import com.grgbanking.denali.doublelive.base.activity.AbstractBasePagerActivity;
import com.grgbanking.denali.doublelive.bean.AllListBean;
import com.grgbanking.denali.doublelive.bean.MainCountBean;
import com.grgbanking.denali.doublelive.bean.MainPageSendData;
import com.grgbanking.denali.doublelive.bean.MySection;
import com.grgbanking.denali.doublelive.bean.VideoItem2;
import com.grgbanking.denali.doublelive.http.ApiService;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.LFilePicker;
import com.grgbanking.denali.doublelive.scan.ScanActivity2;
import com.grgbanking.denali.doublelive.utils.MPChartHelper;
import com.yanzhenjie.permission.AndPermission;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.utils.ClickUtil;
import me.lake.librestreaming.utils.ConstantUtil;
import me.lake.librestreaming.utils.ToastUtils;
import rxhttp.wrapper.param.RxHttp;

/**
 * create by li
 * 视频主页
 */
public class MainPagerActivity extends AbstractBasePagerActivity implements CustomAdapt {

    /**----------------标题栏------------------**/
//    @BindView(R.id.toolbar)
//    Toolbar mToolbar;
//    @BindView(R.id.toolbar_title)
//    TextView mTitle;

    /**
     * ----------------main_info------------------
     **/
    @BindView(R.id.float_main_imgv)
    ImageView float_main_imgv;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.main_recycle)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_layout)
    View empty_layout;

    @BindView(R.id.search_imgv)
    ImageView search_imgv;
    @BindView(R.id.allcount_tv)
    TextView allcount_tv;
    @BindView(R.id.needupload_count_tv)
    TextView needupload_count_tv;
    @BindView(R.id.nopass_count_tv)
    TextView nopass_count_tv;

    @BindView(R.id.main_need_uplaod)
    View main_need_uplaod;
    @BindView(R.id.main_nopass)
    View main_nopass;
    @BindView(R.id.mainhead_nopass_lLayout)
    RelativeLayout mainhead_nopass_lLayout;

    @BindView(R.id.barChart)
    BarChart barChart;
    @BindView(R.id.barChart_tv)
    TextView barChart_tv;

    private List<MySection> mData = new ArrayList<>();
    private Context context;
    SectionQuickAdapter adapter;

    private int REQUEST_CODE_SCAN = 1113;
    private boolean mError = false;
    private boolean mNoData = false;
    private String loginToken;
    private String filecount;
    private String login_username;
    public static int REQUESTCODE_FROM_ACTIVITY = 1030;
    private static final int STORAGE_REQUEST_CODE = 1021;
    private MainCountBean.DataBean data;
    private String auditTime;
    private String recordTime;
    private CountDownTimer timer;
    private int count;
    //初始化Logger
    protected static Logger logger = Logger.getLogger(MainPagerActivity.class);

    // 数据库变量
    private UploadSQLiteOpenHelper helper;
    private SQLiteDatabase db;

    /**
     * bar
     */
    private List<String> xAxisValues;
    private List<String> titles;
    private List<List<Float>> yAxisValues;

    private AlertAnimationLoading alertLoading1;
    private boolean isContinuousScan;
    private int REQUEST_CODE_SCAN2 = 1123;

    @Override
    protected void initView() {

        alertLoading1 = new AlertAnimationLoading(MainPagerActivity.this,
                R.layout.base_loading_dialog);
        alertLoading1.setMessage("加载中....");
        alertLoading1.setCanceledOnTouchOutside(true);

        loginToken = SharedPreferencesUtil.getString(MainPagerActivity.this, SharedPreferencesUtil.LOGINTOKEN, "");
        filecount = SharedPreferencesUtil.getString(MainPagerActivity.this, SharedPreferencesUtil.FILECOUNT, "");
        login_username = SharedPreferencesUtil.getString(MainPagerActivity.this, SharedPreferencesUtil.LOGINUSERNAME, "");
        helper = new UploadSQLiteOpenHelper(MainPagerActivity.this,
                "upload.db", null, 1);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // 进入页面，刷新数据
        empty_layout.setVisibility(View.VISIBLE);
        barChart_tv.setVisibility(View.VISIBLE);
        barChart.setVisibility(View.GONE);
        ConstantUtil.getInstance().setFirstRefresh(true);
        mSwipeRefreshLayout.setRefreshing(true);
        initRefreshLayout();
        getData();
//        getData2();
//        getTest();

        String dateTimeZone = DataFormatUtil.getDateStrIncludeTimeZone();
        Log.e("time", dateTimeZone);

//        mData = DataServer.getSectionData();
//        initData(mData ,data );
    }

    private void initRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeColors(Color.rgb(70, 160, 251));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ConstantUtil.getInstance().setFirstRefresh(false);
                getData();
            }
        });
    }

//    private void getTest() {
//
//        while (true) {
//            RxHttp.get(ApiService.BASE_URL_cloud + "api/v1/video/count") //Get请求
//                    .addHeader("connection", "Keep-Alive")
//                    .addHeader("Authorization", "Bearer " + loginToken)
//                    .asClass(MainCountBean.class)  //这里返回Observable<Response> 对象
//                    .subscribe(response -> {
//                        data = response.getData();
//
//                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", Locale.getDefault());
//                        Log.e("#count#", "时间为：" + sdf.format(new Date()));
//                        logger.error("时间为：" + sdf.format(new Date()));
//                        count++;
//                        Log.e("#count#", "次数为：" + count);
//                        logger.error("次数为：" + count);
//
//                        //成功回调
//                    }, throwable -> {
//                        //失败回调
//                        ToastUtils.showToast(MainPagerActivity.this, "失败");
//                    });
//        }
//    }


    private void initLocalList() {
        new LFilePicker()
                .withActivity(this)
                .withRequestCode(REQUESTCODE_FROM_ACTIVITY)
                .withTitle("文件选择")
//                .withIconStyle(mIconType)
                .withMutilyMode(false)
                .withMaxNum(1)
                .withStartPath("/storage/emulated/0/live/" + login_username)//指定初始显示路径
                .withNotFoundBooks("至少选择一个文件")
                .withTitleColor("#333333")
//                .withIsGreater(false)//过滤文件大小 小于指定大小的文件
//                .withFileSize(5000 * 1024 *1024)//指定文件大小为500K
                .withChooseMode(true)//文件夹选择模式
                .withFileFilter(new String[]{"mp4"})
                .start();

    }

    private void getData() {

        alertLoading1.showDialog();
//        RxHttp.get(ApiService.BASE_URL_cloud + "api/v1/video/count") //Get请求
        RxHttp.get(BaseApplication.getInstance().getBASE_URL_cloud() + "api/v1/video/count") //Get请求
                .addHeader("connection", "Keep-Alive")
                .addHeader("Authorization", "bearer" + loginToken)
                .asClass(MainCountBean.class)  //这里返回Observable<Response> 对象
                .subscribe(response -> {
                    mSwipeRefreshLayout.setRefreshing(false);
                    data = response.getData();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //更改UI；
                            initList(data);
                        }
                    });
                    //成功回调
                }, throwable -> {
                    mSwipeRefreshLayout.setRefreshing(false);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            alertLoading1.hideDialog();
                            //更改UI；
                            empty_layout.setVisibility(View.VISIBLE);
                            barChart_tv.setVisibility(View.VISIBLE);
                            barChart.setVisibility(View.GONE);

                        }
                    });

                    //失败回调
//                    ToastUtils.showToast(MainPagerActivity.this, "失败");
                });

    }

    private void initList(MainCountBean.DataBean data) {
        if (data == null) {
            empty_layout.setVisibility(View.VISIBLE);
            mainhead_nopass_lLayout.setVisibility(View.GONE);
            main_nopass.setVisibility(View.GONE);
            //bar
            barChart_tv.setVisibility(View.VISIBLE);
            barChart.setVisibility(View.GONE);

            alertLoading1.hideDialog();
        } else {
            //bar 为了不显示默认No chart data available提示，间接
            if (ConstantUtil.getInstance().isFirstRefresh()) {
                barChart_tv.setVisibility(View.VISIBLE);
                barChart.setVisibility(View.GONE);
            }
            MainPageSendData sendData = new MainPageSendData();
            RxHttp.postJson(BaseApplication.getInstance().getBASE_URL_cloud() + "api/v1/video/getVideoForCheck")
                    .addHeader("connection", "Keep-Alive")
                    .addHeader("Authorization", "Bearer " + loginToken)
                    .add("vedioCheckQueryDto", sendData.toString())//添加参数，非必须
                    .asClass(AllListBean.class)
                    .subscribe(response -> {    //这里students 即为List<Student>对象
                        //成功回调
                        mSwipeRefreshLayout.setRefreshing(false);
                        List<MainCountBean.DataBean.MonthCountsBean> list = new ArrayList<>();
                        list = data.getMonthCounts();

                        mData.clear();

                        for (int i = 0; i < list.size(); i++) {
                            mData.add(new MySection(true, "2020年" + data.getMonthCounts().get(i).getMonth() + "月"));
                        }
                        for (int j = 0; j < response.getData().getRows().size(); j++) {

                            //当有值为空时，不加入列表
                            if (response.getData().getRows().get(j).getPolicyName() != null ||
                                    response.getData().getRows().get(j).getInsureType() != null) {

                                if (response.getData().getRows().get(j).getRecordTime().contains("T")) {
                                    recordTime = DataFormatUtil.switchCreateTime(response.getData().getRows().get(j).getRecordTime());
                                } else {
                                    recordTime = response.getData().getRows().get(j).getRecordTime();
                                }
                                if (response.getData().getRows().get(j).getAuditTime() != null) {
                                    auditTime = DataFormatUtil.switchCreateTime((String) response.getData().getRows().get(j).getAuditTime());
                                } else
                                    auditTime = null;
                                mData.add(new MySection(false, new VideoItem2(
                                        response.getData().getRows().get(j).getId(),
                                        response.getData().getRows().get(j).getPolicyCard(),
                                        response.getData().getRows().get(j).getOrganizationCode(),
                                        response.getData().getRows().get(j).getPolicyNum(),
                                        response.getData().getRows().get(j).getStaffNum(),
                                        DataFormatUtil.switchCreateTime(response.getData().getRows().get(j).getCreateTime()),
                                        recordTime,
                                        response.getData().getRows().get(j).getVideoId(),
                                        response.getData().getRows().get(j).getStaffName(),
                                        response.getData().getRows().get(j).getStaffCard(),
                                        response.getData().getRows().get(j).getPolicyName(),
                                        response.getData().getRows().get(j).getInsureType(),
                                        response.getData().getRows().get(j).getResult(),
                                        response.getData().getRows().get(j).getCreator(),
                                        response.getData().getRows().get(j).getAuditor(),
                                        response.getData().getRows().get(j).getAuditDesc(),
                                        auditTime,
                                        response.getData().getRows().get(j).getAuditStatus(),
                                        response.getData().getRows().get(j).getInsureTypeDesc(),
                                        response.getData().getRows().get(j).getDuration(),
                                        response.getData().getRows().get(j).getVideoUrl(),
                                        response.getData().getRows().get(j).getImgUrl()
                                )));
                            }

                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertLoading1.hideDialog();
                                //更改UI；
                                initData(mData, data);
                                initBarChartData(data);

                            }
                        });

                    }, throwable -> {
                        mSwipeRefreshLayout.setRefreshing(false);
                        //失败回调
//                        ToastUtils.showToast(MainPagerActivity.this, "失败");
                    });
        }
    }

    private void initBarChartData(MainCountBean.DataBean data) {

        //修改这里看看提示会不会改变
        barChart_tv.setVisibility(View.GONE);
        barChart.setVisibility(View.VISIBLE);

        List<String> xAxisValues;
        List<BarEntry> yAxisValues;
        xAxisValues = new ArrayList<>();
        yAxisValues = new ArrayList<>();

        if (data.getMonthCounts().size() == 1) {

            xAxisValues.add(Integer.parseInt(data.getMonthCounts().get(0).getMonth()) - 2 + "月");
            xAxisValues.add(Integer.parseInt(data.getMonthCounts().get(0).getMonth()) - 1 + "月");
            xAxisValues.add(data.getMonthCounts().get(0).getMonth() + "月");
            xAxisValues.add(Integer.parseInt(data.getMonthCounts().get(0).getMonth()) + 1 + "月");

            yAxisValues.add(new BarEntry(0f, new float[]{
                    0f}));
            yAxisValues.add(new BarEntry(1f, new float[]{
                    0f}));
            yAxisValues.add(new BarEntry(2f, new float[]{
                    data.getMonthCounts().get(0).getCount()}));
            yAxisValues.add(new BarEntry(3f, new float[]{
                    0f}));

        } else if (data.getMonthCounts().size() == 2) {
            xAxisValues.add(Integer.parseInt(data.getMonthCounts().get(0).getMonth()) - 1 + "月");
            xAxisValues.add(data.getMonthCounts().get(0).getMonth() + "月");
            xAxisValues.add(data.getMonthCounts().get(1).getMonth() + "月");
            xAxisValues.add(Integer.parseInt(data.getMonthCounts().get(1).getMonth()) + 1 + "月");

            yAxisValues.add(new BarEntry(0f, new float[]{
                    0f}));
            yAxisValues.add(new BarEntry(1f, new float[]{
                    data.getMonthCounts().get(0).getCount()}));
            yAxisValues.add(new BarEntry(2f, new float[]{
                    data.getMonthCounts().get(1).getCount()}));
            yAxisValues.add(new BarEntry(3f, new float[]{
                    0f}));
        } else if (data.getMonthCounts().size() >= 3) {
            for (int i = 0; i < data.getMonthCounts().size(); i++) {
                xAxisValues.add(data.getMonthCounts().get(i).getMonth() + "月");

                yAxisValues.add(new BarEntry(i, new float[]{
                        data.getMonthCounts().get(i).getCount()}));
            }
        }

        MPChartHelper.setStackChart(barChart, xAxisValues, yAxisValues, "", 10, null);
    }

    private void initData(List<MySection> mData, MainCountBean.DataBean data) {
        initRecycleView(mData, data);
    }

    private void initRecycleView(List<MySection> mData, MainCountBean.DataBean data) {

        if (data != null) {
            allcount_tv.setText(String.format(
                    getString(R.string.allCount_tv), data.getTotalCount() + ""));
            if (data.getFailCount() == 0) {
                mainhead_nopass_lLayout.setVisibility(View.GONE);
                main_nopass.setVisibility(View.GONE);
            } else {
                mainhead_nopass_lLayout.setVisibility(View.VISIBLE);
                main_nopass.setVisibility(View.VISIBLE);
                nopass_count_tv.setText(String.format(
                        getString(R.string.nopass_count), data.getFailCount() + ""));
            }
        }

        if (mData.size() == 0 || data == null) {
            empty_layout.setVisibility(View.VISIBLE);
            mainhead_nopass_lLayout.setVisibility(View.GONE);
            main_nopass.setVisibility(View.GONE);
        } else {
            empty_layout.setVisibility(View.GONE);

            adapter = new SectionQuickAdapter(R.layout.item_video_list, R.layout.def_section_head, mData, context);
            //添加动画
            adapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.AlphaIn);
            //设置是否使用空布局（默认为true）
            adapter.setUseEmpty(true);
//        adapter.setEmptyView(R.layout.empty_view);

            adapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {

                    MySection mySection = mData.get(position);

                    if (mySection.isHeader()) {

                    } else {
                        VideoItem2 videoItem = (VideoItem2) mySection.getObject();
                        Intent intentDetail = new Intent(MainPagerActivity.this,
                                VideoDetailActivity.class);
                        intentDetail.putExtra("status", String.valueOf(videoItem.getAuditStatus()));
                        intentDetail.putExtra("videoItem", videoItem);
                        startActivity(intentDetail);
                    }
                }

            });

            adapter.setOnItemChildClickListener(new OnItemChildClickListener() {
                @Override
                public void onItemChildClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
//                    ToastUtils.showToast(MainPagerActivity.this,
//                            ("onItemChildClick: " + position));
                }
            });
            mRecyclerView.setAdapter(adapter);

            //设置头部
//            View view = getLayoutInflater().inflate(R.layout.main_head_view, mRecyclerView, false);
//            ImageView search_imgv = view.findViewById(R.id.search_imgv);
//            search_imgv.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    //跳转到搜索界面
//                    startActivity(new Intent(MainPagerActivity.this, SearchActivity.class));
//                }
//            });
//
//            TextView allcount_tv = view.findViewById(R.id.allcount_tv);
//            RelativeLayout mainhead_nopass_lLayout = view.findViewById(R.id.mainhead_nopass_lLayout);
//            RelativeLayout mainhead_lLayout = view.findViewById(R.id.mainhead_lLayout);
//            //这2个TextView都是可变的
//            TextView needupload_count_tv = (TextView) view.findViewById(R.id.needupload_count_tv);
//            if (filecount == null || "".endsWith(filecount)) {
//                needupload_count_tv.setText("0个视频需要上传");
//            } else {
//                needupload_count_tv.setVisibility(View.VISIBLE);
//                needupload_count_tv.setText(String.format(
//                        getString(R.string.needupload_count), filecount));
//            }
//            TextView nopass_count_tv = (TextView) view.findViewById(R.id.nopass_count_tv);
//            if (data != null) {
//                allcount_tv.setText(String.format(
//                        getString(R.string.allCount_tv), data.getTotalCount() + ""));
//                if (data.getFailCount() == 0) {
//                    mainhead_nopass_lLayout.setVisibility(View.GONE);
//                } else {
//                    mainhead_nopass_lLayout.setVisibility(View.VISIBLE);
//                    nopass_count_tv.setText(String.format(
//                            getString(R.string.nopass_count), data.getFailCount() + ""));
//                }
//            }
//
//            view.findViewById(R.id.main_need_uplaod).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    //跳转到待上传视频列表页
////                    Intent intentVideo=new Intent(MainPagerActivity.this, LocalVideoListActivity.class);
////                    intentVideo.putExtra("status", "1");
////                    startActivity(intentVideo);
//
//                    initLocalList();
//
//                }
//            });
//            view.findViewById(R.id.main_nopass).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    //跳转到未通过视频列表页
//                    Intent intentVideo = new Intent(MainPagerActivity.this, LoadMoreVideoListActivity.class);
//                    intentVideo.putExtra("status", "2");
//                    startActivity(intentVideo);
//                }
//            });
//
//            adapter.addHeaderView(view);
        }
    }


    public void setToolbarTitle(int resId) {
//        mTitle.setText(resId);
    }

    @Override
    protected void onNewIntent(Intent intent) { //待上传视频列表跳转调用次方法
        super.onNewIntent(intent);
        // 进入页面，刷新数据
        if ( ConstantUtil.getInstance().isUploadToMain()) {
            ConstantUtil.getInstance().setFirstRefresh(false);
            getData();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //统计路径文件个数，暂时不适用
//        File file = new File("/storage/emulated/0/live/"+ login_username);
//        File[] fileArray =file.listFiles();
//        if(fileArray!=null) {
//            int fileSize = fileArray.length;
//            if (fileSize == 0) {
//                needupload_count_tv.setText("0个视频需要上传");
//            } else {
//                needupload_count_tv.setVisibility(View.VISIBLE);
//                needupload_count_tv.setText(String.format(
//                        getString(R.string.needupload_count), String.valueOf(fileSize)));
//            }
//        }else {
//            needupload_count_tv.setText("0个视频需要上传");
//        }

        //通过查找数据库来确定本地列表数据
        queryALL();

        //--------------------------------end------------------
    }

    private void queryALL() {

        int fileSize = 0;
        Cursor cursor = helper.getReadableDatabase().rawQuery(
                "select * from uploads where loginName =? order by recordTime  desc", new String[]{login_username});

        // 从数据库中Record表里找到name=tempName的id
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    //遍历cursor，取出数据
                    fileSize++;

                } while (cursor.moveToNext());
            }
        }
        cursor.close();

        if (fileSize == 0) {
            needupload_count_tv.setText("0个视频需要上传");
        } else {
            needupload_count_tv.setVisibility(View.VISIBLE);
            needupload_count_tv.setText(String.format(
                    getString(R.string.needupload_count), String.valueOf(fileSize)));
        }
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_section_uer;
//    }
    protected int getLayoutId() {
        return R.layout.activity_main_pager;
    }

    @Override
    protected void initToolbar() {
//        setSupportActionBar(mToolbar);
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setDisplayShowTitleEnabled(false);
//        }
//
//        mToolbar.setNavigationOnClickListener(v -> onBackPressedSupport());
//        mToolbar.setNavigationOnClickListener(null);
    }

    @Override
    protected void initEventAndData() {


    }

    @OnClick({R.id.float_main_imgv, R.id.search_imgv, R.id.main_need_uplaod, R.id.main_nopass

    })
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.float_main_imgv:
                if (ClickUtil.isNotFastClick()) {
//                    initScan();
                    initScan2();

                }
                break;
            case R.id.search_imgv:
                //跳转到搜索界面
                startActivity(new Intent(MainPagerActivity.this, SearchActivity.class));
                break;
            case R.id.main_nopass:
                //跳转到未通过视频列表页
                Intent intentVideo = new Intent(MainPagerActivity.this, LoadMoreVideoListActivity.class);
                intentVideo.putExtra("status", "2");
                startActivity(intentVideo);
                break;

            case R.id.main_need_uplaod:
                //跳转到待上传视频列表页
//                    Intent intentVideo=new Intent(MainPagerActivity.this, LocalVideoListActivity.class);
//                    intentVideo.putExtra("status", "1");
//                    startActivity(intentVideo);

                initLocalList();

                break;
            default:
                break;
        }
    }

    private void initScan2() {
        isContinuousScan = true;
        if (ContextCompat.checkSelfPermission(MainPagerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
        }

        AndPermission.with(this)
                .permission(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                // 准备方法，和 okhttp 的拦截器一样，在请求权限之前先运行改方法，已经拥有权限不会触发该方法
                .rationale((context, permissions, executor) -> {
                    // 此处可以选择显示提示弹窗
                    executor.execute();
                })
                // 用户给权限了
                .onGranted(permissions -> {
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeCustomAnimation(this,R.anim.in,R.anim.out);
//                    Intent intent = new Intent(this, CaptureFragmentActivity.class);
                    Intent intent = new Intent(this, CustomCaptureActivity.class);
                    intent.putExtra( "key_title","");
                    intent.putExtra("key_continuous_scan",isContinuousScan);
                    ActivityCompat.startActivityForResult(MainPagerActivity.this,intent,REQUEST_CODE_SCAN2,optionsCompat.toBundle());
                })
                // 用户拒绝权限，包括不再显示权限弹窗也在此列
                .onDenied(permissions -> {
                    // 判断用户是不是不再显示权限弹窗了，若不再显示的话进入权限设置页
                    if (AndPermission.hasAlwaysDeniedPermission(MainPagerActivity.this, permissions)) {
                        // 打开权限设置页
                        AndPermission.permissionSetting(MainPagerActivity.this).execute();
                        return;
                    }
                })
                .start();
    }




//    private void initScan() {
//
//        if (ContextCompat.checkSelfPermission(MainPagerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
//        }
//
//        AndPermission.with(this)
//                .permission(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
//                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                // 准备方法，和 okhttp 的拦截器一样，在请求权限之前先运行改方法，已经拥有权限不会触发该方法
//                .rationale((context, permissions, executor) -> {
//                    // 此处可以选择显示提示弹窗
//                    executor.execute();
//                })
//                // 用户给权限了
//                .onGranted(permissions -> {
//                    Intent intent = new Intent(MainPagerActivity.this, CaptureActivity.class);
//                    /*ZxingConfig是配置类
//                     *可以设置是否显示底部布局，闪光灯，相册，
//                     * 是否播放提示音  震动
//                     * 设置扫描框颜色等
//                     * 也可以不传这个参数
//                     * */
//                    ZxingConfig config = new ZxingConfig();
//                    // config.setPlayBeep(false);//是否播放扫描声音 默认为true
//                    //  config.setShake(false);//是否震动  默认为true
//                    // config.setDecodeBarCode(false);//是否扫描条形码 默认为true
////                                config.setReactColor(R.color.colorAccent);//设置扫描框四个角的颜色 默认为白色
////                                config.setFrameLineColor(R.color.colorAccent);//设置扫描框边框颜色 默认无色
////                                config.setScanLineColor(R.color.colorAccent);//设置扫描线的颜色 默认白色
//                    config.setFullScreenScan(true);//是否全屏扫描  默认为true  设为false则只会在扫描框中扫描
//                    intent.putExtra(Constant.INTENT_ZXING_CONFIG, config);
//                    startActivityForResult(intent, REQUEST_CODE_SCAN);
//                })
//                // 用户拒绝权限，包括不再显示权限弹窗也在此列
//                .onDenied(permissions -> {
//                    // 判断用户是不是不再显示权限弹窗了，若不再显示的话进入权限设置页
//                    if (AndPermission.hasAlwaysDeniedPermission(MainPagerActivity.this, permissions)) {
//                        // 打开权限设置页
//                        AndPermission.permissionSetting(MainPagerActivity.this).execute();
//                        return;
//                    }
//                })
//                .start();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // 扫描二维码/条码回传Li
//        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
//            if (data != null) {
//
//                String content = data.getStringExtra(Constant.CODED_CONTENT);
//                //扫码成功返回更新布局到保单信息
//                Intent intentScan = new Intent(MainPagerActivity.this,
//                        ScanActivity2.class);
//                intentScan.putExtra("scanValue", content);
//                startActivity(intentScan);
//                setResult(RESULT_OK, intentScan);
//            }
//        }
        if (requestCode == REQUEST_CODE_SCAN2 && resultCode == RESULT_OK) {
            if (data != null) {
                String content = data.getStringExtra(Intents.Scan.RESULT);
                //扫码成功返回更新布局到保单信息
                Intent intentScan = new Intent(MainPagerActivity.this,
                        ScanActivity2.class);
                intentScan.putExtra("scanValue", content);
                startActivity(intentScan);
                setResult(RESULT_OK, intentScan);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }


    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}
