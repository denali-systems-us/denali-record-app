/*
 *     (C) Copyright 2019, ForgetSky.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.grgbanking.denali.doublelive.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Cache with Network
 * Created by ForgetSky on 2019/3/17.
 */
public class HeadCacheInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder()
//                .header("platform", "platform")//平台
//                .header("sysVersion", "sysVersion")//系统版本号
//                .header("device", "device")//设备信息
//                .header("screen", "screen")//屏幕大小
//                .header("uuid", "uuid")//设备唯一码
//                .header("version", "version")//app版本
//                .header("apiVersion", "apiVersion")//api版本
//                .header("channelId", "channelId")//渠道
//                .header("networkType", "networkType")//网络类型
                .header("token", "Bearer "+ "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRUYWciOm51bGwsInVzZXJfbmFtZSI6ImFkbWluIiwicm9sZVR5cGUiOm51bGwsImF1dGhvcml0aWVzIjpbImFkZEJ1Y2tDZ2RzIiwic2VlQ2VudGVyIiwicm9sZXNTdGF0dXMiLCJlZGl0X3NwY19kYXRhc291cmNlIiwic2VhcmNoX2F1dGgiLCJkZWxldGVEc0dyb3VwIiwic2VlQXBwcyIsImRsdF9zcGNfZGF0YXNvdXJjZSIsImRlbGV0ZURvY3VtZW50IiwiZWRpdFR5cGUiLCJkZWxldGVDZ2RzR3JvdXAiLCJmaW5kUmVxdWVzdE1vZGVsIiwiZWRpdHNlcnZpY2UiLCJkb1VwbG9hZEFwcHMiLCJ2aWV3X2RldGFpbCIsInJvbGVzRGVsIiwiYWxnb0FkZCIsImVkaXRUZW5hbnQiLCJsaW5rQ2dkcyIsImNyZWF0ZUFwcCIsImZpbmRDZ2RzIiwic2VlUmVuZXdhTGlzdCIsImRvd25sb2FkQ29uZmlnIiwiZ3JvdXBzRGVsIiwiZWRpdERzR3JvdXAiLCJkb3dubG9hZERhdGEiLCJlZGl0Q29udGFjdCIsInVzZXJTdGF0dXMiLCJ2aWV3LXRlbXBsZXRlIiwiYWRkVGVuYW50IiwiYWRkRHNHcm91cCIsImZpbmlzaFJlcXVlc3QiLCJmaW5kQXBwc1J1biIsImFkZF9zcGNfYm9hcmQiLCJlbmFibGVTZXJ2aWNlIiwiZGVwbG95QXBwIiwiYWRkX2NoYXJ0Iiwic2VlIiwiZmluZEFwcHNMaXN0IiwiY2xvc2VSZXF1ZXN0IiwiaW52b2tlTGljZW5jZSIsImRlbGV0ZU9yZGVyIiwiYWRkR3JvdXAiLCJlZGl0UHJvamVjdCIsInNlZVRlbmFudExpc3QiLCJzdGFydFJlcXVlc3RzIiwiZWRpdENnZHMiLCJncm91cFN0YXR1cyIsInJvbGVzRWRpdCIsImZpbmRSb2xlcyIsImFwcEVkaXQiLCJmaW5kV29ya1NwYWNlIiwiYWRkUHJvamVjdCIsInNlZVNlcnZpY2VEZXRhaWwiLCJkZWxldGVDb250YWN0IiwiZGx0X3NwY19ib2FyZCIsInNlbmRSZXF1ZXN0IiwiZmluZFByb2R1Y3RDb25maWciLCJkZWxldGVSZXF1ZXN0IiwicmViYWNrUmVxdWVzdCIsInNlZVNlcnZpY2VMaXN0IiwiZmluZERhdGEiLCJnb1BheSIsImVkaXRDdXN0b21lciIsImF1dGhFZGl0IiwiZGVsZXRlU2VydmljZSIsImVkaXRVc2VyIiwiYWRkQ2dkc0dyb3VwIiwiZGlzYWJsZVNlcnZpY2UiLCJwdXNoUmVxdWVzdHMiLCJhbGdvRWRpdCIsImFkZEN1c3RvbWVyIiwiYWxnb1F1ZXJ5IiwiZWRpdENnZHNHcm91cCIsImZpbmRBbGxQcm9qZWN0cyIsImFkZERvY3VtZW50IiwiYWRkIiwiYWRkUmVxdWVzdCIsImRvd25Mb2FkIiwidGVzdEFsZ29yaXRobSIsImZpbmRUZW1wbGF0ZSIsImRlbGV0ZVRlbmFudCIsImRlbGV0ZVR5cGUiLCJlZGl0UmVxdWVzdCIsInNlZVVzZVN0YXR1cyIsImRlbGV0ZVByb2plY3QiLCJhZGRDZ2RzIiwic3RvcEFwcHMiLCJzZWVEYXNoQm9yZGUiLCJncm91cEVkaXQiLCJzZW5kIiwic2VlQWxnb01vbml0b3IiLCJzZWVSdW4iLCJzZWVVc2VMaXN0IiwicnVuQXBwcyIsImFwcERlbCIsImZpbmRVcGxvYWRlZEFwcHMiLCJkZWxldGVDb25maWciLCJhZGRQcm9qZWN0Q29uZmlnIiwiZmluZFJlcXVlc3RzIiwiYWRkU2VydmljZSIsImdyb3VwTWVtYmVyIiwiZmluZEFsbEFwcHMiLCJmaW5kSW52b2tlTXNnIiwiYmFja1JlcXVlc3RzIiwic2VlLXZpZXciLCJmaW5kVXNlcnMiLCJhZGRfc3BjX2RhdGFzb3VyY2UiLCJtYXBwaW5nX3NwY19kdHNzIiwiYWxnb0RlbCIsInNlZVB1cmNoYXNlSW5mbyIsInNlZU9yZGVyTGlzdCIsImFkZEN1c3RvbWVyQ29udGFjdCIsInNlZURhdGEiLCJlZGl0Q29uZmlnIiwidXNlckFkZCIsImNoYW5nZVBhc3N3b3JkIiwiYWRkVHlwZSIsImZpbmRSb2xlR3JvdXBzIiwiYWRkQXBwIiwiZG93blNyYyIsImZpbmRDdXN0b21lckxpc3QiXSwidGlkIjpudWxsLCJjbGllbnRfaWQiOiIzZWUyMzE5N2FmMDI0ODJiOTk4YTlhNDIwY2UwZWZiNCIsInVpZCI6MSwicmVhbE5hbWUiOiLotoXnuqfnrqHnkIblkZgiLCJ0ZW5hbnROYW1lIjpudWxsLCJzY29wZSI6WyJhbGwiXSwidXNlclR5cGUiOjEsImV4cCI6MTU5MTM4NDc3MywianRpIjoiMDRhZmJiMzItYWFkZi00MDY5LTlkZjctMDU2M2QyN2ExOGExIn0.UWz2u6JzfukrWKSYMrGfHLiS24E8_ZuXEfGeJjIpZVE-1du2s7_xmR4xtjleIuQ0hXlrYS44T7zwxpVH2wsGz6mrqoblzr03zJn8nECVX7mZgwV1R0dTMYXzVwdbvzn1nHkU53qBgNKeJpT6YijnkLBygHhRaJGOlDnD2LMfw-gi4lNXr_67XaQSICvfPwm1svVnXiTYgYRD4FdzK-Yo5ZgX275w5U7K6sVCL5103OMfEhQwokTiuQ7lWjgQkkoiImDKo14Jtaw_a10pAPUWgZVPRWJAMv0xzkNbivKW3qseSCLyuxvQd2PkOetWT1i-afKmozyieqmwS0JR6ykdRw");//令牌

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
