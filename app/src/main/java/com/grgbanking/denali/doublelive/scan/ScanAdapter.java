/*
 *     (C) Copyright 2019, ForgetSky.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.grgbanking.denali.doublelive.scan;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.grgbanking.denali.doublelive.R;

import java.util.List;


public class ScanAdapter extends BaseQuickAdapter<Model.DenaliBean, BaseViewHolder> {





    public ScanAdapter(int layoutResId, @Nullable List<Model.DenaliBean> list) {
        super(layoutResId,list);

    }


    @Override
    protected void convert(BaseViewHolder helper, Model.DenaliBean item) {

        helper.setText(R.id.product_title, item.getName());
        helper.setText(R.id.product_content,item.getMenu());
        helper.setText(R.id.list_tv,item.getId());

    }

}
