package com.grgbanking.denali.doublelive.videoabout;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.grgbanking.denali.doublelive.R;
import com.grgbanking.denali.doublelive.bean.VideoItem;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @Description:
 */
public class LoadMoreAdapter extends BaseQuickAdapter<VideoItem, BaseViewHolder> implements LoadMoreModule {

    public LoadMoreAdapter() {
        super(R.layout.item_video_list);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder helper, @Nullable VideoItem item) {

//        addChildClickViewIds(R.id.btn_status,R.id.item_video_imgv);

        helper.setText(R.id.item_policyname_tv,item.getPolicyname());
        helper.setText(R.id.item_insuretype_tv,item.getInsuretype());
        helper.setText(R.id.item_recordtime_tv,item.getRecordtime());
        helper.setText(R.id.item_duration_tv,item.getDurationtime());

        helper.setText(R.id.btn_status, item.getStatu());

        helper.setImageResource(R.id.item_video_imgv, R.mipmap.login_big);
    }

}
