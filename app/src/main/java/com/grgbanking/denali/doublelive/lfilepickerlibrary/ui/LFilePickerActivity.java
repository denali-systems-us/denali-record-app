package com.grgbanking.denali.doublelive.lfilepickerlibrary.ui;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.grgbanking.denali.doublelive.videoabout.LocalVideoDetailActivity;
import com.leon.lfilepickerlibrary.R;
import com.leon.lfilepickerlibrary.utils.SharedPreferencesUtil;
import com.grgbanking.denali.doublelive.base.activity.AbstractSimpleActivity;
import com.grgbanking.denali.doublelive.bean.ScanBean;
import com.grgbanking.denali.doublelive.bean.VideoItem;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.adapter.FileAdapter;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.filter.LFileFilter;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.model.ParamEntity;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.utils.FileUtils;
import com.grgbanking.denali.doublelive.lfilepickerlibrary.utils.StringUtils;
import com.grgbanking.denali.doublelive.videoabout.UploadSQLiteOpenHelper;
import com.yanzhenjie.permission.AndPermission;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.jessyan.autosize.internal.CustomAdapt;
import me.lake.librestreaming.utils.ConstantUtil;
import me.lake.librestreaming.utils.SharedPreferencesMediaUtil;

public class LFilePickerActivity extends AbstractSimpleActivity implements CustomAdapt {

    private final String TAG = "FilePickerLeon";
    private String mPath;
    private List<File> mListFiles;
    private ArrayList<String> mListNumbers = new ArrayList<String>();//存放选中条目的数据地址
    private FileAdapter mAdapter;
    private ParamEntity mParamEntity;
    private LFileFilter mFilter;
    private static final int STORAGE_REQUEST_CODE = 1023;

    /**
     * ----------------标题栏------------------
     **/

    @BindView(com.grgbanking.denali.doublelive.R.id.toolbar)
    Toolbar mToolbar;
    @BindView(com.grgbanking.denali.doublelive.R.id.toolbar_title)
    TextView mTitle;
    @BindView(com.grgbanking.denali.doublelive.R.id.file_recycle)
    RecyclerView file_recycle;
    @BindView(com.grgbanking.denali.doublelive.R.id.empty_view)
    View mEmptyView;

    //录制的视频节点信息
    private String videoName2;
    private String scanValue;
    private String result;
    private String recordTime;
    private ScanBean scanBean;
    String filePath; //数据库保存的数据
    // 数据库变量
    private UploadSQLiteOpenHelper helper;
    private SQLiteDatabase db;
    private String videoUrl;
    private String loginName;

    List<VideoItem> videoItem = new ArrayList<>();

    @Override
    protected void initView() {
        mParamEntity = (ParamEntity) getIntent().getExtras().getSerializable("param");
        loginName = SharedPreferencesUtil.getString(LFilePickerActivity.this, SharedPreferencesUtil.LOGINUSERNAME, "");
        videoUrl = SharedPreferencesMediaUtil.getString(LFilePickerActivity.this, SharedPreferencesMediaUtil.RECORDVIDEOPATH, "");
        setToolbarTitle(R.string.list_title);

//        initPermission();
        // 2. 实例化数据库SQLiteOpenHelper子类对象
//        helper = new UploadSQLiteOpenHelper(LFilePickerActivity.this);
        helper = new UploadSQLiteOpenHelper(LFilePickerActivity.this,
                "upload.db", null, 1);

        if (!checkSDState()) {
            Toast.makeText(this, R.string.lfile_NotFoundPath, Toast.LENGTH_SHORT).show();
            return;
        }
        mPath = mParamEntity.getPath();
        if (StringUtils.isEmpty(mPath)) {
            //如果没有指定路径，则使用默认路径
            mPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        }

        mFilter = new LFileFilter(mParamEntity.getFileTypes());
        mListFiles = FileUtils.getFileList(mPath, mFilter, mParamEntity.isGreater(), mParamEntity.getFileSize());

        //没有数据
        if (mListFiles.size() == 0 || mListFiles == null) {
            mEmptyView.setVisibility(View.VISIBLE);
            file_recycle.setVisibility(View.GONE);
        } else {

            //删除掉本地列表不能播放的视频文件
            for (int i = mListFiles.size() - 1; i >= 0; i--) {
                if (FileUtils.getPlayTime(mListFiles.get(i).getPath()) == null) {

                    File item = mListFiles.get(i);
                    if (item.exists()) {
                        item.delete();
                    }
                    mListFiles.remove(item);

                }
            }

            //过滤已经上传成功的视频文件不显示在本地列表中
            boolean isneedFilter = ConstantUtil.getInstance().isNeedFilter();
            if (isneedFilter && videoUrl != null) {
                File item = new File(videoUrl);
                if (item.exists()) {
//                    item.delete();
                    mListFiles.remove(item);
                }

                String sPath = videoUrl.substring(0, videoUrl.length() - 4);
                String tPath = sPath.substring(sPath.lastIndexOf("/") + 1);
                String videoName = tPath.replace("-", "");
                deleteData(videoName);

            }

            //添加查询数据库
            queryALL();
            if(videoItem.size()==0 || videoItem==null ){
                mEmptyView.setVisibility(View.VISIBLE);
                file_recycle.setVisibility(View.GONE);
            }else {
                mEmptyView.setVisibility(View.GONE);
                file_recycle.setVisibility(View.VISIBLE);
            }
//            mAdapter = new FileAdapter(R.layout.item_video_list, mListFiles);
            mAdapter = new FileAdapter(R.layout.item_video_list, videoItem);
//            String size = String.valueOf(videoItem.size());
//            SharedPreferencesUtil.putString(LFilePickerActivity.this, SharedPreferencesUtil.FILECOUNT, size);
            mAdapter.setOnItemClickListener((adapter, view, position) -> startActivity(position));
            // 一行代码搞定（默认为渐显效果）
//        mAdapter.openLoadAnimation();
            mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.SlideInLeft);

            file_recycle.setLayoutManager(new LinearLayoutManager(LFilePickerActivity.this));
            file_recycle.setHasFixedSize(true);
            //添加分割线
//        file_recycle.addItemDecoration( new SimpleDividerDecoration(LFilePickerActivity.this));
            file_recycle.setAdapter(mAdapter);
        }
    }

    private void initPermission() {
        if (ContextCompat.checkSelfPermission(LFilePickerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
        }
        AndPermission.with(this)
                .permission(
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                // 准备方法，和 okhttp 的拦截器一样，在请求权限之前先运行改方法，已经拥有权限不会触发该方法
                .rationale((context, permissions, executor) -> {
                    // 此处可以选择显示提示弹窗
                    executor.execute();
                })
                // 用户给权限了
                .onGranted(permissions -> {

                })
                // 用户拒绝权限，包括不再显示权限弹窗也在此列
                .onDenied(permissions -> {
                    // 判断用户是不是不再显示权限弹窗了，若不再显示的话进入权限设置页
                    if (AndPermission.hasAlwaysDeniedPermission(LFilePickerActivity.this, permissions)) {
                        // 打开权限设置页
                        AndPermission.permissionSetting(LFilePickerActivity.this).execute();
                        return;
                    }
                })
                .start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    private void startActivity(int position) {

        String fPath = mListFiles.get(position).getPath();
        String sPath = fPath.substring(0, fPath.length() - 4);
        String tPath = sPath.substring(sPath.lastIndexOf("/") + 1);
        String videoName = tPath.replace("-", "");

        queryData(videoName);

        Intent intentDetail = new Intent(LFilePickerActivity.this,
                LocalVideoDetailActivity.class);

        intentDetail.putExtra("scanValue", scanValue);
        intentDetail.putExtra("result", result);
        intentDetail.putExtra("recordTime", recordTime);
        intentDetail.putExtra("videoUrl", filePath );
        startActivity(intentDetail);

    }

    private void queryData(String videoName) {


        // 从数据库中Record表里找到name=tempName的id

        Cursor cursor = helper.getReadableDatabase().rawQuery(
                "select * from uploads where videoName =? and loginName =?", new String[]{videoName,loginName});
//        Cursor cursor =  helper.getReadableDatabase().rawQuery("select * from uploads where uploadid like ? ", new String[]{"20200723103029"});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    //遍历cursor，取出数据
                    videoName2 = cursor.getString(cursor.getColumnIndex("videoName"));
                    scanValue = cursor.getString(cursor.getColumnIndex("scanValue"));
                    result = cursor.getString(cursor.getColumnIndex("result"));
                    recordTime = cursor.getString(cursor.getColumnIndex("recordTime"));
                    filePath = cursor.getString(cursor.getColumnIndex("videoUrl"));
                } while (cursor.moveToNext());
            }
        }

        cursor.close();

    }

    private void queryALL() {

        Cursor cursor = helper.getReadableDatabase().rawQuery(
                "select * from uploads where loginName =? order by recordTime  desc",  new String[]{loginName});

        // 从数据库中Record表里找到name=tempName的id
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    //遍历cursor，取出数据
                    videoName2 = cursor.getString(cursor.getColumnIndex("videoName"));
                    scanValue = cursor.getString(cursor.getColumnIndex("scanValue"));
                    result = cursor.getString(cursor.getColumnIndex("result"));
                    recordTime = cursor.getString(cursor.getColumnIndex("recordTime"));
                    filePath = cursor.getString(cursor.getColumnIndex("videoUrl"));

                    Gson gson = new Gson();
                    scanBean = gson.fromJson(scanValue, ScanBean.class);
                    videoItem.add(new VideoItem(recordTime, filePath, scanBean.getPolicyName(),
                            scanBean.getInsureTypeDesc(), "0", filePath));
                } while (cursor.moveToNext());
            }
        }
        cursor.close();

    }

    /**
     * 数据库 删除数据
     */
    private void deleteData(String videoName) {
        //values里的值不要存在空格，不然插入的数据前面会有空格
        db = helper.getWritableDatabase();
        db.execSQL("delete from uploads where videoName=?", new String[]{videoName});
        db.close();
    }

    public void setToolbarTitle(int resId) {
        mTitle.setText(resId);
    }


    @Override
    protected void initEventAndData() {

    }


    @Override
    protected void onViewCreated() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_lfile_picker;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mToolbar.setNavigationOnClickListener(v -> onBackPressedSupport());

    }

    /**
     * 检测SD卡是否可用
     */
    private boolean checkSDState() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 360;
    }
}
