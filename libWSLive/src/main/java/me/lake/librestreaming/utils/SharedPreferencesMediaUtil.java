package me.lake.librestreaming.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 */
public class SharedPreferencesMediaUtil {

    private static final String spFileName = "video";


    public static final String RECORDVIDEOPATH = "recordvideopath";
    public static final String RECORDVIDEONAME = "recordvideoname";


    public static String getString(Context context, String strKey,
                                   String strDefault) {//strDefault	boolean: Value to return if this preference does not exist.
        SharedPreferences setPreferences = context.getSharedPreferences(
                spFileName, Context.MODE_PRIVATE);
        String result = setPreferences.getString(strKey, strDefault);
        return result;
    }

    public static void putString(Context context, String strKey,
                                 String strData) {
        SharedPreferences activityPreferences = context.getSharedPreferences(
                spFileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = activityPreferences.edit();
        editor.putString(strKey, strData);
        editor.apply();
    }
}
