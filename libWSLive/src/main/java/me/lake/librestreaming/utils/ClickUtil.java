package me.lake.librestreaming.utils;

import java.util.List;

/**
 * Created by Administrator on 2016/9/6 0006.
 */
public class ClickUtil {


    private static final int CLICK_DELAY_TIME = 2000;
    private static long lastClickTime;

    /**
     * 防止button快速连点
     *
     * @return
     */
    public static boolean isNotFastClick() {
        boolean flag = false;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = currentClickTime;
        return flag;
    }
}
