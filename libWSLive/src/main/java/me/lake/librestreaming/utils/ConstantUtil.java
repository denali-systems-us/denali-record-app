package me.lake.librestreaming.utils;

public class ConstantUtil {

    private String rtmpUrl;

    private String port;

    private int test; //发送检测类型的标志

    private int addstep; //新增检测步骤时，对于检测类型相同但UI不同的情况添加

    private boolean isAgree;  //是否同意

    private boolean isFace2Face; //是否人脸同框
    private boolean isCard; //是否出示身份证件
    private boolean isInstructions; //是否成功检测说明书
    private boolean isProduct; //是否成功产品说明书
    private boolean isClause; //是否成功检测条款
    private boolean isSign; //是否成功检测签字区域

    private boolean isVoiceing;  //是否正在播放

    private boolean isVoiceFinish;

    private String insureContent; //每次检测条款不同的内容

    public static String socketStr="$_";

    private boolean isNeedFilter; //区分以后上传，现在上传,可用来过滤视频文件

    private boolean isFirstRefresh;  //是否第一次刷新
    private boolean isEndStep;  //是否语音播报最后一步
    private boolean isUploadToMain;  //是否是上传列表跳转到首页


    private static ConstantUtil instance = null;

    public static ConstantUtil getInstance(){
        if(instance == null){
            synchronized(ConstantUtil.class){
                if(instance == null){
                    instance = new ConstantUtil();
                }
            }
        }
        return instance;
    }

    public boolean isUploadToMain() {
        return isUploadToMain;
    }

    public void setUploadToMain(boolean uploadToMain) {
        isUploadToMain = uploadToMain;
    }

    public boolean isEndStep() {
        return isEndStep;
    }

    public void setEndStep(boolean endStep) {
        isEndStep = endStep;
    }

    public boolean isFirstRefresh() {
        return isFirstRefresh;
    }

    public void setFirstRefresh(boolean firstRefresh) {
        isFirstRefresh = firstRefresh;
    }

    public String getInsureContent() {
        return insureContent;
    }

    public void setInsureContent(String insureContent) {
        this.insureContent = insureContent;
    }

    public String getRtmpUrl() {
        return rtmpUrl;
    }

    public void setRtmpUrl(String rtmpUrl) {
        this.rtmpUrl = rtmpUrl;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public int getTest() {
        return test;
    }

    public void setTest(int test) {
        this.test = test;
    }

    public int getAddstep() {
        return addstep;
    }

    public void setAddstep(int addstep) {
        this.addstep = addstep;
    }

    public boolean isAgree() {
        return isAgree;
    }

    public void setAgree(boolean agree) {
        isAgree = agree;
    }

    public boolean isFace2Face() {
        return isFace2Face;
    }

    public void setFace2Face(boolean face2Face) {
        isFace2Face = face2Face;
    }

    public boolean isCard() {
        return isCard;
    }

    public void setCard(boolean card) {
        isCard = card;
    }

    public boolean isInstructions() {
        return isInstructions;
    }

    public void setInstructions(boolean instructions) {
        isInstructions = instructions;
    }

    public boolean isProduct() {
        return isProduct;
    }

    public void setProduct(boolean product) {
        isProduct = product;
    }

    public boolean isClause() {
        return isClause;
    }

    public void setClause(boolean clause) {
        isClause = clause;
    }

    public boolean isSign() {
        return isSign;
    }

    public void setSign(boolean sign) {
        isSign = sign;
    }

    public boolean isVoiceing() {
        return isVoiceing;
    }

    public void setVoiceing(boolean voiceing) {
        isVoiceing = voiceing;
    }

    public boolean isVoiceFinish() {
        return isVoiceFinish;
    }

    public void setVoiceFinish(boolean voiceFinish) {
        isVoiceFinish = voiceFinish;
    }

    public boolean isNeedFilter() {
        return isNeedFilter;
    }

    public void setNeedFilter(boolean needFilter) {
        isNeedFilter = needFilter;
    }
}
